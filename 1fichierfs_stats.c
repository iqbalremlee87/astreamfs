/*
 * 1fichierfs: deals with statistics
 *
 *
 * Copyright (C) 2018-2021  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include "1fichierfs.h"

/*******************************************************
 *  These are the things you can change at compile time.
 */
static char *stat_api_label[] = {
/*	FOLDER_LS		= 0, */ "folder/ls.cgi         ",
/*	DOWNLOAD_GET_TOKEN	= 1, */ "download/get_token.cgi",
/*	USER_INFO		= 2, */ "user/info.cgi         ",
/*	FILE_MV			= 3, */ "file/mv.cgi           ",
/*	FILE_CHATTR		= 4, */ "file/chattr.cgi       ",
/*	FILE_RM			= 5, */ "file/rm.cgi           ",
/*	FILE_CP			= 6, */ "file/cp.cgi           ",
/*	FOLDER_MV		= 7, */ "folder/mv.cgi         ",
/*	FOLDER_RM		= 8, */ "folder/rm.cgi         ",
/*	FOLDER_MKDIR		= 9, */ "folder/mkdir.cgi      ",
/*	FTP_USER_LS		=10, */ "ftp/users/ls.cgi      ",
/*	FTP_USER_RM		=11, */ "ftp/users/rm.cgi      ",
/*	FTP_USER_ADD		=12, */ "ftp/users/add.cgi     ",
/*	FTP_PROCESS		=13, */ "ftp/process.cgi       ",
/*	DOWNLOAD_GET_TOKEN	=14, */ "fallback/get_token    ",
};

static char *stat_step_label[] = {
/*	STEP_WRITING	*/ "Writing ",
/*	STEP_WAIT_FTP	*/ "Wait_FTP",
/*	STEP_WAIT_MV	*/ "Wait_mv ",
/*	STEP_WAIT_RM	*/ "Wait_rm ",
/*	STEP_DONE	*/ "  Done  ",
};

	/* Look at the algorithm to change it. SPEED_RES must be even	      */
#define SPEED_RES (8UL)	/* 8 ticks per second to compute transfer speed       */
#define SPEED_MEM (2UL)	/* 2 seconds of speed memory to avoid spike artefacts */

#define CLOCK_PREC (10000UL)  /* Display clock precision, here 100 milli sec  */

/********************************************************
 *  Do NOT change the following definitions or variables.
 *  Things would break!
 *  These definitions and variables are either system dependent,
 *  or depend on how 1fichier works.
 */
#define N_API_ROUTES (sizeof(stat_api_label)/sizeof(stat_api_label[0]))
#define CLOCK_RES (1000000000UL) /* Clock resolution is nano second           */

#define SZ_BUF_TIME (STRLEN_MAX_INT64 + 6) /* Size of buffer needed for       */
#define SZ_BUF_HR (6)			   /* various conversions             */
#define SZ_BUF_REF (7)

#define KILO (1024)

typedef unsigned long long tm_index_t;
typedef unsigned long long tm_max_t;

struct stat_times {
	_Atomic tm_max_t	   max;
	_Atomic unsigned long long sec;
	_Atomic unsigned long      nsec;
};


static const unsigned int refresh_category[] = {
/*	REFRESH_TRIGGER		= 0, */ 0,
/*	REFRESH_HIDDEN		= 1, */ 0,
/*	REFRESH_TIMER		= 2, */ 1,
/*	REFRESH_POLLING		= 3, */ 2,
/*	REFRESH_WRITE		= 4, */ 3,
/*	REFRESH_MV		= 5, */ 4,
/*	REFRESH_LINK		= 6, */ 4,
/*	REFRESH_UNLINK		= 7, */ 4,
/*	REFRESH_MKDIR		= 8, */ 4,
/*	REFRESH_RMDIR		= 9, */ 4,
/*	REFRESH_404		=10, */ 5,
/*	REFRESH_EXIT		=11, */ 6,
/*	REFRESH_INIT_ROOT	=12, */ 6,
};

static const char *refresh_category_label[] = {
			"Trig.", "Timer", "Poll.", "Write", "  API", "Error", " Auto" };

#define NB_REFRESH_CAT (sizeof(refresh_category_label) / \
			sizeof(refresh_category_label[0]))

#define MAX_ITEMS ((MAX_READERS > MAX_WRITERS) ? MAX_READERS : MAX_WRITERS)

struct spd {
		uint32_t	   speed    [MAX_ITEMS][SPEED_RES*SPEED_MEM];
	_Atomic	tm_index_t	   tm_last  [MAX_ITEMS];
		unsigned long	   sec_dl   [MAX_ITEMS];
		unsigned long long size     [MAX_ITEMS];
	_Atomic	tm_index_t	   tot_last ;
	_Atomic	unsigned long	   tot_s_dl ;
};

static struct {
		struct spd	   speed_read;
		struct spd	   speed_write;
	_Atomic unsigned long long read_nrq [MAX_READERS];
	_Atomic unsigned long	   read_err [MAX_READERS];
		struct stat_times  read_tm  [MAX_READERS];
		unsigned long long write_nrq[MAX_WRITERS];
		unsigned long	   write_err[MAX_WRITERS];
		struct stat_times  write_tm [MAX_WRITERS];
	_Atomic	unsigned long long solo_sz  ;
	_Atomic	unsigned long	   solo_nrq ;
	_Atomic	unsigned long	   solo_err ;
		struct stat_times  solo_tm  ;
	_Atomic unsigned long      api_calls[N_API_ROUTES];
	_Atomic unsigned long      api_retry[N_API_ROUTES];
	_Atomic unsigned long      api_err  [N_API_ROUTES];
		struct stat_times  api_tm   [N_API_ROUTES];
	_Atomic unsigned long	   refresh[NB_REFRESH_CAT];
} all_stats;



/*
 * @brief returns the current time index
 *
 *  Gets the current time (MONOTONIC_COARSE).
 *  Compute the index from the delay (now - main_start) shifting sec and nsec
 *  according to the speed resolution (SPEED_RES)
 *
 * @param timespec pointer where the current timespec will be stored
 * @return time index.
 */
static tm_index_t tm_to_tm_index(struct timespec *now)
{
	clock_gettime(CLOCK_MONOTONIC_COARSE, now);
	now->tv_sec -= main_start.tv_sec;
	return  ((tm_index_t)now->tv_sec ) * SPEED_RES +
		((tm_index_t)now->tv_nsec) / (CLOCK_RES / SPEED_RES);
}

/*
 * @brief returns the time max from delay
 *
 *  Compute the index from the delay shifting sec and nsec specified precision
 *
 * @param timespec pointer where the current timespec will be stored
 * @return time index.
 */
static tm_max_t tm_to_tm_max(struct timespec *delay)
{
	return (tm_max_t)delay->tv_sec  *  CLOCK_PREC +
			 delay->tv_nsec / (CLOCK_RES / CLOCK_PREC);
}


/*
 * @brief initialize statistics
 *
 * @param none
 * @return none.
 */
void init_stats()
{
	struct timespec now;
	unsigned int i;
	tm_index_t tm_last;

	memset(&all_stats, '\0', sizeof(all_stats));
	tm_last = tm_to_tm_index(&now);
	for (i = 0; i < MAX_READERS; i++) {
		all_stats.speed_read.tm_last[i] = tm_last;
	}
	for (i = 0; i < MAX_WRITERS; i++) {
		all_stats.speed_write.tm_last[i] = tm_last;
	}
}


/*
 * @brief format an unsigned long long as "human readable" à la 'curl'
 *
 *   The result is always 5 characters long (+ '\0')
 *   If possible display 5 digits (only if num is under 99999)
 *   otherwise display max 4 digits and a possible exponent (K, M, G, etc...)
 *   According to the number of digit in the integer part, displays fraction
 *   digits.
 *   Examples: 24370  1792K  92.1M  1.04G
 *
 * @param number to format
 * @param buffer to store the result (must be at least SZ_BUF_HR size)
 * @return the buffer formatted.
 */
static char *format_hr(unsigned long long num, char *buf)
{
	unsigned long frac;
	int i;
	static const char exp[]= { 'K', 'M', 'G', 'T', 'P', 'E' };

	if (num < 99999) {
		sprintf(buf, "%5lu", (unsigned long)num);
		return buf;
	}
	for (i = -1; num >= 9999; i++) {
		frac = num % KILO;
		num /= KILO;
	}

	if (num >= 100) {
		sprintf(buf, "%4lu%c", (unsigned long)num
				     , exp[i]);
		return buf;
	}
	if (num >= 10) {
		sprintf(buf, "%lu.%lu%c", (unsigned long)num
					, (frac * 10) / KILO
					, exp[i]);
	}
	else {
		sprintf(buf, "%lu.%.02lu%c", (unsigned long)num
					   , (frac * 100) / KILO
					   , exp[i]);
	}
	return buf;
}


/*
 * @brief compute the time average and formats it in the buffer
 *
 * @param seconds
 * @param nano seconds
 * @param dividend (for the average)
 * @param buffer to store the result (if needed, must be SZ_BUF_TIME size min)
 * @return none.
 */
static const char *tm_avg_to_str(unsigned long long sec, unsigned long nsec,
				 unsigned long long n, char *buf)
{
	static const char undef_avg[]= "  0.0000";

	if (0 == n)
		return undef_avg;

	nsec = ((sec % n) * CLOCK_RES + nsec) / n;
	sec /= n;
	sprintf(buf, "%3lu.%.04lu"
		   , (unsigned long)sec
		   , nsec / (CLOCK_RES / CLOCK_PREC));
	return buf;
}


/*
 * @brief formats the max time in the buffer
 *
 * Shift the max time appropriately according to the precision (CLOCK_PREC)
 *
 * @param max time
 * @param buffer to store the result (must be SZ_BUF_TIME size min)
 * @return none.
 */
static const char *tm_max_to_str(tm_max_t max, char *buf)
{
	sprintf(buf, "%3lu.%.04lu"
		   , (unsigned long)(max / CLOCK_PREC)
		   , (unsigned long)(max % CLOCK_PREC));
	return buf;
}


/*
 * @brief retrieves the reference stream for the reader within live streams
 *
 * Can also return 'idle' when appropriate or '(???)' if the reference is not
 * found (should not happen!)
 * Note: as usual, we skip 'special' (shouldn't be) and 'del' streams.
 *
 * @param max time
 * @param buffer to store the result (must be SZ_BUF_TIME size min)
 * @return none.
 */
static const char *reader_ref(bool idle, struct stream *s,
			      struct streams *head, char *buf)
{
	static const char empty [] = " idle ";
	static const char unkown[] = "(???\?)";
	unsigned long i, cnt;

	if (idle || NULL == s || NULL == head)
		return empty;
	for (i = 0; i < head->n_streams; i++) {
		cnt = LOAD(head->a_streams[i]->counter);
		if (0 != (cnt & (FL_DEL | FL_SPECIAL)))
			continue;
		if (s == head->a_streams[i]) {
			snprintf(buf, SZ_BUF_REF, "(%4lu)", i + 1);
			return buf;
		}
	}
	return unkown;
}


/*
 * @brief computes the current speed from the tick measures collected
 *
 * Algorithm: for tm_index_t, see: tm_to_tm_index
 *
 * 	Donwload speed is measured in ticks: SPEED_RES by second.
 *	It accumulates SPEED_MEM seconds.
 *	The first half second has a weight of 1
 *	Each subsequent half second is weighted +1 of the previous second.
 *	The total is then divide by the sum of weights: n * (n + 1) / 2,
 *	where 'n' is the number of half seconds, to normalize it to
 * 	a weight of 1. Finally multiplied by 2 to get speed by second.
 *
 *	Practical example SPEED_RES = 8 and SPEED_MEM = 2
 *
 *     -2s    -1.5s    -1s    -0.5s     0 ('last')
 *	|               |               |
 * 	|A.B.C.D|E.F.G.H|I.J.K.L|M.N.O.P|
 *
 * 	The compute, assuming we are in the same 'tm_index' as 'last' will be:
 *	(1 * (A+B+C+D) + 2 * (E+F+G+H) + 3 * (I+J+K+L) + 4 * (M+N+O+P)) / 5
 *
 *	When 'now' is ahead of 'last' the non-existent tick measures are zeroes,
 *	because it means no data was read in those ticks.
 *
 * 	This computation avoids abnormal "spikes" and smoothes the numbers.
 *
 * @param time index for 'now'
 * @param time index of the last tick measure
 * @param pointer on the collection of measures
 * @return the average speed.
 */
static unsigned long long compute_speed(tm_index_t now,
					tm_index_t last,
					uint32_t spds[])
{
	unsigned int i, j;
	unsigned long long speed = 0;

	if (now - last >= SPEED_RES*SPEED_MEM)
		return 0;

	for (i = SPEED_MEM * 2; i > 0; i--)
		for (j = 0; j < SPEED_RES / 2; j++, now--)
			if (now <= last)
				speed += spds[now % (SPEED_RES*SPEED_MEM)] * i;
	return speed / (2 * SPEED_MEM + 1);
}


/*
 * @brief prints statistics in the buffer, checking for overflow
 *
 *  Macro to simplify the code. Used only in out_stats()
 *    - prints in the buffer
 *    - update the count of bytes
 *    - if overflow, returns.
 */
#define SNPRINTF(format, ...) do{\
	size_t cp;\
	cp = snprintf(&stts->buf[stts->size],\
		      STAT_MAX_SIZE - stts->size,\
		      format,\
		      __VA_ARGS__ );\
	if (cp >= STAT_MAX_SIZE - stts->size) {\
		stts->size = STAT_MAX_SIZE;\
		return stts;}\
	stts->size += cp; } while(0)


/*
 * @brief prepare the content of the stats file
 *
 * This function is called when opening the pseudo-file for statistics.
 * The pointer is then returned by unfichier_open, and is unique to the
 * process that opened this pseudo-file. Should other processes ask for
 * statistics in parallel, they will get a different buffer.
 * Subsequent read from that unfichier_open will return the data that is in
 * the buffer. unfichier_release will free the buffer (no rcu needed here).
 *
 * Note: since this is called by unfichier_open_wrapped, it is under rcu_lock.
 *       It is then safe to get the live (streams) pointer.
 *
 * IMPORTANT (and TODO): the formatting of the data will be deprecated soon
 *	to be replaced by a more "linux-like" feature, same as data presented
 *	by the kernel under /sys. Formatting should not be made here, but
 *	by the (other) user-space tool that needs the statistics.
 *	This deprecation concerns only the formatting part (this function),
 *	the statistics computation stays.
 *
 * Global note:
 * 	Numbers: are displayed "human readable" à la 'curl'.
 * 		4 digits are displays plus a multiplier (K, M, G, etc...)
 *		When a decimal point is used, only 3 digits are displayed.
 *		Examples: 7248M, 1.04G, 12.7K, 940
 *	Time:	for avg and max the precision is 100 millisec: eg 999.1234
 *		for uptime and time left to stream: 2d hh:mm:ss
 *
 * Parts of statistics:
 *   Uptime: time since the program started (eg: 2d 14:23:05)
 *   Readers: for each reader the display shows
 *		- Index: [01] to MAX_READERS
 *		- Down.: bytes read
 *		- N.Req: Number of read requests completed
 *		- Avg Time: average time to respond to a read request
 *		- Max Time: maximum time a read request took
 *		- Ref: either 'idle' or the index of the stream (eg (   7) )
 *		- Qu: number of queued read requests
 *		- N.Err: number of errors
 *		- Speed: current read speed
 *		- Av.Sp: average speed. The average is computed only on seconds
 *			where some data was read. It does not change when
 *			the reader is idle or no request are being served.
 *			The average on the lifespan of the program can be
 *			computed with the uptime.
 *  	Solo:	This shown the same statistics as the readers when single ranges
 *		must be used. Now shown if no single range was ever used.
 *		Note: when this is shown, the global performance starts to drop.
 *		      To fix that, either do less parallel read or recompile
 *		      the program with more readers.
 *		Not displayed 'Ref', 'Qu', 'Speed': it would not make sense.
 *  	Total:	Total for all readers (including Solo if present)
 *   Writers: since writers are started only when needed, this part is shown
 *	      only if a writer have been started. Only started writers are shown
 *		- Index: [01] to MAX_WRITERS
 *		- Upload: bytes written
 *		- N.Req: Number of write requests completed
 *		- Avg Time: average time to respond to a write request
 *		- Max Time: maximum time a write request took
 *		- Ref: either 'idle' or the index of the file (eg (   7) )
 *		- N.Err: number of errors
 *		- Speed: current write speed
 *		- Av.Sp: average speed. The average is computed only on seconds
 *			where some data was written. It does not change when
 *			the writer is idle. The average on the lifespan of the
 *			program can be computed with the uptime.
 *   Total: Total for all started writers
 *
 * 	NOTE: what is measured is the read of write request received (through
 * 	      the fuse callbacks), not the network speed. This can lead to
 *	      noticeable differences, for example when doing a "pv" to copy
 *	      a file trough encfs stacked on top of 1fichierfs, "pv" will
 *	      measure the bytes written (to encfs), whereas 1fichierfs will
 *	      report the bytes sent by encfs. Because encfs sends some data
 *	      and then overwrites it, the data written by encfs is noticeable
 *	      bigger than the "useful" write (around 10 to 15% more data).
 *
 *   API: for each of the API routes where at least 1 request occurred, shows:
 * 		-      : name of the route (eg. folder/ls.cgi)
 *		- N.Req: Number of times this API was called.
 *		- Avg Time: Average time of all calls on this routes
 *		- Max Time: Maximum time it took for a request on this route
 *		- Retry: Number of retries on this route. This happens when the
 *			 server responds HTTP 429 (too many requests). In such
 *			 case, the user should do less parallel tasks to avoid
 *			 throttling.
 *		- N.Err: Number of errors on this route (429 not counted as err)
 *  	Total:	Total for all calls to any API route.
 *   Refresh: show count with cause of refresh. Causes are:
 *		"Trig." : triggered by the user (eg opening trigger file)
 *		"Timer" : timer elapsed (option --refresh-time)
 *		"Write" : when a write action completes it refreshes the target
 *			  directory where to store the file
 *		"  API" : some API calls trigger diectory refreshes (eg rename)
 *		"Error" : some errors do a full refresh (eg cache not synced)
 *		" Auto" : on init (when root is not /) and exit (always).
 *   Memory: show count and memory alloced/freed.
 * 	     plus contentions: number and max spin in the "memory" column.
 *   "Write files": if any, files being written are shown.
 *		- Ref: the reference of this file (links with writers Ref)
 *		- Size: size written so far
 *		- Left: time estimated to next step
 *		- Lnk: number of links on this file
 *		- Er: 'E' if an error was detected on this stream
 *		- Step: step in the writing process, can be: "Writing",
 * 			"Wait_FTP" (before FTP process is triggered),
 *			"Wait_mv" or "Wait_rm" when waiting for file to show in
 *			the upload directory to move or delete it, or "Done".
 *			The step can be prefixed with "rm/" if the user
 * 			requested the deletion of the file.
 *		- Path: target of the file.
 *   Live streams: for each 'live stream' (opened or recently closed files):
 *		- Ref: the reference of this file (links with readers Ref)
 *		- Size: size of the file
 *		- Left: time left before we consider this stream 'dead'
 *		- Lnk: number of links on this stream (opened + readers)
 *		- Er: 'E' if an error was detected on this stream
 *		- N.Loc: number of times download/get_token was called for this
 *			 stream.
 *		- N.Stm: number of curl requests (streams) done for this entry.
 *		- Path: the initial path of this entry (when first opened).
 *			Note: the key to the files is in fact the URL. So if
 *			the file was renamed/moved after the first open, the
 *			entry will still be used for streaming, but the path
 *			is not changed and keeps the value it had when first
 *			opened.
 * @param none
 * @return a pointer on the stat structure populated with the current stats.
 */
struct stats *out_stats(struct stats *out)
{
	struct stats *stts;
	struct streams *strms;
	struct strm_loc *loc;
	tm_max_t read_max, api_max, sum_max = 0;
	unsigned long long api_sec, read_sz, read_sec, read_nrq;
	unsigned long read_err, read_nsec;
	unsigned long api_calls, api_retry, api_err, api_nsec;
	unsigned long long sum_sz  = 0, sum_nrq  = 0;
	unsigned long long sum_sec = 0, sum_nsec = 0;
	unsigned long sum_err = 0, sum_calls = 0, sum_retry = 0;
	unsigned int nQ, sum_Q = 0;
	unsigned int days, hours, minutes;
	char avg[SZ_BUF_TIME], max[SZ_BUF_TIME], buf6[SZ_BUF_REF];
	char buf1[SZ_BUF_HR], buf2[SZ_BUF_HR], buf3[SZ_BUF_HR];
	char buf4[SZ_BUF_HR], buf5[SZ_BUF_HR];
	uint32_t speed[SPEED_RES*SPEED_MEM];
	unsigned long long cur_speed, avg_speed, sum_speed = 0;
	unsigned long long mem_a, mem_f, mem_na, mem_nf;
	tm_index_t tm_last, tm_tmp, tm_now;
	unsigned long sec_dl, sum_sec_dl = 0, t;
	struct timespec now;
	int i;
	unsigned long cnt, ii;
	unsigned long contentions, max_spins;

	struct wfile_list *wfl;
	bool *keep;
	unsigned long wn_files;

	if (out != NULL)
		stts = out;
	else
		stts = fs_alloc(sizeof(struct stats));
	STORE(stts->counter, FL_SPECIAL);
	stts->size = 0;

	strms = LOAD(live);

	/****************************
	 * Part: Uptime
	 */
	tm_now  = tm_to_tm_index(&now);
	days    =  now.tv_sec / 86400;
	hours   = (now.tv_sec % 86400) / 3600;
	minutes = (now.tv_sec %  3600) /   60;
	if (0 != days)
		SNPRINTF("Uptime: %ud %.02u:%.02u:%.02u\n\n",
			 days, hours, minutes, (unsigned int)(now.tv_sec % 60));
	else
		SNPRINTF("Uptime: %.02u:%.02u:%.02u\n\n",
			 hours, minutes, (unsigned int)(now.tv_sec % 60));

	/****************************
	 * Part: Readers (all)
	 */
	SNPRINTF("Readers:\n%s",
		 "     Down. N.Req  Avg Time  Max Time   Ref  Qu N.Err Speed Av.Sp\n");
	for (i=0; i<MAX_READERS; i++) {
		tm_last   = LOAD(all_stats.speed_read.tm_last[i]);
		/* This loop ensures that the speed measures and the tm_last
		 * are in sync. It is not an issue if this function has to
		 * loop a few times since statistics are not time-critical.
		 */
		do {
			tm_tmp   = tm_last;
			memcpy(speed, &all_stats.speed_read.speed[i][0],
			       sizeof(speed));
			tm_last  = LOAD(all_stats.speed_read.tm_last[i]);
		} while (tm_tmp != tm_last);
		sec_dl   = all_stats.speed_read.sec_dl[i];
		read_sz  = all_stats.speed_read.size[i];

		read_nrq = LOAD(all_stats.read_nrq[i]	 , relaxed);
		read_err = LOAD(all_stats.read_err[i]	 , relaxed);
		read_sec = LOAD(all_stats.read_tm[i].sec , relaxed);
		read_nsec= LOAD(all_stats.read_tm[i].nsec, relaxed);
		read_max = LOAD(all_stats.read_tm[i].max , relaxed);
		cur_speed = compute_speed(tm_now, tm_last, speed);
		sum_speed += cur_speed;
		if (read_max > sum_max)
			sum_max = read_max;
		sum_sz  += read_sz;
		sum_nrq += read_nrq;
		sum_err += read_err;
		sum_sec += read_sec;
		sum_nsec+= read_nsec;
		sum_Q   += (nQ = readers[i].n_rq);
		if (0 == sec_dl)
			avg_speed = 0;
		else
			avg_speed = read_sz / (unsigned long long)sec_dl;
		SNPRINTF("[%.02d] %s %s  %s  %s %s %2u %s %s %s\n",
			 i + 1,
			 format_hr(read_sz , buf1),
			 format_hr(read_nrq, buf2),
			 tm_avg_to_str(read_sec, read_nsec, read_nrq, avg),
			 tm_max_to_str(read_max, max),
			 reader_ref(readers[i].idle, readers[i].s, strms, buf6),
			 nQ,
			 format_hr(read_err , buf3),
			 format_hr(cur_speed, buf4),
			 format_hr(avg_speed, buf5));
	}

	/****************************
	 * Part: Readers (solo)
	 */
	read_nrq = LOAD(all_stats.solo_nrq);
	if (0 != read_nrq) {
		read_sz  = LOAD(all_stats.solo_sz	, relaxed);
		read_err = LOAD(all_stats.solo_err	, relaxed);
		read_sec = LOAD(all_stats.solo_tm.sec	, relaxed);
		read_nsec= LOAD(all_stats.solo_tm.nsec	, relaxed);
		read_max = LOAD(all_stats.solo_tm.max	, relaxed);
		if (read_sec < 1024)
			avg_speed = ((read_sz  * 1024) /
				     (read_sec * 1024  +
				      (1024ULL * read_nsec) / CLOCK_RES));
		else
			avg_speed = read_sz / read_sec;
		sum_sz  += read_sz;
		sum_nrq += read_nrq;
		sum_err += read_err;
		sum_sec += read_sec;
		sum_nsec+= read_nsec;
		if (read_max > sum_max)
			sum_max = read_max;
		SNPRINTF("solo %s %s  %s  %s           %s       %s\n",
			 format_hr(read_sz , buf1),
			 format_hr(read_nrq, buf2),
			 tm_avg_to_str(read_sec, read_nsec, read_nrq, avg),
			 tm_max_to_str(read_max, max),
			 format_hr(read_err , buf3),
			 format_hr(avg_speed, buf4));
	}

	/****************************
	 * Part: Readers (Total)
	 */
	SNPRINTF("%s","----------------------------------------------------------------\n");
	sum_sec   += sum_nsec / CLOCK_RES;
	sum_nsec   = sum_nsec % CLOCK_RES;
	sum_sec_dl = LOAD(all_stats.speed_read.tot_s_dl);
	if (0 == sum_sec_dl)
		avg_speed = 0;
	else
		avg_speed = sum_sz / (unsigned long long)sum_sec_dl;
	SNPRINTF("Tot. %s %s  %s  %s       %3u %s %s %s\n\n",
		 format_hr(sum_sz   , buf1),
		 format_hr(sum_nrq  , buf2),
		 tm_avg_to_str(sum_sec, sum_nsec, sum_nrq, avg),
		 tm_max_to_str(sum_max, max),
		 sum_Q,
		 format_hr(sum_err  , buf3),
		 format_hr(sum_speed, buf4),
		 format_hr(avg_speed, buf5));

	/****************************
	 * Part: Writers (all)
	 */
	wfl = LOAD(wflist);
	wn_files = get_wfl_n_files(wfl, &keep);

	for (i = 0; i < MAX_WRITERS; i++) {
		char ref[] = " idle ";
		if (!get_writer_ref(wfl, keep, i, ref, sizeof(ref)))
			break;
		/* Don't display anything if no writers are started */
		if (0 == i) {
			SNPRINTF("Writers:\n%s",
				 "    Upload N.Req  Avg Time  Max Time   Ref     N.Err Speed Av.Sp\n");
			sum_speed = 0;
			sum_max   = 0;
			sum_sz    = 0;
			sum_nrq   = 0;
			sum_err   = 0;
			sum_sec   = 0;
			sum_nsec  = 0;
		}
		tm_last   = LOAD(all_stats.speed_write.tm_last[i]);
		/* This loop ensures that the speed measures and the tm_last
		 * are in sync. It is not an issue if this function has to
		 * loop a few times since statistics are not time-critical.
		 */
		do {
			tm_tmp   = tm_last;
			memcpy(speed, &all_stats.speed_write.speed[i][0],
			       sizeof(speed));
			tm_last  = LOAD(all_stats.speed_write.tm_last[i]);
		} while (tm_tmp != tm_last);
		sec_dl   = all_stats.speed_write.sec_dl[i];
		read_sz  = all_stats.speed_write.size[i];

		read_nrq = all_stats.write_nrq[i];
		read_err = all_stats.write_err[i];
		read_sec = LOAD(all_stats.write_tm[i].sec , relaxed);
		read_nsec= LOAD(all_stats.write_tm[i].nsec, relaxed);
		read_max = LOAD(all_stats.write_tm[i].max , relaxed);
		cur_speed = compute_speed(tm_now, tm_last, speed);
		sum_speed += cur_speed;
		if (read_max > sum_max)
			sum_max = read_max;
		sum_sz  += read_sz;
		sum_nrq += read_nrq;
		sum_err += read_err;
		sum_sec += read_sec;
		sum_nsec+= read_nsec;
		if (0 == sec_dl)
			avg_speed = 0;
		else
			avg_speed = read_sz / (unsigned long long)sec_dl;

		SNPRINTF("[%.02d] %s %s  %s  %s %s    %s %s %s\n",
			 i + 1,
			 format_hr(read_sz , buf1),
			 format_hr(read_nrq, buf2),
			 tm_avg_to_str(read_sec, read_nsec, read_nrq, avg),
			 tm_max_to_str(read_max, max),
			 ref,
			 format_hr(read_err , buf3),
			 format_hr(cur_speed, buf4),
			 format_hr(avg_speed, buf5));
	}
	/****************************
	 * Part: Writers (Total)
	 */
	if (i > 0) {
		SNPRINTF("%s","----------------------------------------------------------------\n");
		sum_sec   += sum_nsec / CLOCK_RES;
		sum_nsec   = sum_nsec % CLOCK_RES;
		sum_sec_dl = LOAD(all_stats.speed_write.tot_s_dl, relaxed);
		if (0 == sum_sec_dl)
			avg_speed = 0;
		else
			avg_speed = sum_sz / (unsigned long long)sum_sec_dl;
		SNPRINTF("Tot. %s %s  %s  %s           %s %s %s\n\n",
			format_hr(sum_sz   , buf1),
			format_hr(sum_nrq  , buf2),
			tm_avg_to_str(sum_sec, sum_nsec, sum_nrq, avg),
			tm_max_to_str(sum_max, max),
			format_hr(sum_err  , buf3),
			format_hr(sum_speed, buf4),
			format_hr(avg_speed, buf5));

	}


	/****************************
	 * Part: API (all)
	 */
	SNPRINTF("API:\n%s",
		 "                        N.Req  Avg Time  Max Time Retry N.Err\n");
	sum_sec = 0;
	sum_nsec= 0;
	sum_err = 0;
	sum_max = 0;
	for (i=0; i<N_API_ROUTES; i++) {
		api_calls = LOAD(all_stats.api_calls[i]  , relaxed);
		if (0 == api_calls)
			continue;
		api_retry = LOAD(all_stats.api_retry[i]  , relaxed);
		api_err   = LOAD(all_stats.api_err[i]	 , relaxed);
		api_sec   = LOAD(all_stats.api_tm[i].sec , relaxed);
		api_nsec  = LOAD(all_stats.api_tm[i].nsec, relaxed);
		api_max   = LOAD(all_stats.api_tm[i].max , relaxed);
		sum_sec   += api_sec;
		sum_nsec  += api_nsec;
		sum_calls += api_calls;
		sum_retry += api_retry;
		sum_err   += api_err;
		if (api_max > sum_max)
			sum_max = api_max;
		SNPRINTF("%s: %s  %s  %s %s %s\n",
			 stat_api_label[i],
			 format_hr(api_calls, buf1),
			 tm_avg_to_str(api_sec, api_nsec, api_calls, avg),
			 tm_max_to_str(api_max, max),
			 format_hr(api_retry, buf2),
			 format_hr(api_err  , buf3));
	}

	/****************************
	 * Part: API (Total)
	 */
	SNPRINTF("%s","--------------------------------------------------------------\n");
	sum_sec += sum_nsec / CLOCK_RES;
	sum_nsec = sum_nsec % CLOCK_RES;
	SNPRINTF("Total                 : %s  %s  %s %s %s\n\n",
		 format_hr(sum_calls, buf1),
		 tm_avg_to_str(sum_sec, sum_nsec, sum_calls, avg),
		 tm_max_to_str(sum_max, max),
		 format_hr(sum_retry, buf2),
		 format_hr(sum_err  , buf3));

	/****************************
	 * Part: Refresh
	 */
	 SNPRINTF("%s", "        ");
	 for (i = 0; i < NB_REFRESH_CAT; i++)
		 SNPRINTF(" %s", refresh_category_label[i]);
	 SNPRINTF("%s", "   Total\nRefresh:");
	 sum_nrq  = 0;
	 for (i = 0; i < NB_REFRESH_CAT; i++) {
		ii = LOAD(all_stats.refresh[i]);
		sum_nrq += ii;
		SNPRINTF(" %s", format_hr((unsigned long long)ii, buf1));
	}
	SNPRINTF("   %s\n\n", format_hr(sum_nrq, buf1));

	/****************************
	 * Part: Memory
	 */
	mem_a  = LOAD(mem_alloced, relaxed);
	mem_f  = LOAD(mem_freed	 , relaxed);
	mem_na = LOAD(n_alloc	 , relaxed);
	mem_nf = LOAD(n_free	 , relaxed);
	SNPRINTF("             Number Memory\nAllocations:  %s  %s\n",
		 format_hr(n_alloc, buf1), format_hr(mem_a, buf2));
	SNPRINTF("Frees      :  %s  %s\n",
		 format_hr(n_free , buf3), format_hr(mem_f, buf4));
	SNPRINTF("Difference :  %s  %s\n",
		 format_hr(mem_na - mem_nf, buf1),
		 format_hr(mem_a  - mem_f , buf2));
	spin_get(&contentions, &max_spins);
	SNPRINTF("Contentions:  %s  %s\n\n",
		 format_hr(LOAD(contentions, relaxed), buf1),
		 format_hr(LOAD(max_spins  , relaxed), buf2));

	/****************************
	 * Part: Live streams
	 */
	if (0 == wn_files && (NULL == strms || 0 == strms->n_streams))
		return stts;

	/* Sub-part 'title'
	 */
	SNPRINTF("Live streams: |Write| %lu    (Read) %lu\n",
		 wn_files,
		 ((NULL == strms) ? 0 : strms->n_streams));
	SNPRINTF("%s", "  Ref   Size  Left Lnk Er N.Loc N.Stm Path\n");

	/* Sub-part for 'write' streams
	 */
	for (ii = 0; ii < wn_files; ii++) {
 		uint64_t size;
		time_t   eta;
		unsigned long counter, step;
		const char *path;
		char chron[] = "--:--";

		get_wf_info(wfl, keep, ii, &size, &eta, &counter, &step, &path);
		if (0 != eta) {
			if (eta >= now.tv_sec + main_start.tv_sec) {
				eta -= (now.tv_sec + main_start.tv_sec);
				if (eta > 99 * 60 + 59)
					snprintf(chron, sizeof(chron),
						 "**:**");
				else
					snprintf(chron, sizeof(chron),
						 "%.02lu:%.02lu",
						 (unsigned long)eta / 60,
						 (unsigned long)eta % 60);
			}
			else {
				eta = (now.tv_sec + main_start.tv_sec) - eta;
				if (eta > 9 * 60 + 59)
					snprintf(chron, sizeof(chron),
						 "-*:**");
				else
					snprintf(chron, sizeof(chron),
						 "-%.01lu:%.02lu",
						 (unsigned long)eta / 60,
						 (unsigned long)eta % 60);
			}
		}
		SNPRINTF("|%4lu| %s %s %3lu  %c %s%s %s\n",
			 ii + 1,
			 format_hr(size, buf1),
			 chron,
			 (counter & OPEN_MASK) / OPEN_INC,
			 (counter & FL_ERROR) != 0 ? 'E'   : ' '  ,
			 (counter & FL_DEL  ) != 0 ? "rm/" : "   ",
			 stat_step_label[step],
			 path);
	}
	if (0 != wn_files) {
		fs_free(keep);
	/* Separation line when both parts are present
	 */
		if (NULL != strms) {
			SNPRINTF("%s", "------\n");
		}
	}

	/* Sub-part for 'read' streams
	 */
	if (NULL == strms)
		return stts;
	for (ii = 0; ii < strms->n_streams; ii++) {
		cnt = LOAD(strms->a_streams[ii]->counter, relaxed);
		if (0 != (cnt & (FL_DEL | FL_SPECIAL)))
			continue;

		loc = LOAD(strms->a_streams[ii]->loc, relaxed);
		if (NULL == loc) {
			t= 0;
		}
		else {
			if (loc->until > now.tv_sec + main_start.tv_sec)
				t = loc->until - now.tv_sec - main_start.tv_sec;
			else
				t = 0;
		}
		SNPRINTF("(%4lu) %s %.02lu:%.02lu %3lu  %c %s %s %s\n",
			 ii + 1,
			 format_hr(strms->a_streams[ii]->size, buf1),
			 t / 60, t % 60,
			 (cnt & OPEN_MASK) / OPEN_INC,
			 (cnt & FL_ERROR) != 0 ? 'E' : ' ',
			 format_hr(strms->a_streams[ii]->nb_loc, buf2),
			 format_hr(strms->a_streams[ii]->nb_streams, buf3),
			 strms->a_streams[ii]->path);
	}
	return stts;
}

/*
 * @brief time increment
 *
 * The statistic times are update so:
 * 	- Add the delay between now and start param.
 * 		If nsec is above 1000000000, normalise it (atomic exchange).
 *	- Save the 'max' if needed.
 *
 * @param time statistics pointer to update
 * @param start time
 * @return none
 */
static void inc_time(struct stat_times *t, struct timespec *start )
{
	unsigned long spin = 0;
	unsigned long cur_nsec, nsec, sec;
	tm_max_t max, cur_t;
	struct timespec now;

	clock_gettime(CLOCK_MONOTONIC, &now);
	if (now.tv_nsec < start->tv_nsec) {
		now.tv_nsec += CLOCK_RES;
		now.tv_sec  -= 1;
	}
	now.tv_nsec -= start->tv_nsec;
	now.tv_sec  -= start->tv_sec;
	cur_nsec = LOAD(t->nsec);

	do {
		spin++;
		nsec = cur_nsec + now.tv_nsec;
		if (nsec >= CLOCK_RES) {
			nsec -= CLOCK_RES;
			sec  = now.tv_sec + 1;
		}
		else {
			sec  = now.tv_sec;
		}
	} while(!XCHG(t->nsec, cur_nsec, nsec));
	spin_add(spin -1, "inc_time(1)");
	/* Atomic operations on 64 bits implies a lock on 32 bits systems, so
	 * avoiding it when not necessary should be beneficial in spite of
	 * the test and jump cost */
	if (0 != sec)
		ADD(t->sec, (unsigned long long)sec, relaxed);

	cur_t = tm_to_tm_max(&now);
	max = LOAD(t->max);
	spin = 0;
	while (cur_t > max && !XCHG(t->max, max, cur_t))
		spin++;
	spin_add(spin, "inc_time(2)");
}

/*
 * @brief update speed statistics
 *
 * This is called by the "engine", precisely prune_queue when full buffers
 * are sent back. Calling it there avoids locking.
 *
 * @param number of bytes written (if negative = error)
 * @param index of the reader (from 0 to MAX_READERS - 1)
 * @return none
 */
void update_speed_stats(bool read, int written, unsigned int i)
{
	unsigned int j;
	struct timespec now;
	tm_index_t tm_now, tot_last;
	struct spd *sp;

	if (NULL == params1f.stat_file)
		return;

	sp = (read) ? &all_stats.speed_read : &all_stats.speed_write;
	sp->size[i] += (unsigned long long)written;

	tm_now = tm_to_tm_index(&now);
	if (tm_now == sp->tm_last[i]) {
		j = tm_now % (SPEED_RES * SPEED_MEM);
		if (likely(sp->speed[i][j] < UINT32_MAX - (long)written))
			sp->speed[i][j] += written;
		else
			sp->speed[i][j]  = UINT32_MAX;
	}
	else {
		if (tm_now - sp->tm_last[i] >= (SPEED_RES*SPEED_MEM)) {
			sp->sec_dl[i]++;
			memset(sp->speed[i],
			       '\0',
			       sizeof(sp->speed[0]));
		}
		else {
			if (sp->tm_last[i] / SPEED_RES !=
			    tm_now / SPEED_RES)
				sp->sec_dl[i]++;
			for (j = 1; sp->tm_last[i] + j < tm_now; j++)
				sp->speed[i]
					[(sp->tm_last[i] + j) %
						(SPEED_RES*SPEED_MEM)] = 0;
		}
		sp->speed[i][tm_now % (SPEED_RES*SPEED_MEM)] = written;
		STORE(sp->tm_last[i], tm_now);
		tot_last = LOAD(sp->tot_last);
		if (tot_last / SPEED_RES != tm_now / SPEED_RES) {
			if (XCHG(sp->tot_last, tot_last, tm_now))
				ADD(sp->tot_s_dl, 1UL, relaxed);
		}
	}
}

/*
 * @brief update read statistics
 *
 *
 * @param number of bytes written (if negative = error)
 * @param index of the reader (from 0 to MAX_READERS - 1)
 * @param timespec when the read was received
 * @return none
 */
void update_read_stats(int written, unsigned int i, struct timespec *start)
{
	inc_time(&all_stats.read_tm[i], start);

	ADD(all_stats.read_nrq[i], 1UL, relaxed);
	if (written < 0)
		ADD(all_stats.read_err[i], 1UL, relaxed);
}

/*
 * @brief update write statistics
 *
 * Times and request count.
 *
 * @param number of bytes written (not called on error, is always >0)
 * @param index of the writer
 * @param timespec when the write was received
 * @return none
 */
void update_write_stats(int written, unsigned int i, struct timespec *start)
{
	if (i >= MAX_WRITERS)	/* This can happen when racing with a DEL     */
		return;		/* In this case, do nothing avoids mem corrup */

	inc_time(&all_stats.write_tm[i], start);

	all_stats.write_nrq[i] += 1UL;
}

/*
 * @brief update write errors
 *
 * Error detected by async writers are counted here.
 *
 * @param index of the writer
 * @param timespec when the write was received
 * @return none
 */
void update_write_err(unsigned int i)
{
	all_stats.write_err[i]++;
}

/*
 * @brief update read statistics when doing a single range
 *
 *
 * @param number of bytes written (if negative = error)
 * @param timespec when the read was received
 * @return none
 */
void update_single_stats(int written, struct timespec *start)
{
	if (written >= 0)
		ADD(all_stats.solo_sz, (unsigned long long)written, relaxed);
	else
		ADD(all_stats.solo_err, 1L, relaxed);
	ADD(all_stats.solo_nrq, 1UL, relaxed);

	inc_time(&all_stats.solo_tm, start);
}


/*
 * @brief update api statistics
 *
 *
 * @param index of the API route
 * @param timespec when the api call started
 * @param http_code of the response
 * @param number of retries
 * @return none
 */
void update_api_stats(enum api_routes rt, struct timespec *start,
		      long http_code, unsigned int retry)
{
	ADD(all_stats.api_calls[rt], 1UL, relaxed);
	if (0 != retry)
		ADD(all_stats.api_retry[rt], retry, relaxed);
	if (HTTP_OK != http_code)
		ADD(all_stats.api_err[rt], 1UL, relaxed);
	inc_time(&all_stats.api_tm[rt], start);
}


/*
 * @brief update refresh statistics
 *
 * @param cause of the refresh
 * @return none
 */
void update_refresh_stats(enum refresh_cause cause)
{
	ADD(all_stats.refresh[refresh_category[cause]], 1UL, relaxed);

}
