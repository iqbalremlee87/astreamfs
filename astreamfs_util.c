/*
 * astreamfs and derivative work (1fichierfs) common code
 *
 *
 * Copyright (C) 2018-2020  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#define STREAM_STRUCT void
#include "astreamfs_util.h"
#include <malloc.h>


/*
 * @brief Global parameters of the daemon.
 *
 */
struct params params= {
/*	struct stat mount_st;	 */	{0},
/*	bool	foreground;	 */	false,
/*	bool	debug;		 */	false,
/*	bool	noatime;	 */	false,
/*	bool	noexec;		 */	false,
/*	bool	nosuid;		 */	false,
/*	unsigned int log_level;  */	LOG_WARNING,
/*	unsigned int network_wait*/	60, /* Default wait 60 sec at init */
/*	char *log_file;		 */	NULL,
/*	char *user_agent;	 */	NULL,
/*	char *filesystem_name;	 */	NULL,
/*	char *filesystem_subtype;*/	NULL,
/*	char *ca_file;		 */	NULL,
	  };


/*
 * @brief log utility.
 *
 * The log utility printf either on terminal, on a file or to syslog depending
 * on the situation.
 *
 * We have here global external variables, macro and function definition.
 *
 * The use is with the macro lprintf(LOG_xxx, ...)
 * LOG_xxx is a log level as defined in syslog.h
 * The rest are the same paramaters as printf.
 */
bool init_done = false;
FILE *log_fh = NULL;
struct timespec main_start;

void lprintf( int level, const char *pFormat, ... )
{
	char msg[1024];
	static const char *msg_err_lvl[]= {
		"EMERGENCY",
		"ALERT",
		"CRITICAL",
		"ERROR",
		"WARNING",
		"NOTICE",
		"INFO",
		"DEBUG" };
	int s;
	va_list ap;
	struct timespec now;

	if ( level <= params.log_level ) {
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		if (now.tv_nsec < main_start.tv_nsec) {
			now.tv_nsec += 1000000000L;
			now.tv_sec -= 1L;
		}
		now.tv_sec  -= main_start.tv_sec;
		now.tv_nsec -= main_start.tv_nsec;

		s = snprintf(msg, sizeof(msg), "[%s %5lu.%03lu] %s: ",
			     log_prefix, now.tv_sec, now.tv_nsec / 1000000,
			     msg_err_lvl[level]);

		va_start(ap, pFormat);
		vsnprintf(msg + s, sizeof(msg) - s, pFormat, ap);
		va_end(ap);

		if (NULL == log_fh) {
			if (init_done && !params.foreground)
				syslog(LOG_DAEMON | level, "%s", msg);
			else
				fprintf(stderr, "%s", msg);
		}
		else {
			fprintf(log_fh, "%s", msg);
			fflush(log_fh);
		}
	}
	if (( init_done && level <= LOG_CRIT) ||
	    (!init_done && level <= LOG_ERR ))
		exit(EXIT_FAILURE);
}

_Atomic unsigned long long mem_alloced = 0;
_Atomic unsigned long long mem_freed   = 0;
_Atomic unsigned long long n_alloc     = 0;
_Atomic unsigned long long n_free      = 0;
/*
 * @brief Memory allocation utility.
 *
 * Simplifies handling potential error on memory allocation.
 *
 * @param: size of memory to allocate
 * @return: pointer to allocated memory... does not return on error since we
 *          trigger a critical error, lprintf will call exit with EXIT_FAILURE.
 * 	    This means the caller do not need to test for the returned pointer,
 *          it is always valid when the function returns (hence simplification)
 */

void *fs_alloc(size_t size)
{
	void *p;
	if (0 == size)
		return NULL;
	p = malloc(size);
	if ( NULL == p )
		lprintf(LOG_CRIT,
			"out of memory. Cannot allocate %lu bytes.\n",
			size);
	ADD(n_alloc, 1UL, relaxed);
	ADD(mem_alloced, malloc_usable_size(p), relaxed);
	return p;
}

void *fs_calloc(size_t nmemb, size_t size)
{
	void *p;
	p= fs_alloc(nmemb * size);
	memset(p, '\0', nmemb * size);
	return p;
}

void fs_free(void *p)
{
	if (NULL == p)
		return;
	ADD(n_free, 1UL, relaxed);
	ADD(mem_freed, malloc_usable_size(p), relaxed);
	free(p);
}

void *fs_realloc(void *ptr, size_t size)
{
	size_t old_alloced, new_alloced;

	if (NULL == ptr)
		return fs_alloc(size);
	if (0 == size) {
		fs_free(ptr);
		return NULL;
	}

	old_alloced = malloc_usable_size(ptr);
	ptr = realloc(ptr, size);
	if ( NULL == ptr )
		lprintf(LOG_CRIT,
			"out of memory. Cannot re-allocate %lu bytes.\n",
			size);
	new_alloced = malloc_usable_size(ptr);
	if (old_alloced != new_alloced) {
		if (new_alloced > old_alloced)
			ADD(mem_alloced, new_alloced - old_alloced);
		else
			SUB(mem_alloced, old_alloced - new_alloced);
	}
	return ptr;
}

char *fs_strdup (const char *s)
{
	size_t len;
	void *new;
	if (NULL == s)
		return NULL;
	len = strlen(s) + 1;
	new = fs_alloc(len);
	if (NULL == new)
		return NULL;
	return (char *)memcpy(new, s, len);
}

/*
 * @brief Mutex lock-unlock utility.
 *
 * Simplifies handling and logging of mutex lock/unlock.
 * When the state pointer locked is NULL, the function locks/unlocks the
 * mutex and logs.
 * When the state pointer is not NULL, if the state corresponds to the action
 * the function does nothing but logging, otherwise the lock/unlock happens
 * and the state is changed to reflect the new mutex state.
 *
 * @param: mutex pointer to lock/unlock
 * @param: pointer to the state of the mutex (locked= true/false)
 * @return: none
 */

void lock(pthread_mutex_t *mutex, bool *locked)
{
	int err;

	if (NULL != locked && *locked)
		return;
	err= pthread_mutex_lock(mutex);
	DEBUG("lock(%p) %d\n", mutex, (NULL != locked)?*locked : 999);
	if (NULL != locked)
		*locked = true;
	if (0 != err)
		lprintf(LOG_CRIT,"pthread_mutex_lock failed with %d\n", err);
}

void unlock(pthread_mutex_t *mutex, bool *locked)
{
	int err;
#ifdef _DEBUG
	int tmp = 999;
#endif

	if (NULL != locked) {
#ifdef _DEBUG
		tmp = *locked;
#endif
		if (!*locked)
			return;
		else
			*locked = false;
	}
	DEBUG("unlock(%p) %d\n", mutex, tmp);
	err= pthread_mutex_unlock(mutex);
	if (0 != err)
		lprintf(LOG_CRIT,"pthread_mutex_lock failed with %d\n", err);
}

/*
 * @brief semaphore wait, post, init, etc... utlity.
 *
 * Simplifies handling and logging of sem_wait, sem_post, sem_trywait
 * trywait will always return 0 or EGAIN and fail (crit) on other errors.
 * timedwait will always return 0 or ETIMEDOUT and fail (crit) on other errors.
 *
 * @param: semaphore pointer
 * @return: ret code of the function
 */
int wait(sem_t *sem)
{
	int err;

	err = sem_wait(sem);
	if (0 != err)
		lprintf(LOG_CRIT,"sem_wait failed with %d\n", err);
	DEBUG( "sem_wait(%p)\n", sem);
	return err;
}

int post(sem_t *sem)
{
	int err;

	err = sem_post(sem);
	if (0 != err)
		lprintf(LOG_CRIT,"sem_post failed with %d\n", err);
	DEBUG( "sem_post(%p)\n", sem);
	return err;
}

char err_sem_init[] = "sem_init: %d\n";

int trywait(sem_t *sem)
{
	int err, tmp;
	errno = 0;

	err = sem_trywait(sem);
	tmp = errno;
	if (0 != err && EAGAIN != errno)
		lprintf(LOG_CRIT,"sem_trywait failed with %d errno=%d\n"
				, err, tmp);
	DEBUG( "sem_trywait(%p) err=%d errno=%d\n", sem, err, tmp);
	errno = tmp;
	return err;
}

int timedwait(sem_t *sem, struct timespec *until)
{
	int err, tmp;
	errno = 0;

	err = sem_timedwait(sem, until);
	tmp = errno;
	if (0 != err && ETIMEDOUT != errno)
		lprintf(LOG_CRIT,"sem_timedwait failed with %d errno=%d\n"
				, err, tmp);
	DEBUG( "sem_timedwait(%p) err=%d errno=%d\n", sem, err, tmp);
	errno = tmp;
	return err;
}

/*
 * @brief Global contention logging and counting.
 *
 * Each function using atomic_compare_exchange should have a spin counter
 * and call that function after the exchange is done.
 * When (hopefully) it did not spin, it means we had no contention.
 * Otherwise we log (debug) the function name that triggered the contention
 * and move the global counters.
 * Since spin_add uses itself an exchange, it recursively calls if it has
 * to spin. The live lock is unlikely but would be protected by stack crash.
 *
 * @param: spin count
 * @param: function name
 * @return: none
 */

static _Atomic unsigned long contentions = 0;
static _Atomic unsigned long max_spins   = 0;

void spin_add(unsigned long spin, const char* fct_name) {
	unsigned long spin2 = 0;
	unsigned long max;
	if (0 == spin)
		return;
	lprintf(LOG_INFO, "%lu contention(s) in %s\n", spin, fct_name);
	ADD(contentions, spin, relaxed);
	max= LOAD(max_spins);
	while ( spin > max &&
		!XCHG(max_spins, max, spin))
		spin2++;
	if (0 != spin2)
		spin_add(spin, "spin_add");
}

/*
 * @brief contention getter to avoid exposing the variable.
 *
 * @param: where to store the spin count (unsigned long pointer)
 * @param: where to store the max spin   (unsigned long pointer)
 * @return: none
 */
void spin_get(unsigned long *c, unsigned long *m) {
	*c = LOAD(contentions);
	*m = LOAD(max_spins);
}

/*
 * @brief Lock free (producers)/wait free (consumer) message queue.
 *
 * This is a lock free for producers and wait free for consumer message queue.
 * It works here with N producers/1 consumer. Producers push messages, they
 * might spinlock if another producer pushed a message at the same time, or if
 * the consumer poped the message queue.
 * The consumer always pops all messages in the queue, then stores them in a
 * private queue where it is alone handling the messages with no contention.
 *
 * @param: head of the message queue
 * @param: message to queue or address where to pop
 * @return: none
 */

void msg_push(_Atomic (struct node *) *head, struct node *work)
{
	unsigned long spin = 0;
	work->next= LOAD(head[0]);
	while (!XCHG(head[0], work->next, work))
		spin++;
	spin_add(spin, "msg_push");
}

void  msg_pop(_Atomic (struct node *) *head, struct node **work)
{
	*work= AND(head[0], 0);
}



/*
 * @brief bsearch callback to compare long names options
 *
 * @param pointer on key
 * @param pointer on a member of the search elements
 * @return <0 >0 or ==0 according to order of key/element
 */
static int long_names_cmp(const void *k, const void *m)
{
	unsigned char c1, c2;
	unsigned const char *p = *(unsigned char **)m;
	unsigned const char *l = k;

	do {
		c1 = *l++;
		c2 = *p++;
		if ('=' == c2 || '\0' == c2)
			return ('\0' == c1) ? 0 : 1;
	} while (c1 == c2);

	return (c1 < c2) ? -1 : 1;
}

/*
 * @brief Expands arguments from the command line
 *
 * This is needed since fuse_opt_parse does not understand GNU style combined
 * single letters arguments. For example, this will transform
 *  	-JOLl7
 * into
 * 	-J -O -L -l7
 *
 * Also, fuse_opt_parse is picky: the function handles long arguments so that
 * --arg value, is accepted as --arg=value.
 * This expansion is not done for "fuse-like" arguments listed after -o, since
 * it would not make sense. They MUST be specified as: -o arg=value
 *
 * This function uses its parameter in/out initialized as argc/argv in input.
 * If there is no expansion to do, the param remains unchanged.
 * When expansion is needed, the count is changed accordingly and memory is
 * allocated in one chunk for both the new *argv[] structure and the new
 * arguments strings space. The caller must then free argv when done with
 * newly allocated argument strings.
 *
 * @param (in/out) fuse_args struct pointer
 * @param array of long names that call for a second argument
 * @param number of elements in the array of long names
 * @param in one string, short arguments that call for a second argument
 * @return none
 */
void expand_args(struct fuse_args *args,
		 const char **long_names, size_t n_long_names,
		 const char *end_expand)
{
	unsigned int i, j, k, m, argc;
	int new_args;
	char **argv, *p;
	size_t mem_needed;

	new_args = 0;
	mem_needed = 0;
	argc = args->argc;
	argv = args->argv;
	for (i=1; i < argc; i++) {
		if ('-' != argv[i][0] || '\0' == argv[i][0])
			continue;
		if ('-' == argv[i][1] ) {
			if ('\0' == argv[i][2])
				break;
			if ('=' == argv[i][strlen(argv[i]) -1])
				lprintf(LOG_ERR,
					"`%s` is not a valid option.\n",
					argv[i]);
			if ( i < argc - 1 &&
			     NULL != bsearch(argv[i] + 2, long_names,
					     n_long_names, sizeof(char *),
					     long_names_cmp)) {
				new_args--;
				mem_needed += strlen(argv[i]) +
					      strlen(argv[i + 1]) + 2;
				i++;
			}
			continue;
		}
		if ('\0' == argv[i][1])
			lprintf(LOG_ERR, "`-` is not a valid option.\n");
		for (j=1; '\0' != argv[i][j + 1]; j++) {
			if (NULL != strchr(end_expand, argv[i][j]))
				break;
			new_args++;
			mem_needed += 3;
		}
		if (j > 1) {
			if ( '\0' == argv[i][j+1] )
				mem_needed +=  3;
			else
				mem_needed += strlen(argv[i]+j) + 2;
		}
	}

	if (0 == mem_needed)
		return;

	args->argc += new_args;
	args->argv = fs_alloc(sizeof(char *) * (argc + new_args) + mem_needed);
	args->allocated= true;
	args->argv[0]= argv[0];
	p=(char *)(args->argv + args->argc);
	for (i = 1, j = 1; i < argc; i++, j++) {
		args->argv[j]= argv[i];
		if ('-' != argv[i][0] || '\0' == argv[i][0])
			continue;
		if ('-' == argv[i][1]) {
			if ('\0' == argv[i][2]) {
				break;
			}
			if ( i < argc - 1 &&
			     NULL != bsearch(argv[i] + 2, long_names,
					     n_long_names, sizeof(char *),
					     long_names_cmp)) {
				args->argv[j] = p;
				m = strlen(argv[i]);
				memcpy(p, argv[i], m);
				p += m;
				*p = '=';
				p++;
				i++;
				m = strlen(argv[i]);
				strcpy(p, argv[i]);
				p += m + 1;
			}
			continue;
		}
		if ('\0' == argv[i][2])
			continue;
		for (k=1, j--; '\0' != argv[i][k]; k++) {
			j++;
			if (NULL != strchr(end_expand, argv[i][k])) {
				if (k > 1) {
					args->argv[j] = p;
					*p = '-';
					p++;
					m = strlen(argv[i] + k) + 1;
					memcpy(p, argv[i] + k, m);
					p += m;
				}
				break;
			}
			args->argv[j] = p;
			p[0] = '-';
			p[1] = argv[i][k];
			p[2] = '\0';
			p += 3;
		}
	}
	for (; i < argc; i++, j++)
		args->argv[j] = argv[i];
}


/*
 * @brief checks the mount directory path
 *
 * Note: strerror is not thread safe, but more portable than strerror_r, and
 *       since this is used before daemonizing and multithreading, there is
 *       no race condition here.
 *
 * @param path of the mountpoint to check
 * @return none
 */
void check_mount_path(const char *path)
{
	if (NULL == path)
		lprintf(LOG_ERR, "no mountpoint path specified.\n");
	if (-1 == stat(path, &params.mount_st)) {
		lprintf(LOG_ERR , "getting stats of `%s`: %s\n"
				, path, strerror(errno));
	}
	if ( ! S_ISDIR(params.mount_st.st_mode) )
		lprintf(LOG_ERR, "`%s` is not a directory\n",  path);
	params.mount_st.st_nlink = 2;
}


/*
 * @brief logs the start date-time and start message
 *
 * Note: main should start with clock_gettime to pass the start time.
 *
 * @param pointer to time_t representing the start date
 * @return none
 */
void start_message(time_t *t)
{
	struct tm local_time;
	char buf[80];
	if (NULL == localtime_r(t, &local_time))
		lprintf(LOG_CRIT, "error %d on localtime.\n", errno);
	if (0 == strftime(buf, sizeof(buf), "%A %e %B %Y at %X",
			  &local_time))
		lprintf(LOG_CRIT, "error on strftime.\n", errno);
	lprintf(LOG_NOTICE, "started: %s\n", buf);
	lprintf(LOG_INFO, "successfuly parsed arguments.\n");
}

/*
 * @brief extracts an unsigned integer from an argument string
 *
 * @param the argument string
 * @return the integer (unsigned 64 bits)
 */
uint64_t read_int(const char *arg)
{
	uint64_t i = 0;
	const char *p;

	if ('-' == *arg && '-' != *(arg +1))
		arg += 2;
	else
		for (; '\0' != *arg; arg++)
			if ('=' == *arg) {
				arg++;
				break;
			}
	for(p = arg; isdigit(*p) && '\0' != *p; p++) {
		/* Max uint64 is 18446744073709551615 (=2^64 -1).
		 * This is the overflow test here. */
		if ( 1844674407370955161 <  i ||
		    (1844674407370955161 == i && '5' < *p ))
			lprintf(LOG_ERR,
				"Number overflow: %s\n", arg);
		i = i*10 + *p - '0';
	}
	if ('\0' != *p)
		lprintf(LOG_ERR,
			"malformed argument. Expecting number, got: %s\n", arg);
	return i;
}

/*
 * @brief extracts and stores a string from a long argument, or -o fuse form.
 *
 * It does also free a previously allocated parameter in case we repeat
 * multiple times the same option.
 *
 * @param the argument string
 * @param where to store it
 * @return none
 */
void read_str(const char *arg, char **where)
{
	char *p;

	p = strchr(arg, '=');
	if (NULL == p)
		lprintf(LOG_ERR, "malformed argument: %s\n", arg);
	fs_free(*where);
	*where = fs_alloc(strlen(p));
	strcpy(*where, p + 1);
}


/*
 * ***************************************************************************
 * From there starts the "stream engine"
 */


/*
 * @brief copy data from one buffer to the other when there is an overlap.
 *
 * @param destination buffer address
 * @param destination buffer offset
 * @param destination buffer size
 * @param source buffer address
 * @param source buffer offset
 * @param source buffer size
 * @return size of data copied (or zero when no overlap)
 */
size_t  move_buf(char *dst, off_t doff, size_t dsz,
		 char *src, off_t soff, size_t ssz)
{
	size_t chunk;

	if (doff >= soff && doff < soff + ssz) {
		if (doff + dsz > soff + ssz)
			chunk = soff + ssz - doff;
		else
			chunk = dsz;
		memcpy(dst, src + doff - soff, chunk);
		return chunk;
	}
	return 0;
}


/*
 * @brief inserts a received work buffer into the private reader's queue.
 *
 * First fill the received buffered with already queued buffer's data.
 *
 * Then the buffer is inserted ordered by offsets and sizes. If the internal
 * buffer was replaced by a received buffer of the exact same characteristics,
 * it is not necessary anymore hence removed.
 *
 * @param address of the head pointer
 * @param the received work buffer
 * @param the internal work buffer
 * @return none
 */
void prv_insert(struct node **head, struct node *rcv, struct node *internal)
{
	struct node **where, *n;

	for (n = *head; NULL != n && 0 != n->size; n = n->next)
		if (0 != n->written)
			rcv->written += move_buf(rcv->buf + rcv->written,
						 rcv->offset + rcv->written,
						 rcv->size - rcv->written,
						 n->buf,
						 n->offset,
						 n->written);

	for (where = head, n = *head;
	     NULL != n &&
	     ( n->offset  < rcv->offset ||
	      (n->offset == rcv->offset && n->size < rcv->size));
	     where = &n->next, n = n->next);

	if (n == internal && n->size == rcv->size) {
		rcv->next= n->next;
		DEBUG("prv_insert: removed internal buffer %"PRIu64":%lu-%lu\n",
		      internal->offset,
		      internal->written,
		      internal->size);
	}
	else {
		rcv->next= n;
	}
	*where = rcv;
}

/*
 * @brief fills the queued buffers with a buffer read from the server.
 *
 * @param reader's address
 * @param buffer's address
 * @param buffer's offset in the current stream (absolute offset)
 * @param buffer's size
 * @return true if one of the queued buffers is full, false otherwise
 */
bool fill_buf(struct reader *r, char * buf, off_t offset, size_t size)
{
	struct node *n;
	bool prune = false;

	if (0 == size)
		return false;
	for (n = r->prv; NULL != n && 0 != n->size; n = n->next) {
		if (buf == n->buf)
			continue;
		n->written +=  move_buf(n->buf + n->written,
					n->offset + n->written,
					n->size - n->written,
					buf, offset, size);
		if (n->written == n->size)
			prune = true;
	}
	return prune;
}


extern void update_speed_stats(bool read, int written, unsigned int i);

/*
 * @brief prune full buffers from the reader's private queue.
 *
 * The previous function (fill_buf) gives a hint whether the prune_queue
 * function can be called to return some buffers.
 *
 * @param reader's address
 * @param the internal work buffer
 * @return none
 */
void prune_queue(struct reader *r, struct node *internal)
{
	struct node **n, *tmp;
	n = &r->prv;
	while (NULL != *n)
		if ((*n)->written == (*n)->size) {
			tmp = (*n)->next;
			if (internal != *n) {
				sem_post(&(*n)->sem_done);
				update_speed_stats(true,
						   (*n)->written,
						   r - readers);
			}
			*n = tmp;
		}
		else {
			n = &(*n)->next;
		}
}

/*
 * @brief curl callback for single range reading
 *
 * @param pointer to data read from the URL
 * @param size of member
 * @param number of members
 * @param userdata pointer (node structure here)
 * @return number of bytes read (should be size unless error or end of file)
 */
size_t single_range(char *ptr,
		    size_t size,
		    size_t nmemb,
		    struct node *n)
{
	size_t s;
	s = size * nmemb;
	if (n->written + s > n->size) {
		lprintf(LOG_WARNING,
			"unexpected read past end of range: s=%lu %lu:%lu\n",
			s, n->written, n->size);
		s = n->size - n->written;
	}
	memcpy(n->buf + n->written, ptr, s);
	n->written += s;
	return s;
}

/*
 * @brief init callback for fuse
 *
 * Initialize the reader threads.
 *
 * @param see fuse documentation
 * @return none
 */
void *astreamfs_init(struct fuse_conn_info *conn)
{
	struct fuse_context *context;
	int i, err;
	extern struct reader readers[];
	extern void *async_reader(void *arg);

	for (i=0; i< MAX_READERS; i++) {
		lprintf(LOG_NOTICE, ">> starting thread reader[%d].\n", i);
		STORE(readers[i].rcv, NULL);
		readers[i].prv = NULL;
		readers[i].s = NULL;
		readers[i].start = 0;
		readers[i].end = 0;
		readers[i].last_rq_id = 0;
		readers[i].n_rq = 0;
		sem_init(&readers[i].sem_go, 0, 0);
		readers[i].idle = true;
		readers[i].streaming = false;
		readers[i].curl = NULL;
		err = pthread_create(&readers[i].thread,
				     NULL,
				     async_reader,
				     &readers[i]);
		if (0 != err)
			lprintf(LOG_CRIT, "creating reader thread[%d]: %d\n"
					, i, err);
	}
	context = fuse_get_context();
	return context->private_data;
}

