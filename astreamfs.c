/*
 * AStreamFS: stands for Asynchronous Stream Filesystem (fuse).
 *	      It mounts a set of URLs so that they can be accessed/streamed
 *	      as if it were local read-only local files.
 *
 * Copyright (C) 2018-2020  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Feature:
 *	This filesystem mounts URLs as readonly files in the mount directory.
 *	The only thing it needs, is for the server to handle ranges.
 *	Note that there was already httpfs/httpfs2
 *	The implementation of it was simple an straightforward: it did a
 *	http GET range offset-(offset+size) for each read request.
 *	It works, but has a lot of overhead, even with kernel buffering.
 *	The trouble with that simple approach comes with the generalisation
 *	of https these days (2018), when the server closes the	connection,
 *	not only you have the mentioned overhead, but you also	get to do
 *	the TLS handshake. On some test servers I use, this comes close
 *	to 1 second overhead for each read, which made httpfs unusable
 *	for more than very small tasks.
 *
 *	What this implementation does instead, is that it tries its best to
 *	"stream" access to the http(s) resource. This correspond in fact to
 *	a majority of cases like video, music, uncompressing a rar/zip, etc...
 *	Even the use case quoted by https: reading files in a iso, generally
 *	involves random access to locate that file followed by (hopefully)
 *	sequential access on the file.
 *
 *	On the case you really need random access, there is little thing that
 *	this implementation can do better than httpfs (it could actually be
 *      a little worse because closing streams takes time) and should that be
 *	your case, you would probably be better of downloading the file to
 *	access it locally for more than small tasks.
 *
 *	On top of this "stream" orientation, a few useful options are also
 *	implemented, borrowing from the curl CLI tool when it made sense, like
 *	following locations, etc...
 *	See the help for more details.
 *
 * Architecture:
 *	It works via workers or "reader threads" (readers for short).
 *
 *	An active reader has current buffers it is filling from a stream. These
 *      buffers are consecutive with at most 128k "holes", thus min/max offset.
 *
 *	When a reader has no more things to do it waits at the offset of the
 *	last buffer that was responded.
 *
 * 	After some time the reader stops waiting and goes back to idle state.
 *
 * 	When a request arrives astreamfs looks in order of preference:
 *	- if the buffer received goes inside a (min/)max or just after max
 *	  with a possible "hole" of 128k.
 *	- if there is an idle worker.
 *	If none of that is found, it does a single range request (not streamed).
 *
 *	Locking:
 *	Fuse incoming reads lock the file so that they read a consistent state.
 *	Readers lock only when they send back full buffers.
 *	The buffer exchange between fuse threads and readers is done via a pair
 *	symetric semaphores.
 *
 *	The 128k gap mentioned above is to take into account parallelism from
 *	the kernel that can send streamed blocks in "reversed" order. If a
 *	reader goes into a "hole" it uses its internal 128k buffer, which can
 *	be used to fill late reversed blocks.
 *
 *
 *	Worker nodes that encapsulate the read requests are not allocated,
 *	since we need only one buffer for each fuse thread, and one buffer for
 *	each async reader, we use a thread local variable.
 *
 * Usage:
 *	Either
 *		read the help provided by: `astreamfs -h` or
 *		look at the help section below, under astreamfs_opt_proc()
 *
 * Compile with:
 *      cc -Wall astreamfs.c $(pkg-config fuse --cflags --libs)\
			   $(curl-config --cflags --libs)\
			   -o astreamfs -lpthread
 *
 * Compile dependancy:
 *	curl and fuse library
 *
 * Version: 0.9.1.1
 *
 * History:
 * 	2020/03/17: 0.9.1.1 Fix: buf queue (prv_ins) + Copyright to 2020
 * 	2019/06/22: 0.9.1 backport cleaner get http_code (hd_chk) & check size
 * 	2019/06/08: 0.9.0 New: added curl options --insecure and --cacert
 * 	2019/04/28: 0.8.6 Fix stat's st_block, subtype param, algo simplified.
 * 	2019/02/14: 0.8.5 Backport from 1fichierfs: fix 32bits + common code.
 * 	2018/12/29: 0.8.4 Better logging + replaced GCC atomics by C11 standard.
 * 	2018/12/16: 0.8.3 Cleaning: moved common code to astreamfs_util.c/h
 *	2018/12/01: 0.8.2 Fix some bugs with parameters handling
 *	2018/11/24: 0.8.1 Support user:password basic authentication (-u --user)
 *	2018/03/25: 0.8.0 Full path support (incl. relative) + minor optim.
 *	2018/03/24: 0.7.9 Canonicalize and deduplicate paths
 *	2018/03/24: 0.7.8 Log to a file option (prevent syslog spamming!)
 *	2018/03/17: 0.7.7 Bump for packaging
 *	2018/03/12: 0.7.5 First path implementation
 *	2018/03/10: 0.7.4 More work on argument management
 *	2018/03/08: 0.7.3 Project renamed: avoid confusion with existing httpfs2
 *	2018/03/06: 0.7.2 Better multi-file implementation
 *	2018/03/04: 0.7.1 Added options passed by /etc/fstab to click-mount!
 *	2018/03/03: 0.7.0 First incomplete implementation of multiple files
 *	2018/02/18: 0.6.2 Bug fix on close requests
 *	2018/02/17: 0.6.1 File modification time, single ranges, more comments.
 *	2018/02/16: 0.6.0 Version with locks, it became too complex and bug
 *			  prone without locking! We might look into it later.
 *	2018/02/11: 0.5.5 Simplified algorithm: removed wait state.
 *	2018/02/10: 0.5.4 Several bug fix
 * 	2018/02/10: 0.5.1 Implemented timeouts, better streaming algorithm
 * 	2018/01/31: 0.5.0 Initial version
 *
 *
 */

#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stddef.h>

#define STREAM_STRUCT struct filespec
#include "astreamfs_util.h"

#define PROG_NAME	"astreamfs"
#define PROG_VERSION	"0.9.1.1"


#define MAX_READERS 4
#define KBUF 131072

#define ST_BLK_SZ 512 /* This is the block size stat's st_blocks */

/* Global immutable constant strings for various use */
       const char log_prefix[]		= PROG_NAME;
static const char default_user_agent[]	= PROG_NAME"/"PROG_VERSION;
static const char default_filename[]	= "file";
static const char ro_opt[]		= "-oro,fsname=";
static const char subtype[]		= ",subtype=";
static const char def_type[]		= PROG_NAME;

#define MSG_ERR_CONTENT_RANGE "malformed Content-Range header.\n"
static char content_range_header[]="Content-Range:";
#define S_CONTENT_RANGE_HEADER sizeof(content_range_header) -1

#define MSG_ERR_CONTENT_DISPOSITION "malformed Content-Disposition header.\n"
static char content_disposition_header[]="Content-Disposition:";
#define S_CONTENT_DISPOSITION_HEADER sizeof(content_disposition_header) -1
static char attachment_part[]="attachment";
#define S_ATTACHMENT_PART sizeof(attachment_part) -1
static char inline_part[]="inline";
#define S_INLINE_PART sizeof(inline_part) -1
static char filename_part[]="filename=\"";
#define S_FILENAME_PART sizeof(filename_part) -1
static char location_header[]="Location:";
#define S_LOCATION_HEADER sizeof(location_header) -1

/*
 *  Global variables set during the initialisation phase then immutable.
 */

static unsigned long count_URL = 0;
static unsigned long count_path = 1;
static mode_t st_mode;

/*****************************************/

#define FL_GOT_NAME		    1
#define FL_GOT_SIZE		    2
#define FL_GOT_TIME		    4
#define FL_LOCATION		    8
#define FL_KEEP_LOCATION	   16
#define FL_REMOTE_NAME		   32
#define FL_REMOTE_HEADER_NAME	   64
#define FL_REMOTE_TIME		  128
#define FL_INSECURE		  256

#define FL_IS_INIT		  512
#define FL_NO_RANGE		 1024

#define F_GOT_NAME(fs)		 (0 != ((fs)->flags & FL_GOT_NAME))
#define F_GOT_SIZE(fs)		 (0 != ((fs)->flags & FL_GOT_SIZE))
#define F_GOT_TIME(fs)		 (0 != ((fs)->flags & FL_GOT_TIME))
#define F_LOCATION(fs)		 (0 != ((fs)->flags & FL_LOCATION))
#define F_KEEP_LOCATION(fs)	 (0 != ((fs)->flags & FL_KEEP_LOCATION))
#define F_REMOTE_NAME(fs)	 (0 != ((fs)->flags & FL_REMOTE_NAME))
#define F_REMOTE_HEADER_NAME(fs) (0 != ((fs)->flags & FL_REMOTE_HEADER_NAME))
#define F_REMOTE_TIME(fs)	 (0 != ((fs)->flags & FL_REMOTE_TIME))
#define F_INSECURE(fs)		 (0 != ((fs)->flags & FL_INSECURE))

#define F_IS_INIT(fs)		 (0 != ((fs)->flags & FL_IS_INIT))
#define F_NO_RANGE(fs)		 (0 != ((fs)->flags & FL_NO_RANGE))

struct filespec {
	char         *URL;
	unsigned int  flags;
	unsigned int  location_count;
	char         *location;
	char         *filename;
	char         *path;
	char	     *userpwd;
	long          curl_IP_resolve;
	unsigned int  curl_opt_keepalive_time;
	unsigned int  idle_time;
	unsigned int  keep_location_time;
	unsigned int  nb_open;
	struct stat   st;
	unsigned long last_rq_id;
	struct timespec start;
	pthread_mutex_t fastmutex;
	pthread_mutex_t curlmutex;
};

static struct filespec *fss = NULL;
static char **paths = NULL;

static __thread struct node work = {NULL,NULL};

struct reader readers[MAX_READERS];

struct parse_data {
	unsigned long cur_URL;
	bool new_args;
	bool got_URL;
	const char *last_noopt;
	char *cur_abs_path;
	size_t path_sz;
};


static struct filespec *find_file(const char *path)
{
	unsigned int i;
	for (i = 0; i < count_URL; i++)
		if (0 == strncmp(path, fss[i].path, strlen(fss[i].path)) &&
		    0 == strcmp(path + strlen(fss[i].path), fss[i].filename))
			return &fss[i];
	return NULL;
}

static char *find_dir(const char *path)
{
	unsigned int i;
	size_t sz = strlen(path);
	int cmp;

	for (i = 0; i < count_path; i++) {
		cmp = strncmp(path, paths[i], sz);
		if (0 == cmp)
			return paths[i];
		if (cmp < 0)
			break;
	}
	return NULL;
}

static int astreamfs_getattr(const char *path, struct stat *stbuf)
{
	struct filespec *fs;
	char *p;

	lprintf(LOG_DEBUG,"getattr: `%s`\n", path);

	memset(stbuf, 0, sizeof(struct stat));
	p = find_dir(path);
	if (NULL != p) {
		memcpy(stbuf, &params.mount_st, sizeof(struct stat));
	}
	else {
		fs = find_file(path);
		if (NULL == fs)
			return -ENOENT;
		memcpy(stbuf, &fs->st, sizeof(struct stat));
	}

	return 0;
}

static int astreamfs_readdir(const char *path, void *buf,
			     fuse_fill_dir_t filler, off_t offset,
			     struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;
	unsigned int i, j;
	int cmp;
	size_t s, s_prev;
	char *p, *q, *prev = NULL;

	lprintf(LOG_DEBUG,"readdir: `%s` %u\n", path, offset);

	p = find_dir(path);
	if (NULL == p)
		return -ENOENT;

	if (0 != filler(buf, ".", &params.mount_st, 0))
		return 1;
	if (p == paths[0]) {
		if (0 != filler(buf, "..", NULL, 0))
			return 1;
	}
	else {
		if (0 != filler(buf, "..", &params.mount_st, 0))
			return 1;
	}

	s = strlen(path);
	j = (0 == strcmp(path, "/")) ? 0 : 1;
	s_prev = 0;
	for (i = 0; i < count_path; i++) {
		cmp = strncmp(path, paths[i], s);
		if (0 == cmp) {
			q = strchr(paths[i] + s + j, '/');
			if (NULL == q ||
			    (s_prev == q - paths[i] - s - j &&
			     0 == strncmp(prev, paths[i] + s + j, s_prev)))
				continue;
			s_prev = q - paths[i] - s - j;
			prev = paths[i] + s + j;
			char dir[s_prev + 1];
			memcpy(dir, prev, s_prev);
			dir[s_prev] = '\0';
			if (0 != filler(buf, dir, &params.mount_st, 0))
				return 1;
		}
		if (cmp < 0)
			break;
	}
	for (i = 0; i < count_URL; i++)
		if (fss[i].path == p) {
			if (0 != filler(buf, fss[i].filename, &fss[i].st, 0))
				return 1;
		}
	return	0;
}

static int astreamfs_open(const char *path, struct fuse_file_info *fi)
{
	struct filespec *fs;

	fs = find_file(path);
	if (NULL == fs)
		return -ENOENT;

	if ((fi->flags & 3) != O_RDONLY || F_NO_RANGE(fs))
		return -EACCES;

	lock(&fs->curlmutex, NULL);
	fs->nb_open++;
	unlock(&fs->curlmutex, NULL);
	fi->fh = (uintptr_t)fs;
	lprintf(LOG_INFO, ">> Opening (%d).\n", fs->nb_open);
	return 0;
}

static int astreamfs_release(const char *path, struct fuse_file_info *fi)
{
	unsigned int i;
	struct filespec *fs;
	bool locked = false;

	fs = (struct filespec *)((uintptr_t)(fi->fh));

	lprintf(LOG_INFO, ">> Releasing (%d).\n", fs->nb_open);
	lock(&fs->curlmutex, NULL);
	fs->nb_open--;
	work.size = 0;
	if (0 == fs->nb_open) {
		for (i=0; i< MAX_READERS; i++) {
			lock(&fs->fastmutex, &locked);
			if (!readers[i].idle && readers[i].s == fs) {
				msg_push(&readers[i].rcv, &work);
				sem_post(&readers[i].sem_go);
				unlock(&fs->fastmutex, &locked);
				sem_wait(&work.sem_done);
			}
		}
		unlock(&fs->fastmutex, &locked);
	}
	unlock(&fs->curlmutex, NULL);

	return 0;
}

static void get_work(struct reader *r)
{
	int err;
	struct timespec until;
	struct node *tmp, *rcv;


	if (r->idle || 0 == r->s->idle_time || NULL != r->rcv) {
		lprintf(LOG_DEBUG,
			"get_work[%d] sem_wait\n", r - readers);
		err= sem_wait(&r->sem_go);
	}
	else {
		clock_gettime(CLOCK_REALTIME, &until);
		until.tv_sec += r->s->idle_time;
		lprintf(LOG_DEBUG,
			"get_work[%d] timedsem: %lu %lu\n",
			r - readers, until.tv_sec, until.tv_nsec );
		err= sem_timedwait(&r->sem_go, &until);
		if ( ETIMEDOUT == errno ) {
			lprintf(LOG_DEBUG,
				"get_work[%d]  timedout\n",
				r - readers);
			return;
		}
	}
	if ( 0 != err ) {
		lprintf(LOG_CRIT,
			"get_work[%d] sem_wait error: %d.\n",
			r - readers, err);
	}

	msg_pop( &r->rcv, &rcv );
	lprintf(LOG_DEBUG,
		"get_work[%d] got: %p\n",
		r - readers, rcv);
	while(1) {
		lprintf(LOG_DEBUG,
			"get_work[%d] poped: %p->%p %"PRIu64":%lu\n",
			r - readers, rcv, rcv->next, rcv->offset, rcv->size);
		tmp = rcv->next;
		if (0 == rcv->size) {
			rcv->next = r->prv;
			r->prv = rcv;
			if ( NULL != tmp ) {
				lprintf(LOG_CRIT,
					"get_work[%d] there should NOT be another request after close: %p %"PRIu64":%lu-%lu\n",
					r - readers, tmp, rcv->offset,
					rcv->written, rcv->size);

			}
			break;
		}
		else {
			prv_insert(&r->prv, rcv, &work);
			if ( NULL == tmp )
				break;
		}
		lprintf(LOG_DEBUG,
			"get_work[%d] additional sem_wait, rcv=%p\n",
			r - readers, tmp);
		sem_wait(&r->sem_go);
		rcv = tmp;
	}
}

#include "astream_engine.c"

struct fs_loc {
	struct filespec *fs;
	bool  locked;
	long  http_status_code;
	CURL *curl;
	void *userdata;
	curl_read_callback callback;
};

static size_t header_get_location(char *buffer, size_t size,
				  size_t nitems, struct fs_loc *fsl)
{
	size_t s = nitems * size;
	char *p, *q, *r;

	/*
	 * Once we have the location from the header we stop the transfer.
	 */
	if (s > S_LOCATION_HEADER &&
	    0 == strncasecmp(buffer, location_header, S_LOCATION_HEADER)) {
		for (p= buffer + S_LOCATION_HEADER;
		     p < buffer + s && *p ==' ' ;
		     p++);
		q = memchr(p, '\x0d', s - (p - buffer) );
		if ( NULL == q )
			lprintf( LOG_CRIT, "malformed Location header.\n");
		r = fs_alloc( q - p + 1 );
		memcpy(r, p, q - p);
		r[q - p] = '\0';
		free(fsl->fs->location);
		fsl->fs->location = r;
		fsl->fs->location_count ++;
		lprintf(LOG_DEBUG, "got a new location (%d): %s\n"
				 , fsl->fs->location_count, r);
		return 0;
	}

	return s;
}

static size_t header_get_details(char *buffer, size_t size,
				 size_t nitems, struct fs_loc *fsl)
{
	struct filespec *fs = fsl->fs;
	size_t s = nitems * size, i;
	unsigned long long file_size;
	char *p, *q, *r;

	/*
	 * Get the file size from the Content-Range header.
	 * Unlike the name we read only once (even if it changes), we
	 * check the size even if we have it. If it has changed: error.
	 */
	if (s > S_CONTENT_RANGE_HEADER &&
	    0 == strncasecmp( buffer, content_range_header
				    , S_CONTENT_RANGE_HEADER ) ) {
		p= memchr(buffer + S_CONTENT_RANGE_HEADER, '/', s);
		if ( NULL == p ) {
			lprintf(LOG_ERR, MSG_ERR_CONTENT_RANGE);
			fsl->http_status_code = 0;
			return 0;
		}
		file_size= 0;
		for ( p++; p < buffer + s && *p !='\x0d' ; p++ ) {
			if ( isdigit(*p) ) {
				file_size= 10 * file_size + *p - '0';
			}
			else {
				lprintf(LOG_ERR, MSG_ERR_CONTENT_RANGE);
				fsl->http_status_code = 0;
				return 0;
			}
		}
		if (F_GOT_SIZE(fs)) {
			if (fs->st.st_size != file_size) {
				lprintf(LOG_ERR,
					"file size changed from %"PRIu64" to %"PRIu64" for %s\n",
					fs->st.st_size,
					file_size,
					fs->URL);
				fsl->http_status_code = 0;
				return 0;
			}
		}
		else {
			fs->st.st_size= file_size;
			fs->st.st_blocks= file_size / ST_BLK_SZ;
			fs->flags |= FL_GOT_SIZE;
		}
		return s;
	}

	/*
	 * Get the remote filename from the Content-Disposition header, when
	 * we have -JO option (RFC6266). If not found we will use default names.
	 */
	if (!F_GOT_NAME(fs) && s > S_CONTENT_DISPOSITION_HEADER &&
	    0 != (fsl->fs->flags & FL_REMOTE_HEADER_NAME) &&
	    0 == strncasecmp(buffer, content_disposition_header
	    			   , S_CONTENT_DISPOSITION_HEADER)) {
		for (i = S_CONTENT_DISPOSITION_HEADER;
		     i < s && ' ' == buffer[i];
		     i++);
		if (!(s - i > S_ATTACHMENT_PART &&
		      0 == strncasecmp(buffer + i, attachment_part,
				       S_ATTACHMENT_PART)) &&
		    !(s - i > S_INLINE_PART &&
		      0 == strncasecmp(buffer + i, inline_part,
				       S_INLINE_PART))) {
			lprintf(LOG_WARNING, MSG_ERR_CONTENT_DISPOSITION);
			return s;
		}
		for (; i < s && ';' != buffer[i]; i++);
		for (i++; i < s && ' ' == buffer[i]; i++);

		if (!(s - i > S_FILENAME_PART &&
		      0 == strncasecmp(buffer + i, filename_part,
				       S_FILENAME_PART))) {
			lprintf(LOG_WARNING, MSG_ERR_CONTENT_DISPOSITION);
			return s;
		}

		/* Count bytes for allocation */
		p = buffer + i + S_FILENAME_PART;
		for (q = p; q < buffer + s && *q !='"' ; q++) {
			if ( '\\' == *q ) q++;
		}
		if (buffer + s == q) {
			lprintf(LOG_WARNING, MSG_ERR_CONTENT_DISPOSITION);
			return s;
		}

		/* Allocate, copy, and sanitize potential '/' */
		free((void *)fs->filename);
		r = fs_alloc( q - p + 1 );
		fs->filename = r;
		fs->flags |= FL_GOT_NAME;
		for (q = p; *q !='"' ; q++, r++) {
			if ( '\\' == *q )
				q++;
			*r = ( '/' == *q ) ? '_' : *q ;
		}
		*r='\0';
	}
	return s;
}


static size_t header_check(char *buffer, size_t size,
                           size_t nitems, struct fs_loc *fsl)
{
	struct filespec *fs = fsl->fs;
	size_t s = nitems * size;

	/*
	 * The first header MUST be the http status code otherwise this is
	 * a protocol error. Thus we just use standard curl call here and
	 * expect to get the http status code at this first call.
	 */

	CURL_EASY_GETINFO(fsl->curl, CURLINFO_RESPONSE_CODE,
			  &fsl->http_status_code);
	switch(fsl->http_status_code) {
		case HTTP_PARTIAL_CONTENT :
				CURL_EASY_SETOPT(fsl->curl,
						 CURLOPT_HEADERFUNCTION,
						 header_get_details,
						 "%p");
				return s;
		case HTTP_MOVED_PERMANENTLY  :
		case HTTP_FOUND		     :
		case HTTP_SEE_OTHER	     :
		case HTTP_TEMPORARY_REDIRECT :
		case HTTP_PERMANENT_REDIRECT :
				if  (!F_LOCATION(fs)) {
					lprintf(LOG_WARNING,
						"the server returned %ld but you did not specify -L for %s\n",
						fsl->http_status_code, fs->URL);
					break;
				}
				CURL_EASY_SETOPT(fsl->curl,
						 CURLOPT_HEADERFUNCTION,
						 header_get_location,
						 "%p");
				fsl->http_status_code = 300;
				return s;
		case HTTP_OK :
				lprintf(LOG_ERR,
					"the server does not support ranges for %s\n",
					fs->URL);
				break;
		default :
				lprintf(LOG_ERR,
					"protocol error or unexpected return code (%ld) for %s\n",
					fsl->http_status_code, fs->URL);
				break;
	}
	fsl->http_status_code = 0;
	return 0;
}


static size_t write_first(char *ptr,
			  size_t size,
			  size_t nmemb,
			  struct fs_loc *fsl)
{
	if (206 != fsl->http_status_code) {
		lprintf(LOG_WARNING,
			"probably malformed redirect without location.\n");
		return 0;
	}
	if (!F_GOT_TIME(fsl->fs) && F_REMOTE_TIME(fsl->fs)) {
		long filetime;
		int e;
		e = curl_easy_getinfo(fsl->curl, CURLINFO_FILETIME, &filetime);
		if((CURLE_OK == e) && (filetime >= 0)) {
			fsl->fs->st.st_mtim.tv_sec = filetime;
			fsl->fs->st.st_ctim.tv_sec = filetime;
		}
		else {
			lprintf(LOG_WARNING,
				"could not get file modification time.\n");
		}
		fsl->fs->flags |= FL_GOT_TIME;
	}
	if (!params.noatime)
		clock_gettime(CLOCK_REALTIME, &fsl->fs->st.st_atim);
	fsl->fs->flags |= FL_IS_INIT;
	unlock(&fsl->fs->curlmutex, &fsl->locked);
  	CURL_EASY_SETOPT(fsl->curl, CURLOPT_WRITEFUNCTION, fsl->callback, "%p");
  	CURL_EASY_SETOPT(fsl->curl, CURLOPT_WRITEDATA, fsl->userdata, "%p");
  	return fsl->callback(ptr, size, nmemb, fsl->userdata);
}

/*
 * @brief stream (and single range) start
 *
 * This function does the actual reading of streams (and ranges). It does also
 * take care of following redirects (if instructed with -L), and initialises
 * files size and modification date, and check that we have "ranges".
 * This is done with the help of the callbacks above.
 *
 * It is the only place where we use curl_easy_perform.
 *
 * We lock when we read an URL for the first time and initialise time and date,
 * and also when we need to renew a redirect (see keep-location-time), to avoid
 * a race condition between several threads trying to get a new location.
 *
 * @param none
 * @return the initialised curl handle
 */

#define STREAM_ERROR ((CURLcode)99) /* CURL errors stop at 90 */

static CURLcode stream_fs(CURL *curl,
			  struct filespec *fs,
			  curl_read_callback callback,
			  void *userdata)
{
	CURLcode res;
	const char *url;
	struct fs_loc fsl;
	unsigned int count;
	struct timespec now;
	bool use_location;

	count = fs->location_count;
	fsl.callback = callback;
	fsl.userdata = userdata;
	fsl.curl = curl;
	fsl.fs = fs;
	fsl.locked = false;
	CURL_EASY_SETOPT(curl, CURLOPT_HEADERDATA, &fsl, "%p");

	use_location = false;
	url = fs->URL;
	if (!F_IS_INIT(fs)) {
		lock(&fs->curlmutex, &fsl.locked);
		if (F_KEEP_LOCATION(fs))
			clock_gettime(CLOCK_REALTIME, &fs->start);
	}
	else {
		if (F_KEEP_LOCATION(fs)) {
			lock(&fs->curlmutex, &fsl.locked);
			clock_gettime(CLOCK_REALTIME, &now);
			if ( fs->start.tv_sec + fs->keep_location_time <
			     now.tv_sec - 1 ) {
				clock_gettime(CLOCK_REALTIME, &fs->start);
			}
			else {
				unlock(&fs->curlmutex, &fsl.locked);
				url = fs->location;
				use_location = true;
			}
		}
	}

	while(1) {
		fsl.http_status_code = 0;
		CURL_EASY_SETOPT(curl,
				 CURLOPT_WRITEFUNCTION, write_first, "%p");
		CURL_EASY_SETOPT(curl,
				 CURLOPT_HEADERFUNCTION, header_check, "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_WRITEDATA, &fsl, "%p");

		CURL_EASY_SETOPT(curl, CURLOPT_URL, url, "%s");

		lprintf(LOG_INFO, "curling %s 0x%X %u\n",
			url,fs->flags, fsl.locked );
		res= curl_easy_perform(curl);
		lprintf(LOG_INFO, "curl_easy_perform exited with %d http=%d\n",
			res,fsl.http_status_code);

		if (206 == fsl.http_status_code)
			break;
		if (0 == fsl.http_status_code) {
			fs->flags |= FL_IS_INIT | FL_NO_RANGE;
			lprintf(LOG_INFO, "marking %s as 'No-Range'\n"
					, fs->URL);
			res = STREAM_ERROR;
			break;
		}
		/* From here http_status_code is 300 */
		if (use_location) {
			lock(&fs->curlmutex, &fsl.locked);
			if (count != fs->location_count) {
				count = fs->location_count;
				unlock(&fs->curlmutex, &fsl.locked);
			}
			else {
				use_location = false;
				url = fs->URL;
				free(fs->location);
				fs->location = NULL;
				clock_gettime(CLOCK_REALTIME, &fs->start);
				continue;
			}
		}
		url = fs->location;

	}
	unlock(&fs->curlmutex, &fsl.locked);
	return res;
}

/*
 * @brief utility to initialise a curl handle
 *
 * The handle is initialised and populated according to the global parameters
 *
 * @param none
 * @return the initialised curl handle
 */
static CURL *curl_init(struct filespec *fs)
{
	CURL *curl;

	curl = curl_easy_init();
	if (NULL == curl)
		lprintf( LOG_CRIT, "initializing curl easy handle.\n" );
	if (!F_GOT_TIME(fs))
		CURL_EASY_SETOPT(curl, CURLOPT_FILETIME, 1L, "%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_IPRESOLVE, fs->curl_IP_resolve, "%ld");
	if (F_INSECURE(fs))
		CURL_EASY_SETOPT(curl, CURLOPT_SSL_VERIFYPEER, 0, "%ld");
	if (NULL != params.ca_file)
		CURL_EASY_SETOPT(curl, CURLOPT_CAPATH, params.ca_file, "%s");
	if (NULL != params.user_agent)
		CURL_EASY_SETOPT(curl, CURLOPT_USERAGENT, params.user_agent,
				 "%s");
	if (0 == fs->curl_opt_keepalive_time) {
		CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPALIVE, 0L, "%ld");
	}
	else {
		CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPALIVE, 1L,"%ld");
		CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPIDLE,
				(long)fs->curl_opt_keepalive_time, "%ld");
		CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPINTVL,
				(long)fs->curl_opt_keepalive_time, "%ld");
	}

	if (NULL != fs->userpwd) {
		CURL_EASY_SETOPT(curl, CURLOPT_USERPWD, fs->userpwd, "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC, "%l");
	}


	return curl;
}

/*
 * @brief asynchronous reader
 *
 * This is the actual asynchronous reader.
 * The very first part initializes the thread local 'work' buffer
 * The main loop waits for some work to arrive from astreamfs_read.
 * - if it is a "close" (size is zero) we do nothing and loop back.
 * - otherwise we start reading at the offset requested by astreamfs_read, after
 *   having inserted our local cache buffer.
 * We return from reading the stream on 3 conditions:
 * - stream end
 * - close
 * - httfs2_read selected this stream because there were no pending requests
 * In all those cases, we have nothing more to do than loop to the start, the
 * third case will automatically give a new work item and start a stream at
 * the new offset specified.
 *
 * @param opaque pointer
 * @return return value when the thread terminates (never reached for now)
 */

void *
async_reader(void *arg) {
	struct reader *r= arg;
	char range[40];
	struct node *n;
	struct filespec *last_fs = NULL;
	char buf[KBUF];

	work.buf= buf;
	work.size = 0;

	while (1) {
		if (NULL == r->prv) {
			lprintf(LOG_DEBUG,
				"async_reader[%d] loop: waiting.\n",
				r - readers);

			get_work(r);
			lprintf(LOG_DEBUG,
				"async_reader[%d] received read (%"PRIu64", %lu)\n",
				r - readers, r->prv->offset, r->prv->size );
		}
		if (F_NO_RANGE(r->s)) {
			struct node *tmp;
			tmp = r->prv->next;
			if (&work != r->prv)
				sem_post(&r->prv->sem_done);
			r->prv = tmp;
			continue;
		}
		if (0 == r->prv->size) {
			n = r->prv;
			r->prv = n->next;
			sem_post(&n->sem_done);
			continue;
		}

		if (last_fs != r->s) {
			if (NULL != r->curl)
				curl_easy_cleanup(r->curl);
			r->curl = curl_init(r->s);
			last_fs = r->s;
		}
		if (!r->streaming) {
			work.offset = r->start;
			work.written = 0;
			work.size = KBUF;
			if (work.offset + work.size > r->s->st.st_size)
				work.size = r->s->st.st_size - work.offset;
			work.next = r->prv;
			r->prv = &work;
			r->pos = r->start;
			r->streaming = true;
			sprintf(range,"%"PRIu64"-", r->start);
		}
		else {
			sprintf(range,"%"PRIu64"-", r->pos);
		}
		CURL_EASY_SETOPT(r->curl, CURLOPT_RANGE, range, "%s");
		lprintf(LOG_DEBUG, "async_reader[%d] range is %s\n",
			r - readers, range);
		if (CURLE_OK != stream_fs(r->curl,
					  r->s,
					  (curl_read_callback)write_data,
					  r)
			&&
		    r->streaming) {
			lprintf(LOG_DEBUG, "stream_fs exception[%d].\n"
					 , r - readers);
			lock(&r->s->fastmutex, NULL);
			for (n = r->prv; n != NULL; n = n->next) {
				if (&work == n) {
					n->written = 0;
				}
				else {
					n->written = -EIO;
					sem_post(&n->sem_done);
				}
			}
			n = atomic_load_explicit(&r->rcv, memory_order_relaxed);
			while (NULL != n) {
				int err;
				err= sem_wait(&r->sem_go);
				if ( 0 != err )
					lprintf(LOG_CRIT, "async_reader[%d] sem_wait error: %d.\n",
						r - readers, err);
				n->written = -EIO;
				sem_post(&n->sem_done);
				n = n->next;
			}
			r->idle = true;
			r->prv = NULL;
			atomic_store_explicit(&r->rcv, NULL,
					      memory_order_relaxed);
			unlock(&r->s->fastmutex, NULL);
		}
	}

	return NULL;
}

/*
 *  Added by 1fichierfs 1.3.0, now "speed" is measured by the engine
 *  So here is provided an empty function until we implement that too in
 *  astreamfs.
 */
void update_speed_stats(bool read, long written, unsigned int i){}

/*
 * @brief fuse callback for read
 *
 * This function is called each time there is a block of data to read.
 * Steps:
 * - encapsulates the read request in our thread local 'work' buffer, including
 *   an arrival timestamp
 * - under lock, selects the relevant async reader for that read block.
 *   Each reader exposes a "read slice": start-end, a pending number of requests
 *   and a flag for idle state.
 *   -1) Best match is a block inside our "read slice" or at most 128k beyond
 *       the end (128k being the kernel's readahead cache buffer size)
 *   -2) Second the "oldest" idle reader
 *   -3) Last the "oldest" reader with no pending requests
 * - Match 1) uses an existing stream reader.
 * - Match 2 and 3) will start a new stream, either on an already idle reader
 *       or terminating the waiting stream plus start a new one.
 *       We always add a 128K buffer ahead of the offset we received to better
 *       serve out of order requests.
 *  - Match 4) is when no match is found: we do a range request with the exact
 *       size instead of giving the request to the async reader.
 * - Once that is done, we put the request on the selected reader's receive
 *   queue and signal it with a semaphore (unless case 4).
 * - Now we only need to wait on the "done" semaphore for the reader to fill
 *   our buffer.
 * - We can then display the time it took and return to fuse.
 *
 * @param path to the file being read
 * @param buffer where we store read data
 * @param size of the buffer
 * @param offset from where we read
 * @param fuse internal pointer with possible private data
 * @return number of bytes read (should be size unless error or end of file)
 */
static int astreamfs_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	struct timespec start, now;
	int i, j, i_idle, i_wait;
	unsigned long last_rq_idle, last_rq_wait, this_rq_id;
	struct filespec *fs;
	struct reader r_dup[MAX_READERS];

	if (NULL == work.buf) {
		sem_init(&work.sem_done, 0, 0);
        }

	fs = (struct filespec *)((uintptr_t)(fi->fh));

	if (F_NO_RANGE(fs))
		return -EIO;

	/* Shortcut in case of impossible read: beyond file end! */
	if (offset >= fs->st.st_size) {
		lprintf(LOG_DEBUG,
			">>*<< impossible read beyond EOF (fast return 0): (%"PRIu64",%lu).\n",
			offset,	size);
		return 0;
	}

	work.buf = buf;
	if (offset + size > fs->st.st_size)
		work.size = fs->st.st_size - offset;
	else
		work.size = size;
	work.offset= offset;
	clock_gettime(CLOCK_REALTIME, &start);
	work.written= 0;

	i_idle = -1;
	i_wait = -1;
	last_rq_idle = ULONG_MAX;
	last_rq_wait = ULONG_MAX;

	/*
	 * Start critical section
	 */
	lock(&fs->fastmutex, NULL);
	if (LOG_DEBUG == params.log_level)
		memcpy(r_dup, readers, sizeof(readers));
	for (i=0; i < MAX_READERS; i++) {
		if (readers[i].idle) {
			if (readers[i].last_rq_id < last_rq_idle) {
				i_idle = i;
				last_rq_idle = readers[i].last_rq_id;
			}
		}
		else {
			if (readers[i].s == fs &&
			    work.offset >= readers[i].start &&
			    work.offset <= readers[i].end + KBUF)
			    break;
			if (0 == readers[i].n_rq &&
			    readers[i].last_rq_id < last_rq_wait) {
				i_wait = i;
				last_rq_wait = readers[i].last_rq_id;
			}
		}
	}
	if (i == MAX_READERS) {
		i = (-1 == i_idle) ? i_wait : i_idle;
		if (i == -1) {
			char range[40];
			CURL *curl;

			unlock(&fs->fastmutex, NULL);
			lprintf(LOG_DEBUG,
				"no reader available, doing single range: %"PRIu64"-%"PRIu64"\n",
				offset, offset + size - 1);
			curl = curl_init(fs);
			sprintf(range,"%"PRIu64"-%"PRIu64, offset
							 , offset + size - 1);
			CURL_EASY_SETOPT(curl, CURLOPT_RANGE, range, "%s");
			stream_fs(curl,
				  fs,
				  (curl_read_callback)single_range,
				  &work);
			curl_easy_cleanup(curl);
			fs->last_rq_id++;
			this_rq_id = fs->last_rq_id;
			goto out_time;
		}
		readers[i].start = (work.offset > KBUF) ?
				    work.offset - KBUF : 0;
		readers[i].end = work.offset + work.size;
		if (readers[i].end < KBUF)
			readers[i].end = KBUF;
		if (readers[i].end > fs->st.st_size)
			readers[i].end = fs->st.st_size;
		readers[i].idle = false;
	}
	else {
		if (offset + size > readers[i].end)
			readers[i].end = offset + size;
	}
	atomic_fetch_add_explicit(&readers[i].n_rq, 1, memory_order_acq_rel);
	fs->last_rq_id++;
	readers[i].last_rq_id = this_rq_id = fs->last_rq_id;
	readers[i].s = fs;
	msg_push(&readers[i].rcv, &work);
	sem_post(&readers[i].sem_go);
	unlock(&fs->fastmutex, NULL);
	/*
	 * End critical section
	 */
	lprintf(LOG_DEBUG, ">> unfichier_read[%d]: %p (%"PRIu64",%lu) rq[%d]--------\n",
		i, buf, offset, size, this_rq_id);
	for (j=0; j< MAX_READERS; j++)
		lprintf(LOG_DEBUG,
			"dump: readers[%d] fs=%p %"PRIu64":%"PRIu64" (idle=%d) (n_rq=%u)\n",
			j, ((NULL == r_dup[j].s) ? -1 : r_dup[j].s - fss),
			r_dup[j].start, r_dup[j].end,
			r_dup[j].idle, r_dup[j].n_rq);
	sem_wait(&work.sem_done);
	atomic_fetch_sub_explicit(&readers[i].n_rq, 1, memory_order_acq_rel);

out_time:
	clock_gettime(CLOCK_REALTIME, &now);
	if (now.tv_nsec < start.tv_nsec) {
		now.tv_nsec -= start.tv_nsec - 1000000000L;
		now.tv_sec  -= start.tv_sec + 1;
	}
	else {
		now.tv_nsec -= start.tv_nsec;
		now.tv_sec  -= start.tv_sec;
	}
	lprintf(LOG_INFO,
		">> read-response[%u]: (%"PRIu64",%lu|%d) T=%lu.%lu\n",
		this_rq_id, offset, size, work.written,
		now.tv_sec, now.tv_nsec );

	return (int)work.written;
}

static struct fuse_operations astreamfs_oper = {
        .init           = astreamfs_init,
	.getattr	= astreamfs_getattr,
	.readdir	= astreamfs_readdir,
	.open		= astreamfs_open,
	.read		= astreamfs_read,
	.release	= astreamfs_release,
};


enum {
     KEY_HELP,
     KEY_VERSION,
     KEY_DEBUG,
     KEY_FOREGROUND,
     KEY_RW,
     KEY_NOATIME,
     KEY_NOEXEC,
     KEY_NOSUID,
     KEY_FS_NAME,
     KEY_FS_SUBTYPE,
     KEY_LOG_LEVEL,
     KEY_LOG_FILE,
     KEY_SIZE,
     KEY_TIME,
     KEY_IDLE_TIME,
     KEY_PATH,
     KEY_KEEP_LOCATION_TIME,
     KEY_NO_KEEP_LOCATION,
     KEY_CURL_CACERT,
     KEY_CURL_CONFIG,
     KEY_CURL_INSECURE,
     KEY_CURL_IPV4,
     KEY_CURL_IPV6,
     KEY_CURL_KEEPALIVE_TIME,
     KEY_CURL_LOCATION,
     KEY_CURL_NEXT,
     KEY_CURL_NO_KEEPALIVE,
     KEY_CURL_OUTPUT,
     KEY_CURL_REMOTE_HEADER_NAME,
     KEY_CURL_REMOTE_NAME,
     KEY_CURL_REMOTE_TIME,
     KEY_CURL_URL,
     KEY_CURL_USER_AGENT,
     KEY_CURL_USERPWD,
};

static const char end_expand[] = "AilotPSTu";


static const char arg_log_level[] = "--log-level=%i";
static const char arg_log_file[]  = "--log-file=%s";
static const char arg_idle_time[] = "--idle-time=%i";
static const char arg_path[]      = "--path=%s";
static const char arg_size[]      = "--size=%li";
static const char arg_time[]      = "--time=%li";
static const char arg_keep_location_time[]
				  = "--keep-location-time=%i";
static const char arg_no_keep_location[]
				  = "--no-keep-location";
static const char arg_ipv4[]      = "--ipv4";
static const char arg_ipv6[]      = "--ipv6";
static const char arg_config[] 	  = "--config=%s";
static const char arg_insecure[]  = "--insecure";
static const char arg_next[]      = "--next";
static const char arg_user_agent[]= "--user-agent=%s";
static const char arg_remote_header_name[]
				  = "--remote-header-name";
static const char arg_location[]  = "--location";
static const char arg_remote_name[]
				  = "--remote-name";
static const char arg_output[]    = "--output=%s";
static const char arg_remote_time[]
				  = "--remote-time";
static const char arg_userpwd[]   = "--user=%s";
static const char arg_url[]	  = "--url=%s";
static const char arg_keepalive_time[]
				  = "--keepalive-time=%i";
static const char arg_no_keepalive[]
				  = "--no-keepalive";
static const char arg_debug[]	  = "--debug";

static const char *long_names[] = {
	arg_config		+ O_OFFSET,
	arg_idle_time		+ O_OFFSET,
	arg_keep_location_time	+ O_OFFSET,
	arg_keepalive_time	+ O_OFFSET,
	arg_log_file		+ O_OFFSET,
	arg_log_level		+ O_OFFSET,
	arg_output		+ O_OFFSET,
	arg_path		+ O_OFFSET,
	arg_size		+ O_OFFSET,
	arg_time		+ O_OFFSET,
	arg_url			+ O_OFFSET,
	arg_userpwd 		+ O_OFFSET,
	arg_user_agent		+ O_OFFSET,
};
#define S_LONG_NAMES (sizeof(long_names) / sizeof(long_names[0]))

#define SZ_LONG_ARG_PATH (sizeof(arg_path) - 1 - SZ_STD_ARG_TYPE)
#define SZ_LONG_ARG_USER_AGENT (sizeof(arg_user_agent) - 1 - SZ_STD_ARG_TYPE)
#define SZ_O_ARG_USER_AGENT (SZ_LONG_ARG_USER_AGENT - O_OFFSET)
#define SZ_LONG_ARG_USERPWD (sizeof(arg_userpwd) - 1 - SZ_STD_ARG_TYPE)

static const struct fuse_opt astreamfs_opts[] = {
  /* our specific option */
  FUSE_ALL_ARG( "-l %i"	, arg_log_level	 , (int)KEY_LOG_LEVEL		),
  FUSE_L_O_ARG(           arg_log_file	 , (int)KEY_LOG_FILE		),
  FUSE_ALL_ARG( "-i %i"	, arg_idle_time	 , (int)KEY_IDLE_TIME		),
  FUSE_ALL_ARG( "-P %s"	, arg_path	 , (int)KEY_PATH		),
  FUSE_ALL_ARG( "-S %li", arg_size	 , (int)KEY_SIZE		),
  FUSE_ALL_ARG( "-T %li", arg_time	 , (int)KEY_TIME		),
  FUSE_ALL_ARG( "-t %i"	,
			  arg_keep_location_time
					 , (int)KEY_KEEP_LOCATION_TIME	),
  FUSE_L_O_ARG(		  arg_no_keep_location
					 , (int)KEY_NO_KEEP_LOCATION	),

  /* curl options we understand */
  FUSE_ALL_ARG( "-4"	, arg_ipv4	 , (int)KEY_CURL_IPV4		),
  FUSE_ALL_ARG( "-6"	, arg_ipv6	 , (int)KEY_CURL_IPV6		),
  FUSE_ALL_ARG( "-:"	, arg_next	 , (int)KEY_CURL_NEXT		),
  FUSE_ALL_ARG( "-A %s"	, arg_user_agent , (int)KEY_CURL_USER_AGENT	),
  FUSE_ALL_ARG( "-J"	,
			  arg_remote_header_name
					 , (int)KEY_CURL_REMOTE_HEADER_NAME),
  FUSE_ALL_ARG( "-k"	, arg_insecure	 , (int)KEY_CURL_INSECURE	),
  FUSE_ALL_ARG( "-K"	, arg_config	 , (int)KEY_CURL_CONFIG		),
  FUSE_ALL_ARG( "-L"	, arg_location	 , (int)KEY_CURL_LOCATION	),
  FUSE_ALL_ARG( "-O"	, arg_remote_name,(int)KEY_CURL_REMOTE_NAME	),
	/*
	 * We can't use -o for output since that is used by fuse.
	 * We have to use the long option instead --output
	 */
  FUSE_L_O_ARG( 	  arg_output	 , (int)KEY_CURL_OUTPUT		),
  FUSE_ALL_ARG( "-R"	, arg_remote_time, (int)KEY_CURL_REMOTE_TIME	),
  FUSE_ALL_ARG( "-u %s"	, arg_userpwd	 , (int)KEY_CURL_USERPWD	),
  FUSE_L_O_ARG( 	  arg_url	 , (int)KEY_CURL_URL		),
  FUSE_L_O_ARG(		  arg_keepalive_time
					 , (int)KEY_CURL_KEEPALIVE_TIME	),
  FUSE_L_O_ARG(		  arg_no_keepalive
					 , (int)KEY_CURL_NO_KEEPALIVE	),

  /*
   *  fuse standard options
   */
  FUSE_OPT_KEY( "-h"			 , (int)KEY_HELP		),
  FUSE_OPT_KEY( "--help"		 , (int)KEY_HELP		),
  FUSE_OPT_KEY( "-V"			 , (int)KEY_VERSION		),
  FUSE_OPT_KEY( "--version"		 , (int)KEY_VERSION		),
  FUSE_OPT_KEY( "rw"			 , (int)KEY_RW			),
  FUSE_OPT_KEY( "ro"			 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_ALL_ARG( "-d"	, arg_debug	 , (int)KEY_DEBUG		),
  FUSE_OPT_KEY( "-f"			 , (int)KEY_FOREGROUND		),
  FUSE_OPT_KEY( "-o "			 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "-s"			 , (int)FUSE_OPT_KEY_KEEP	),

  FUSE_OPT_KEY( "allow_other"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "allow_root"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "auto_unmount"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "nonempty"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "default_permissions"	 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "fsname=%s"		 , (int)KEY_FS_NAME		),
  FUSE_OPT_KEY( "subtype=%s"		 , (int)KEY_FS_SUBTYPE		),
  FUSE_OPT_KEY( "large_read"		 , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "max_read=%i"		 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "hard_remove"		 , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "noatime"		 , (int)KEY_NOATIME		),
  FUSE_OPT_KEY( "noexec"		 , (int)KEY_NOEXEC		),
  FUSE_OPT_KEY( "nosuid"		 , (int)KEY_NOSUID		),
  FUSE_OPT_KEY( "nodev"			 , (int)FUSE_OPT_KEY_KEEP	),

  FUSE_OPT_KEY( "use_ino"                , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "readdir_ino"            , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "direct_io"              , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "kernel_cache"           , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "auto_cache"             , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "noauto_cache"           , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "umask=%s"               , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "uid=%i"                 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "gid=%i"                 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "entry_timeout=%s"       , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "negative_timeout=%s"    , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "attr_timeout=%s"        , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "ac_attr_timeout=%s"     , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "noforget"               , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "remember=%s"            , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "nopath"                 , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "modules=%s"             , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "max_write=%i"           , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "max_readahead=%i"       , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "max_background=%i"      , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "congestion_threshold=%i", (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "async_read"             , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "sync_read"              , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "atomic_o_trunc"         , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "big_writes"             , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_remote_lock"         , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "no_remote_flock"        , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_remote_posix_lock"   , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_KEY( "splice_write"           , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_splice_write"        , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "splice_move"            , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_splice_move"         , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "splice_read"            , (int)FUSE_OPT_KEY_DISCARD	),
  FUSE_OPT_KEY( "no_splice_read"         , (int)FUSE_OPT_KEY_KEEP	),
  FUSE_OPT_END
};

static const char s_http[] = "http://" ;
static const char s_https[]= "https://";
/* static const char s_ftp[]  = "ftp://"  ; */

static const struct {
  const char *p;
  const unsigned int  s;
} scheme[]= { {s_http , sizeof(s_http)  - 1},
              {s_https, sizeof(s_https) - 1},
/*            {s_ftp  , sizeof(s_ftp)   - 1}, */
            };

#define N_SCHEMES (sizeof(scheme)/sizeof(scheme[0]))
#define MSG_ERR_ADD_OPT_ARG "adding option argument (fuse_opt_add_arg).\n"

/*
 * @brief canonicalizes an absolute path
 *
 * Since we create paths from the arguments, we cannot use the standard
 * canonicalize_file_name function. These functions do the same but do not
 * resolve links. So they only remove multiple consecutive /, useless ./
 * and compute ../
 * Because we don't resolve links, the returned buffer's size is always
 * shorter or equal than the entry buffer, so the "ret" buffer should be
 * at least of same length as the "name".
 *
 * @param path argument passed to the program
 * @param return buffer
 * @return size of the result string
 */
static size_t canonicalize_path(const char *arg, char *res)
{
	const char *p;
	char *q;

	p = arg + (('-' == arg[1]) ? SZ_LONG_ARG_PATH : SZ_SHORT_ARG);
	if ('\0' == *p) {
		lprintf(LOG_ERR, "empty path.\n");
		return 0;
	}

	if ( '/' == *p ) {
		*res = '/';
		q = res;
		p++;
	}
	else {
		q = res + strlen(res) -1;
	}

	for (; '\0' != *p; p++)
	{
		if ('/' == *q) {
			if ( '/' == *p )
				continue;
			if ('.' == *p) {
				if ('/' == p[1]) {
					p++;
					continue;
				}
				if ('\0' == p[1])
					break;
				if ('.' == p[1] &&
				    ('/' == p[2] || '\0' == p[2])) {
					if (q != res)
						while ('/' != *(--q));
					p += 2;
					continue;
				}
			}
		}
		if (q - res > PATH_MAX - 3) {
			lprintf(LOG_ERR, "path is too long: %s.\n", arg);
			return 0;
		}
		q++;
		*q = *p;
	}
	if ('/' != *q)
		*(++q) = '/';
	*(++q) = '\0';
	return q -res;
}

/*
 * @brief qsort callback to compare on paths
 *
 * @param pointer on key
 * @param pointer on another key
 * @return <0 >0 or ==0 according to order of keys
 */
static int path_cmp(const void *k1, const void *k2)
{
	return strcmp(*((char **)k1), *((char **)k2));
}


/*
 * @brief update paths pointer while deduplicating paths
 *
 * @param old path pointer
 * @param new path pointer
 * @return none
 */
static void update_path(char *old, char *new)
{
	unsigned long i;
	for (i = 0; i < count_URL; i++)
		if ( fss[i].path == old )
			fss[i].path = new;
}

/*
 * @brief deduplicates paths
 *
 * @param none
 * @return none
 */
static void dedup_path()
{
	unsigned long i, j, k, uniq;
	char **new;
	size_t mem;

	qsort(paths, count_path, sizeof(char *), path_cmp);
	uniq = 1;
	mem = strlen(paths[0]) + 1;
	for (j = 0, i = 1; i < count_path; i++) {
		if (0 == strcmp(paths[i], paths[j])) {
			update_path(paths[i],  paths[j]);
		}
		else {
			uniq++;
			mem += strlen(paths[i]) + 1;
			j = i;
		}
	}
	if (uniq == count_path) {
		lprintf(LOG_INFO,"all paths are unique.\n");
		return;
	}

	new = fs_alloc(uniq * sizeof(char *) + mem);
	new[0] = (char *)(&new[uniq]);
	strcpy(new[0], paths[0]);
	update_path(paths[0],  new[0]);
	for (k= 0, j = 0, i = 1; i < count_path; i++)
		if (0 != strcmp(paths[i], paths[j])) {
			new[k + 1] = new[k] + strlen(new[k]) + 1;
			k++;
			strcpy(new[k], paths[i]);
			update_path(paths[i],  new[k]);
			j = i;
		}
	count_path = uniq;
	free(paths);
	paths = new;
	lprintf(LOG_INFO,"there are %u unique paths.\n", uniq);
}

/*
 * @brief finalise a filesystem entry
 *
 * Final operations and checks to simplify further management
 *
 * @param index of the entry to initialise
 * @return none
 */
static void end_init(unsigned long cur_URL)
{
	char *p;

	if (!F_REMOTE_TIME(&fss[cur_URL])) {
		fss[cur_URL].flags |= FL_GOT_TIME;
	}
	if (!F_GOT_NAME(&fss[cur_URL])) {
		if (!F_REMOTE_NAME(&fss[cur_URL])) {
			fss[cur_URL].filename =
					fs_alloc(sizeof(default_filename) + 6);
			sprintf(fss[cur_URL].filename, "%s%06lu",
				default_filename, cur_URL);
			fss[cur_URL].flags |= FL_GOT_NAME;
		}
		else {
			p = strrchr(fss[cur_URL].URL, '/');
			if ('\0' == p[1]) {
			/*
			 * We could have issued only a warning and used the
			 * default file value, but we choose to replicate
			 * the curl command line tool behaviour here!
			 */
				lprintf(LOG_ERR,
					"remote file name has no length.\n");
			}
			fss[cur_URL].filename = fs_alloc(strlen(p));
			strcpy(fss[cur_URL].filename, p + 1);
			if (0 == (fss[cur_URL].flags & FL_REMOTE_HEADER_NAME))
				fss[cur_URL].flags |= FL_GOT_NAME;
		}
	}
}

/*
 * @brief initialise a filesystem entry
 *
 * Depending on new_args boolean, either sets the entry to default options or
 * copy options from the previous entry.
 *
 * @param pointer to the parse_data structure
 * @return none
 */
static void init_url(struct parse_data* d)
{
	if (d->new_args) {
		fss[d->cur_URL].flags = 0;
		fss[d->cur_URL].curl_IP_resolve = CURL_IPRESOLVE_WHATEVER;
		fss[d->cur_URL].curl_opt_keepalive_time = 60;
		fss[d->cur_URL].idle_time = 300;
		fss[d->cur_URL].keep_location_time = 0;
		fss[d->cur_URL].start.tv_sec = 0;
		fss[d->cur_URL].start.tv_nsec = 0;
		fss[d->cur_URL].userpwd = NULL;
		fss[d->cur_URL].path = paths[0];
		fss[d->cur_URL].location_count = 0;
		fss[d->cur_URL].location = NULL;
		fss[d->cur_URL].nb_open = 0;
		fss[d->cur_URL].last_rq_id = 0;
		d->new_args = false;
	}
	else {
		memcpy( &fss[d->cur_URL],
			&fss[d->cur_URL - 1],
			sizeof(struct filespec));
		if (0 != strcmp(fss[d->cur_URL].path, d->cur_abs_path))
			fss[d->cur_URL].path = paths[count_path - 1];
		fss[d->cur_URL].flags &= ~(FL_GOT_NAME | FL_GOT_SIZE |
					   FL_GOT_TIME);
	}
	fss[d->cur_URL].URL = NULL;
	fss[d->cur_URL].filename = NULL;
	pthread_mutex_init(&fss[d->cur_URL].fastmutex, NULL);
	pthread_mutex_init(&fss[d->cur_URL].curlmutex, NULL);
}


/*
 * @brief processing function called by fuse_opt_parse
 *
 * See fuse_opt.h for this function specifics.
 *
 * @param data is the user data passed to the fuse_opt_parse() function
 * @param arg is the whole argument or option
 * @param key determines why the processing function was called
 * @param outargs the current output argument list
 * @return -1 on error, 0 if arg is to be discarded, 1 if arg should be kept
 */
static int astreamfs_opt_proc(void *parse_data,
			      const char *arg,
			      int key,
			      struct fuse_args *outargs)
{
	struct parse_data *d = (struct parse_data *)parse_data;
	int i;
	unsigned long off, sz;
	char *p, *q;

	switch (key) {
	case KEY_CURL_URL :
		arg = strchr(arg, '=') + 1;
		if (NULL == arg)
			lprintf(LOG_ERR, "malformed argument: %s\n", arg);
		/* fall through to manage the URL */
	case FUSE_OPT_KEY_NONOPT :
		if (count_URL - 1 == d->cur_URL &&
		    NULL != fss[d->cur_URL].URL) {
			return 1;
		}

		if (d->got_URL) {
			end_init(d->cur_URL);
			d->cur_URL++;
			init_url(d);
		}
		d->got_URL = true;
		for (i = 0;
		     i < N_SCHEMES &&
		     0 != strncmp(arg, scheme[i].p, scheme[i].s);
		     i++);
		if (N_SCHEMES == i && NULL != strstr(arg, "://"))
			lprintf( LOG_ERR, "unknown scheme in: %s\n", arg);
		p = strchr(arg + (( N_SCHEMES == i ) ? 0 : scheme[i].s), '/');
		q = fs_alloc(strlen(arg) +
			     ((NULL==p) ? 2 : 1) +
			     (( N_SCHEMES == i ) ? scheme[0].s : 0));
		fss[d->cur_URL].URL = q;
		if (N_SCHEMES == i) {
			memcpy(q, scheme[0].p, scheme[0].s);
			q += scheme[0].s;
		}
		strcpy(q, arg);
		if (NULL == p) {
			q += strlen(arg);
			q[0]= '/';
			q[1]= '\0';
		}
		break;

	case KEY_CURL_NEXT :
		if (count_URL - 1 == d->cur_URL || !d->got_URL) {
			lprintf(LOG_WARNING,
				"--next is ignored before we got a URL or after last URL.\n");
			break;
		}
		end_init(d->cur_URL);
		d->cur_URL++;
		d->new_args = true;
		d->got_URL = false;
		init_url(d);
		d->cur_abs_path[1] = '\0';
		break;

	case KEY_PATH:
		paths[count_path] = paths[0] + d->path_sz;
		sz = canonicalize_path(arg, d->cur_abs_path);
		strcpy(paths[count_path], d->cur_abs_path);
		d->path_sz += sz + 1;
		if (!d->got_URL)
			fss[d->cur_URL].path = paths[count_path];
		count_path++;
		break;

	case KEY_CURL_USERPWD:
		off = ('-' == arg[1])	? SZ_LONG_ARG_USERPWD
					: SZ_SHORT_ARG   ;
		free(fss[d->cur_URL].userpwd);
		fss[d->cur_URL].userpwd = fs_alloc(strlen(arg + off) + 1);
		strcpy(fss[d->cur_URL].userpwd, arg + off);
		break;
	case KEY_RW:
		lprintf(LOG_WARNING,
			"this filesystem will be mounted read-only, ignoring 'rw' option.\n");
		break;

	case KEY_DEBUG:
		params.debug = true;
                    /* debug implies foreground: fall through */
	case KEY_FOREGROUND:
		params.foreground = true;
		return 1;
	case KEY_NOATIME:
		params.noatime = true;
		return 1;
	case KEY_NOEXEC:
		params.noexec = true;
		return 1;
	case KEY_NOSUID:
		params.nosuid = true;
		return 1;

	case KEY_CURL_CACERT:
		read_str(arg, &params.ca_file);
		break;
	case KEY_CURL_IPV4:
		fss[d->cur_URL].curl_IP_resolve = CURL_IPRESOLVE_V4;
		break;
	case KEY_CURL_IPV6:
		fss[d->cur_URL].curl_IP_resolve = CURL_IPRESOLVE_V6;
		break;
	case KEY_CURL_REMOTE_HEADER_NAME:
		fss[d->cur_URL].flags |= FL_REMOTE_HEADER_NAME;
		break;
	case KEY_CURL_REMOTE_NAME:
		fss[d->cur_URL].flags |= FL_REMOTE_NAME;
		break;
	case KEY_CURL_REMOTE_TIME:
		fss[d->cur_URL].flags |= FL_REMOTE_TIME;
		break;
	case KEY_CURL_LOCATION:
		fss[d->cur_URL].flags |= FL_LOCATION ;
		break;
	case KEY_NO_KEEP_LOCATION:
		fss[d->cur_URL].flags &= ~FL_KEEP_LOCATION;
		break;
	case KEY_CURL_NO_KEEPALIVE:
		fss[d->cur_URL].curl_opt_keepalive_time = 0;
		break;
	case KEY_CURL_OUTPUT:
		read_str(arg, &fss[d->cur_URL].filename);
		fss[d->cur_URL].flags |= FL_GOT_NAME;
		break;
	case KEY_SIZE:
		fss[d->cur_URL].flags |= FL_GOT_SIZE;
		fss[d->cur_URL].st.st_size    =
		fss[d->cur_URL].st.st_blocks  = (size_t)read_int(arg);
		fss[d->cur_URL].st.st_blocks /= ST_BLK_SZ;
		break;
	case KEY_TIME:
		fss[d->cur_URL].flags |= FL_GOT_TIME;
		fss[d->cur_URL].st.st_mtim.tv_sec = read_int(arg);
		break;
	case KEY_IDLE_TIME:
		fss[d->cur_URL].idle_time = (unsigned int)read_int(arg);
		break;
	case KEY_CURL_KEEPALIVE_TIME:
		fss[d->cur_URL].curl_opt_keepalive_time =
						(unsigned int)read_int(arg);
		break;
	case KEY_LOG_LEVEL:
		params.log_level = (unsigned int)read_int(arg);
		if (MAX_LEVEL < params.log_level) {
			lprintf(LOG_WARNING,
				"log level=%u must be between 0 and %u: assuming %u.\n",
				params.log_level, MAX_LEVEL, MAX_LEVEL);
			params.log_level = MAX_LEVEL;
		}
		break;
	case KEY_LOG_FILE:
		if (NULL != log_fh)
			fclose(log_fh);
		read_str(arg, &params.log_file);
		log_fh = fopen(params.log_file, "w");
		if (NULL == log_fh)
			lprintf(LOG_ERR, "cannot open %s.\n", params.log_file);
		break;
	case KEY_KEEP_LOCATION_TIME:
		fss[d->cur_URL].keep_location_time =
						(unsigned int)read_int(arg);
		fss[d->cur_URL].flags |= FL_KEEP_LOCATION;
		break;
	case KEY_CURL_INSECURE:
		fss[d->cur_URL].flags |= FL_INSECURE;
		break;
	case KEY_FS_NAME:
		read_str(arg, &params.filesystem_name);
		break;
	case KEY_FS_SUBTYPE:
		read_str(arg, &params.filesystem_subtype);
		break;
	case KEY_CURL_USER_AGENT:
		if (default_user_agent != params.user_agent)
			free(params.user_agent);
		off = ('-' == arg[0]) ? ( '-' == arg[1] ? SZ_LONG_ARG_USER_AGENT
							: SZ_SHORT_ARG )
				      :			  SZ_O_ARG_USER_AGENT;
		sz = strlen(arg + off);
		if (0 == sz) {
			params.user_agent = NULL;
		}
		else {
			params.user_agent = fs_alloc(strlen(arg + off) + 1);
			strcpy(params.user_agent, arg + off);
		}
		break;
	case KEY_HELP :
	case KEY_VERSION :
		lprintf(LOG_WARNING,
			"--help and --versions options are ignored in a config file.\n");
		break;
	default:
		lprintf(LOG_ERR, "unknown option: %s.\n", arg);
		break;
	}
	return 0;
}

/*
 * @brief counting function called by fuse_opt_parse
 *
 * This callback only handles --help, --version, and counts the number of
 * URLs so that we can allocate memory for the filesystem entries.
 * It does also keep track of the last non-option argument which is the
 * mount point.
 *
 * See fuse_opt.h for this function specifics.
 *
 * @param data is the user data passed to the fuse_opt_parse() function
 * @param arg is the whole argument or option
 * @param key determines why the processing function was called
 * @param outargs the current output argument list
 * @return -1 on error, 0 if arg is to be discarded, 1 if arg should be kept
 */
 static int astreamfs_opt_count(void *parse_data,
				const char *arg,
				int key,
				struct fuse_args *outargs)
{
	struct parse_data *d = (struct parse_data *)parse_data;

	switch (key) {
	case KEY_HELP :
		fprintf(stderr,
			"Usage: astreamfs [options] URL [options] [[URL] [options]]* mountpoint\n"
			"\n"
			"astreamfs options:\n"
			"     -l --log-level=N  Verbosity level. The levels are those of syslog.\n"
			"                       The higher, the most verbose. Default: 4 (warning).\n"
			"        --log-file=filepath\n"
			"                       Log will be saved to that file instead of stderr\n"
			"                       (when foreground) of syslog. This is mostly useful\n"
			"                       to avoid spamming syslog when log-level is high and\n"
			"                       not running foreground.\n"
			"     -i  --idle-time=T Time (sec) after which a stream will return to idle\n"
			"                       state if it receives no request. Default: 300s\n"
			"     -t --keep-location-time\n"
			"                       Combined with -L option, astreamfs will use the\n"
			"                       redirected location for the specified time (sec), when\n"
			"                       a new stream is needed. It can save a lot of overhead\n"
			"                       in situations where the redirected location is valid\n"
			"                       for some time.\n"
			"                       This option has no effect when -L is not specified.\n"
			"                       Default: --no-keep-location (see below)\n"
			"                       Both time option: 0 means forever.\n"
			"        --no-keep-location\n"
			"                       This is the default and cancels a previous -t.\n"
			"                       It means always restart from the original URL and never\n"
			"                       use twice a previous redirect location.\n"
			"     -P --path         Specifies a path for all subsequent URLs.\n"
			"                       Default (and after --next) is root: /\n"
			"     -S --size=N       Specify the file size (bytes).\n"
			"                       astreamfs won't try to get the file from the server.\n"
			"     -T --time=T       Specify the file modification date & time (timestamp).\n"
			"                       Default: same date & time as the mount directory.\n"
			"                       When specified, -R option is ignored.\n"
			"\n"
			"curl options used by astreamfs (`man curl` for detail):\n"
			"     -4 --ipv4\n"
			"     -6 --ipv6\n"
			"     -k --insecure\n"
			"        --cacert\n"
			"     -A --user-agent\n"
			"     -J --remote-header-name\n"
			"        --keepalive-time\n"
			"     -L --location\n"
			"        --no-keepalive\n"
			"        --output\n"
			"     -O --remote-name\n"
			"     -R --remote-time\n"
			"     -u --user (Note: authentication is set to basic as with --basic)\n"
			"     	 --url\n"
			"     -: --next\n"
			"\n"
			"     The default user agent sent in each request is: %s\n"
			"     You can change it with the -A option. To stop sending user agent use: -A ''\n"
			"     All fuse options, standard mount options, --user-agent, --log-level and\n"
			"     --log-file are global: eg. not specific to a single URL. They may all be\n"
			"     specified with the -o form of fuse, unlike options related to URLs.\n"
			"\n"
			"Multiple URLs:\n"
			"     All non global options applies to the 'current' URL.\n"
			"     When a new URL is specified, directly or with --url, it inherits all the\n"
			"     options set for the previous URL, except: --output, -S and -T\n"
			"     If you want options reset to default use: -: --next before the next URL.\n"
			"     Note: global options (see above) are not reset by --next.\n"
			"\n"
			"General options:\n"
			"    -o opt,[opt...]  mount options\n"
			"    -h   --help      print help\n"
			"    -V   --version   print version\n"
			"\n", default_user_agent);
		if (0 != fuse_opt_add_arg(outargs, "-ho"))
			lprintf(LOG_ERR, MSG_ERR_ADD_OPT_ARG);

		exit((0 == fuse_main(outargs->argc,
				     outargs->argv,
				     &astreamfs_oper,
				     NULL)) ? EXIT_SUCCESS : EXIT_FAILURE);


	case KEY_VERSION :
		fprintf(stderr, "Copyright (C) 2018-2020  Alain Bénédetti\n"
			"This program comes with ABSOLUTELY NO WARRANTY.\n"
			"This is free software, and you are welcome to redistribute it\n"
			"under the conditions of the GPLv3 or later, at your convenience.\n"
			"Full license text can be found here: https://www.gnu.org/licenses/gpl-3.0.html\n\n");
		fprintf(stderr, PROG_NAME": version "PROG_VERSION"\n");
		if (0 != fuse_opt_add_arg(outargs, "--version"))
			lprintf( LOG_ERR, MSG_ERR_ADD_OPT_ARG );

		exit((0 == fuse_main(outargs->argc,
				     outargs->argv,
				     &astreamfs_oper,
				     NULL)) ? EXIT_SUCCESS : EXIT_FAILURE);

	case KEY_PATH :
		count_path++;
		d->path_sz += canonicalize_path(arg, d->cur_abs_path) + 1;
		break;

	case KEY_CURL_NEXT :
		d->cur_abs_path[1] = '\0';
		break;

	case KEY_CURL_URL :
		d->last_noopt = NULL;
		count_URL++;
		break;
	case FUSE_OPT_KEY_NONOPT :
		d->last_noopt = arg;
		count_URL++;
		break;
	}
	return 0;
}


/*
 * @brief curl writedata callback for testing urls
 *
 * @param pointer on read data
 * @param size of a member
 * @param number of members
 * @param user private data
 * @return number of managed bytes
 */
static size_t write_devnull(char *ptr_unused,
			    size_t size_unused,
			    size_t nmemb_unused,
			    void *userdata_unused)
{
	(void)ptr_unused;
	(void)size_unused;
	(void)nmemb_unused;
	(void)userdata_unused;
	return 0;
}


/*
 * @brief Tests the URL passed as parameter
 *
 * This will also check various important things, such a whether we have
 * range support. It also measure times including potential redirects and
 * the estimated download speed.
 *
 * @param pointer to structure specifying the details of the URL to check
 * @return none
 */
 static void test_URL()
{
	CURL *curl;
	int i;
	struct filespec *fs;

	lprintf(LOG_NOTICE, "checking the URLs.\n");

	for (i = 0; i < count_URL; i++) {
		if (!F_LOCATION(&fss[i]))
			fss[i].flags &= ~FL_KEEP_LOCATION;
		if (!F_GOT_NAME(&fss[i]) ||
		    !F_GOT_SIZE(&fss[i]) ||
		    !F_GOT_TIME(&fss[i])) {
			curl= curl_init(&fss[i]);

			fs= fss +i;
			CURL_EASY_SETOPT(curl, CURLOPT_RANGE, "0-", "%s");
			stream_fs(curl,
				&fss[i],
				(curl_read_callback)write_devnull,
				fs);

			curl_easy_cleanup(curl);
		}
	}
}


/*
 * @brief consistency check on arguments passed to the command line
 *
 * Also prints out the parameters used if verbosity is >= INFO (6).
 *
 * @param none
 * @return none
 */
static void check_args(struct fuse_args *args)
{
	size_t s_name, s_type;
	char *fsname;

	lprintf(LOG_INFO, "log level is %d.\n",
	 params.log_level);
	if (NULL == params.user_agent)
		lprintf(LOG_INFO, "no user_agent string will be sent\n");
	else
		lprintf(LOG_INFO, "user_agent=%s\n", params.user_agent );
	if (params.foreground)
		lprintf(LOG_INFO, "foreground.\n");
	if (params.debug)
		lprintf(LOG_INFO, "debug.\n");
	fsname = (NULL == params.filesystem_name) ? fss[0].URL
						  : params.filesystem_name;
	s_name = strlen(fsname);
	if (NULL == params.filesystem_subtype) {
		params.filesystem_subtype= (char *)def_type;
		s_type= sizeof(def_type) - 1;
	}
	else {
		s_type= strlen(params.filesystem_subtype);
		lprintf(LOG_INFO, "filesystem_subtype=%s\n"
				, params.filesystem_subtype);
	}
	char add_opt[sizeof(ro_opt) + sizeof(subtype) + s_name + s_type - 1];
	memcpy(add_opt, ro_opt, sizeof(ro_opt) - 1 );
	memcpy(add_opt + sizeof(ro_opt) - 1, fsname, s_name);
	memcpy(add_opt + sizeof(ro_opt) + s_name - 1, subtype,
	       sizeof(subtype) - 1);
	memcpy(add_opt + sizeof(ro_opt) + s_name + sizeof(subtype) - 2,
	       params.filesystem_subtype, s_type + 1);
	lprintf(LOG_INFO, "adding options=%s\n",add_opt);
	fuse_opt_add_arg(args, add_opt);
}


/*
 *
 */
int main(int argc, char *argv[])
{
	unsigned int ret = 0, i;
	struct parse_data data = { 0, true, false, NULL, NULL, 2 };
	struct fuse_args exp_args = {argc, argv, false};
	struct fuse_args args;
	struct timespec t;

	clock_gettime(CLOCK_REALTIME, &t);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &main_start);

	expand_args(&exp_args, long_names, S_LONG_NAMES, end_expand);

	/* Count URLs, allocate memory for the FS and checks mount path */
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	args.allocated = false;
	{
		char buf[PATH_MAX];
		data.cur_abs_path = buf;
		buf[0] = '/';
		buf[1] = '\0';
		if (0 != fuse_opt_parse(&args,
					&data,
					astreamfs_opts,
					astreamfs_opt_count))
			lprintf(LOG_ERR, "parsing arguments.\n");
	}
	fuse_opt_free_args(&args);
	if (count_URL < 2)
		lprintf(LOG_ERR, "no URL or mount path specified.\n");
	count_URL--;

	check_mount_path(data.last_noopt);

	fss   = fs_alloc(count_URL * sizeof(struct filespec));
	params.mount_st.st_mode &= ~( S_IWUSR |	S_IWGRP | S_IWOTH );
	for (i = 0; i < count_URL; i++) {
		memcpy( &fss[i].st, &params.mount_st, sizeof(struct stat));
		fss[i].st.st_nlink = 1;
		fss[i].st.st_size = 0;
	}

	/* Parse arguments */
	params.user_agent = (char *)default_user_agent;
	paths = fs_alloc(count_path * sizeof(char *) + data.path_sz);
	paths[0] = (char *)(&paths[count_path]);
	paths[0][0] = '/';
	paths[0][1] = '\0';
	data.path_sz = 2;
	count_path = 1;
	init_url(&data);
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	{
		char buf[PATH_MAX];
		data.cur_abs_path = buf;
		buf[0] = '/';
		buf[1] = '\0';
		if (0 != fuse_opt_parse(&args,
					&data,
					astreamfs_opts,
					astreamfs_opt_proc))
			lprintf(LOG_ERR, "parsing arguments.\n");
	}
	end_init(count_URL - 1);

	start_message(&t.tv_sec);

	lprintf(LOG_INFO, "found %u URL.\n", count_URL);
	lprintf(LOG_INFO, "found %u paths\n", count_path);
	if (exp_args.allocated)
		free(exp_args.argv);

	/* Check arguments, test URLs and output debug information */
	check_args(&args);

	st_mode = params.mount_st.st_mode & ~(S_IFMT | S_ISVTX);
	st_mode |= S_IFREG;
	if (params.noexec)
		st_mode &= ~(S_IXUSR | S_IXGRP | S_IXOTH);
	if (params.nosuid)
		st_mode &= ~S_ISUID;
	for (i = 0; i < count_URL; i++) {
		fss[i].st.st_mode = st_mode;
	}

	if ( CURLE_OK != curl_global_init(CURL_GLOBAL_ALL) )
		lprintf( LOG_ERR, "initializing curl: curl_global_init().\n" );

	test_URL();

	dedup_path();

	if (params.log_level >= LOG_INFO)
		for (i= 0; i < count_URL; i++) {
			lprintf(LOG_INFO, "fss[%d].URL=%s\n", i, fss[i].URL);
			lprintf(LOG_INFO, "fss[%d].filename=%s%s\n"
					, i, fss[i].path, fss[i].filename);
			lprintf(LOG_INFO, "Remote file size=%"PRIu64"\n"
					, fss[i].st.st_size);
			lprintf(LOG_INFO, "flags=%x times: keepalive=%u idle=%u\n"
					, fss[i].flags
					, fss[i].curl_opt_keepalive_time
					, fss[i].idle_time);
			if (F_LOCATION(&fss[i]) && F_KEEP_LOCATION(&fss[i]))
				lprintf(LOG_INFO, "location=%s keep=%u\n"
						, fss[i].location
						, fss[i].keep_location_time);
			if (NULL != fss[i].userpwd) {
				lprintf(LOG_INFO, "User:pwd: (%p) `%.*s:***`\n"
						, fss[i].userpwd
						, strchr(fss[i].userpwd, ':') -
						  fss[i].userpwd
						, fss[i].userpwd );
			}
		}

	/*
	 * Starting the fuse mount now!
	*/
	lprintf(LOG_NOTICE,"Init done, starting fuse mount.\n");
	init_done= true;

	ret = fuse_main(args.argc,
			args.argv,
			&astreamfs_oper,
			NULL);

	/* Cleaning */
	curl_global_cleanup();
	fuse_opt_free_args(&args);
	for (i= 0; i < count_URL; i++) {
		free(fss[i].URL);
		free(fss[i].location);
		free(fss[i].filename);
		free(fss[i].userpwd);
	}
	if (default_user_agent != params.user_agent)
		free(params.user_agent);
	if (def_type != params.filesystem_subtype)
		free(params.filesystem_subtype);
	free(params.filesystem_name);
	free(fss);
	free(paths);
	for (i=0; i< MAX_READERS; i++)
		if (NULL != readers[i].curl)
			curl_easy_cleanup(readers[i].curl);
	return ret;
}
