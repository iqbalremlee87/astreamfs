/*
 * 1fichierfs:	mount your 1fichier account (fuse).
 *		This work is derived from astreamfs that is a generic
 *		http(s) fuse reader. It uses the 1fichier's API to manage
 *		the directory tree and get download tickets.
 *
 * Copyright (C) 2018-2021  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Feature:
 *	Same as astreamfs but "specialised" to 1fichier
 *	On top of reading files from the server in a stream-optimised way,
 *	the APIs allow to:
 *	- Automatically read remote directory content and synchronize to
 *	  the server tree.
 *	- Report the usage of "Cold Storage" and compute available storage.
 *	- Delete files and directories (`rm` and `rmdir` commands)
 *	- Rename -and move- (`mv` command) files and directories
 *	- Hard link files only (`ln` command, no soft link, hard link only)
 *	- Create directories (`mkdir` command)
 *      - write files (since 1.6.0) -limited to 'almost' sequential/ new files-
 *
 * Usage:
 *	Either
 *		read the help provided by: `1fichier -h` or
 *		look at the help section below, under astreamfs_opt_proc()
 *
 * Compile with:
 *	cc -Wall 1fichierfs.c	$(pkg-config fuse --cflags --libs)\
				$(curl-config --cflags --libs)\
				-o 1fichierfs -lpthread
 *
 * Compile dependancy:
 *	curl and fuse library
 *
 * Version: 1.8.4
 *
 *
 * History:
 * 	2021/03/07: 1.8.4 When writers busy: wait instead of returning EAGAIN
 * 	2021/03/02: 1.8.3 Deduplicate dir & files + rename on exist (for rsync)
 * 	2021/01/23: 1.8.2 Bug fix: date compute in resume.
 * 	2020/12/20: 1.8.1 Better "resume": mitigation of 403 and tweaks.
 * 	2020/12/05: 1.8.0 Write environment lazy initialisation + bug fixes.
 * 	2020/09/26: 1.7.2 small files write stay in memory during upload cycle.
 * 	2020/05/21: 1.7.1.1 statfs returns max name lg: needed by 20.04 Nautilus
 * 	2020/05/17: 1.7.1 Now wait network readiness on init (--network-wait=60)
 * 	2020/04/27: 1.7.0 Translation of forbidden characters (+ option not to).
 * 	2020/03/27: 1.6.5.3 Fix: read bug; Write errors code; Improvs; Cleaning.
 * 	2020/03/17: 1.6.5.2 Improved and simpler rename/del upload process.
 *	2020/03/14: 1.6.5.1 Crash: small buf (ftp_del) Fix: buf queue (prv_ins)
 *	2020/03/07: 1.6.5 Full feature version. + doc and upload dir protection.
 *	2020/02/29: 1.6.4 New: resuming first implementation.
 *	2020/02/01: 1.6.3 Bugfix and handling of special+forbid char in names.
 *	2020/01/20: 1.6.2 Write statistics, improvements and bugfixes.
 *	2020/01/12: 1.6.1 Del/Ren 100% Ok during write, better algo, bugfixes...
 *	2019/12/17: 1.6.0 MAJOR: writing is now enabled (create + sequential).
 *	2019/11/24: 1.5.5 Stats +contention FIX: engine (4 bugs) stats times (1)
 *	2019/11/22: 1.5.4 better and more resilient algo for download tokens.
 *	2019/11/10: 1.5.3 bug & crash fix on shares (+ optimised with --root)
 *	2019/10/30: 1.5.2 New: mount a path of the account + bug & crash fix.
 *	2019/10/17: 1.5.1 Crash: if ls.cgi API error / better options handling
 *	2019/08/06: 1.5.0 Shared folders including read-only + "hidden links".
 *	2019/06/10: 1.4.1 Crash: in size computation for stats read.
 *	2019/06/08: 1.4.0 New: added curl options --insecure and --cacert
 *	2019/06/08: 1.3.2 Crash:(Jaxx21@ubuntu-fr) typo with stats (parse_check)
 *	2019/06/03: 1.3.1 Fix: avg speed computation. Add: Memory stats.
 *	2019/05/28: 1.3.0 New: Statistics. Bug: engine broken on stream change.
 *	2019/05/22: 1.2.2 Fix bug: correctly display content of shared folders
 *	2019/05/19: 1.2.1 Fix crash: don't free streams when they are in use!
 *	2019/05/12: 1.2.0 Bugfixes (memory-management,...) / New no_ssl feature
 *	2019/05/11: 1.1.1 Fixed bugs on the first refactored version.
 *	2019/05/09: 1.1.0 Refactored live streams (avoid getting new DL-tickets)
 *	2019/04/28: 1.0.11 fix: arg subtype, JSON-esc names mv/cp,stat st_blocks
 *	2019/04/02: 1.0.10 ls: folders+files/New storage algo/Engine: header chk
 *	2019/03/24: 1.0.9 API chg: opti ls folder+files/ date ok dirs/ del optim
 *	2019/03/05: 1.0.8 API change: atomic and much simpler mv (folders)
 *	2019/03/04: 1.0.7 API change: atomic and much simpler ln (files)
 *	2019/02/28: 1.0.6 API change: atomic mv (files) in all cases
 *	2019/02/23: 1.0.5 API change: rename/move directory is now possible!
 *	2019/02/09: 1.0.4 info API changed; refr-time = min; protect refrsh-file
 *	2019/02/03: 1.0.3 Clean + make it compile and work on 32bits!
 *	2019/02/03: 1.0.2 API change: limit 1/5min user/info + get max storage.
 *	2019/01/21: 1.0.1 mkdir added.
 *	2019/01/19: 1.0.0 Last possible realistic implem: hard linking.
 *	2019/01/18: 0.9.7 Rename files in all cases (rename + move).
 *	2019/01/16: 0.9.6 Remove file (rm) and directory (rmdir) implemented.
 *	2019/01/13: 0.9.5 Fix bugs on statfs / (RW) Rename files same directory
 *	2019/01/05: 0.9.1 Resist to server error, retry (429) and free storage!
 *	2019/01/01: 0.9.0 Cope with server out of sync + Valgrinded bugs.
 *	2018/12/29: 0.8.7 Complete refresh: trigger, hidden and timer.
 *	2018/12/24: 0.8.6 First version with refresh and RCU freeing.
 *	2018/12/23: 0.8.5 Separate file and dir requests in preparation of RCU.
 *	2018/12/22: 0.8.4 Several bugfix + API Key can be specified in a file.
 *	2018/12/16: 0.8.3 Cleaning: moved common code to astreamfs_util.c/h
 *	2018/12/09: 0.8.2 "Good enough" initial specialized version, based on
 *			astreamfs as of version 0.8.2
 *
 *
 * Notes:
 *  Clocks: we use mainly CLOCK_MONOTONIC_COARSE, and CLOCK_REALTIME only when
 *   necessary, that is to display start time, and for sem_timedwait.
 *   To correctly time the 'download ticket' validity period, CLOCK_BOOTTIME
 *   should be used instead, because it takes care of suspend time. However,
 *   CLOCK_BOOTTIME is more than 50 times slower that CLOCK_MONOTONIC_COARSE,
 *   because it does a system call instead of using vdso (simulated system call)
 *   So it might happen, when resuming after suspend, that a 'download ticket'
 *   the program sees as valid is in fact invalid. Anyway, the server will
 *   respond with 410 (gone) and a new ticket will be requested. This
 *   possible short delays after resume are the price to pay for the
 *   simplification and optimisation. Anyway, there are many things that have
 *   to be "restarted" after resume, first one being the network!..
 *
 * May 2019 Refactoring:
 *  Information needed to display the directory tree and needed to stream are
 *  now separate. struct file gets the former, and struct stream the latter.
 *  Common parts: size and URL are copied on open().
 *  This allows:
 *    - To keep download tickets for their full duration (30 min) regardless
 *	of any refresh() that happened during the "ticket" validity period.
 *    - A more straightforward RCU free code since it does not need anymore to
 *	check if files are opened or not.
 *    - Lower memory footprint (on normal conditions assuming a reasonable
 *      amount of files are opened at the same time). This is also true because
 *      the 'st' structure (144 bytes in 64bits) is now replaced in 'file' by
 *      the only information we need for files: size and create date (16 bytes).
 *      That change alone saves 128 bytes for each file currently cached in the
 *      directory tree.
 *  Other changes:
 *    - The list of opened files is managed mainly at open().
 *    - refresh() takes care of potential files that were marked as ERROR.
 *	This is a feature.
 *	When there is a read error on a file, it is marked as error and EIO or
 *	EREMOTEIO are returned. Subsequent open() of the same file return
 *	EACCESS. Any refresh will purge the files that were marked as error
 *	from the list, allowing the user to retry.
 *    - release() only decrements the opened counter.
 *    - Also release() does NOT send the close signal to the async_reader unless
 *	the file is marked as error. This means that this stream will stay live
 *	for 45 seconds and could serve requests for the same file. That works
 *	well for files under 16K (curl default buffer), they will be served
 *	from memory within the 45 seconds regardless of kernel cache parameter.
 *    - This change also makes open() and release() completely lockless.
 *	Note that open() is not waitless since the new 'live' (opened and
 *	alive) 'streams' struct is managed with atomics & RCU (no mutex locks).
 *
 * Note: using only the CA that signs *.1fichier.com certificates greatly
 *       reduces the memory footprint on GnuTLS (around 32MB instead of 96MB!)
 *       To make this CA file, look at the curl documentation or do:
$ openssl s_client -showcerts -servername 1fichier.com -connect 1fichier.com:443 >1fichier.pem
 *       Then extract from that file the first certificate (--begin/--end cert)
 */

// TODO: we really need a test program/shell to avoid regressions!

#define _GNU_SOURCE
#define FUSE_USE_VERSION 26

#include <fuse.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stddef.h>
#include <sys/statvfs.h>
#include <signal.h>

#include "1fichierfs.h"

/*******************************************************
 *  These are the things you can change at compile time.
 */

/*  See 1fichier.h to change that:
#define PROG_NAME	"1fichierfs"
#define PROG_VERSION	"1.2.0"
*/
       const char log_prefix[] = PROG_NAME;	/* Prefix for log messages    */

extern const char def_user_agent[];		/* See 1fichierfs_options.c   */
extern const char def_type[];			/* See 1fichierfs_options.c   */
extern const char def_fsname[];			/* See 1fichierfs_options.c   */

/* See definition and comments in 1fichier.f
#define DEF_JSON_BUF_SIZE (16 * 1024 + 1) */
						/* Upon HTTP 429 (too many
						 * requests), this sets delays
						 * and number of retries.     */
static const useconds_t retry_delay_ms[]= {  350000,  700000, 1000000 };

#define NETWORK_WAIT_INTVL (2) /* Wait interval (sec) for network to be ready */

/*
#define MAX_READERS 4	 can also be changed. it is located in astreamfs_util.h
*/

static const char hidden_url[] = "{hidden}";

/********************************************************
 *  Do NOT change the following definitions or variables.
 *  Things would break!
 *  These definitions and variables are either system dependent,
 *  or depend on how 1fichier works.
 */
#define ST_BLK_SZ (512)		/* This is the block size stat's st_blocks */
#define DEFAULT_BLK_SIZE (4096)	/* This is the block size for statvfs */

#define URL_API_EP "https://api.1fichier.com/v1/"
static const char url_api_ep[]		= URL_API_EP;
static const char *api_urls[] = {
/*	FOLDER_LS		= 0, */ URL_API_EP"folder/ls.cgi",
/*	DOWNLOAD_GET_TOKEN	= 1, */ URL_API_EP"download/get_token.cgi",
/*	USER_INFO		= 2, */ URL_API_EP"user/info.cgi",
/*	FILE_MV			= 3, */ URL_API_EP"file/mv.cgi",
/*	FILE_CHATTR		= 4, */ URL_API_EP"file/chattr.cgi",
/*	FILE_RM			= 5, */ URL_API_EP"file/rm.cgi",
/*	FILE_CP			= 6, */ URL_API_EP"file/cp.cgi",
/*	FOLDER_MV		= 7, */ URL_API_EP"folder/mv.cgi",
/*	FOLDER_RM		= 8, */ URL_API_EP"folder/rm.cgi",
/*	FOLDER_MKDIR		= 9, */ URL_API_EP"folder/mkdir.cgi",
/*	FTP_USER_LS		=10, */ URL_API_EP"ftp/users/ls.cgi",
/*	FTP_USER_RM		=11, */ URL_API_EP"ftp/users/rm.cgi",
/*	FTP_USER_ADD		=11, */ URL_API_EP"ftp/users/add.cgi",
/*	FTP_PROCESS		=13, */ URL_API_EP"ftp/process.cgi",
};

static const char *refresh_msgs[] = {
/*	REFRESH_TRIGGER		= 0, */ "trigger",
/*	REFRESH_HIDDEN		= 1, */ "hidden",
/*	REFRESH_TIMER		= 2, */ "timer",
/*	REFRESH_POLLING		= 3, */ "polling",
/*	REFRESH_WRITE		= 4, */ "write",
/*	REFRESH_MV		= 5, */ "move",
/*	REFRESH_LINK		= 6, */ "link",
/*	REFRESH_UNLINK		= 7, */ "unlink",
/*	REFRESH_MKDIR		= 8, */ "mkdir",
/*	REFRESH_RMDIR		= 9, */ "rmdir",
/*	REFRESH_404		=10, */ "HTTP 404",
/*	REFRESH_EXIT		=11, */ "exit",
/*	REFRESH_INIT_ROOT	=12, */ "init_root",
};


/* These are constants set to 1fichier timeouts */
#define CURL_KEEPALIVE_TIME	( 20L)
#define CURL_IDLE_TIME		(  45)	/* As of 1.3.0: Strange behaviour when
					   reusing a "keep-alived" TCP stream
					   after 1 min, there is ~20 sec delay
					   to restart (or even sometimes curl
					   error 50: RCV_ERROR). Keeping readers
					   only for 45 seconds avoids the issue,
					   at the expense of more 'curling'
					   which is faster than waiting
					   ~20/25 seconds!
					    Was: 300 for 1fichier, 295 safer! */
#define CURL_KEEP_LOCATION_TIME (1800)  /* 30 minutes as documented.          */
#define CURL_CNX_TIMEOUT	( 30L)

#define STREAM_ERROR (-1L)	   /* HTTP status codes can't have that value */

/*****************************************/
/** Fuse behaviour: "hidden" files.
 * - For deletion, fuse does not trust the UserSpace drive to correctly do RCU
 *   and uses a trick. Unless the -o hard_remove option is passed, when a file
 *   is deleted while it is opened, fuse will rename it instead, so that
 *   subsequent read/write operation succeed.
 *   Once all handles on that file are closed (or upon fusermount -u) the
 *   renamed files are deleted.
 * - When hard_remove option is passed, that does not happen and files are
 *   immediately deleted even when they are open. In this case, fuse responds
 *   -ENOENT to the subsequent read/write on the handles without calling the
 *   driver.
 *
 * 1fichierfs
 * - hides those files in the directory list
 * - does proper RCU on write: no need uploading to save bandwidth!
 * - //TODO test if these names can be received as source/target of rename/link
 *   and if so filter them out.
 */

static const char fuse_hidden[] = ".fuse_hidden";

/****/

struct params1f params1f= {
/* curl_IP_resolve */	CURL_IPRESOLVE_WHATEVER,
/* api_key	   */	NULL,
/* n_no_ssl	   */	0UL,
/* a_no_ssl	   */	NULL,
/* stat_file	   */	NULL,
/* refresh_file	   */	NULL,
/* ftp_user	   */	NULL,
/* refresh_time	   */	0UL,
/* resume_delay    */   97200UL, /* 27h = 24h (manual FTP) + 300G max size */
/* refresh_hidden  */	false,
/* readonly	   */	false,
/* insecure	   */	false,
/* no_upload	   */	false,
/* dry_run	   */	false,
/* raw_names	   */	false,
/* uid		   */	-1,
/* gid		   */	-1,
};



mode_t st_mode;
				      /* auth_header is auth_bearer + API key */
static const char auth_bearer[]= "Authorization: Bearer ";
static char *auth_header = NULL;

_Atomic bool exiting = false;
_Atomic unsigned long refresh_start;


#define S_ERROR(s) (0 != (LOAD(((s)->counter)) & FL_ERROR))
#define F_DEL(f)   (0 != (LOAD(((f)->flags  )) & FL_DEL  ))


_Atomic (struct streams *)live = NULL;

struct file {
	const char		*URL;
	const char		*filename;
	time_t			cdate;
	unsigned long long	size;
	_Atomic unsigned int	flags;
};

#define ACCESS_RO	(1)
#define ACCESS_HIDDEN	(2)
#define ACCESS_MASK	(3)
#define ACCESS_SHIFT	(2)

#define IS_HIDDEN(a) (ACCESS_HIDDEN == ((a) & ACCESS_MASK))
#define IS_SHARED_ROOT(d) (0 == d->id && ACCESS_RW != d->access)
#define SHARER_EMAIL(d) (d->name + (d->access >> ACCESS_SHIFT))

/* On the root of the share, the field holds the offset of the sharer's
 * e-mail on the name field, shifted 2 positions, and the last 2 bits are
 * one of above: ACCESS_RW, ACCESS_RO or ACCESS_HIDDEN.
 * For descendants of the shares, they inherit only these last 2 bits.
 */


struct dentries {
	unsigned long	n_subdirs;
	unsigned long	n_files;
	struct dir	*subdirs;
	struct file	files[];
};

static char def_root[] =  "/";

static struct dir root = {
/*	*d		*/	NULL,
/*	lookupmutex	*/	PTHREAD_MUTEX_INITIALIZER,
/*	name		*/	def_root,
/*	id		*/	0,
/*	cdate		*/	0,
/*	access		*/	ACCESS_RW,
			  };

static struct dir upload_d = {
/*	*d		*/	NULL,
/*	lookupmutex	*/	PTHREAD_MUTEX_INITIALIZER,
/*	name		*/	NULL,
/*	id		*/	UPLOAD_DIR_ID_NOT_INITIALISED,
/*	cdate		*/	0,
/*	access		*/	ACCESS_RW,
			  };

/* setter/getter for upload_dir_id.
 * Setter must be called in a single thread before any getter to avoid locks */
long long get_upload_id(void)		{ return upload_d.id;	   }
void	  set_upload_id(long long id)	{	 upload_d.id = id; }

/* Note: 1fichier has put a hard limit of minimum time 5 minutes between calls
 *	 to user_info. Calling more often triggers a 403, and if too many
 *	 calls are made despite the 403, the user is temporarily banned!
 *	 We could RCU the storage values, but for the moment it is not worth
 *	 it, a simple mutex (needed anyway) seems enough.
 *	 Hence all storage related variable don't need to be atomic, they
 *	 are read/written under lock.
 */
#define USER_INFO_DELAY (305) /* 5min + 5sec to be sure: time of req + round */
#define FL_STOR_ERROR	1
#define FL_STOR_REFRESH 2

static pthread_mutex_t	storage_mutex = PTHREAD_MUTEX_INITIALIZER;
static off_t		avail_storage;
static off_t		max_storage;
static time_t		storage_next_update = 0;
static unsigned int	storage_flags = FL_STOR_REFRESH;

/* Variables and define used when fallback on download/get_token API is needed.
 */
static pthread_mutex_t	        fallback_mutex = PTHREAD_MUTEX_INITIALIZER;
static unsigned long		fallback_tested = 0;
static _Atomic time_t		fallback_until  = 0;
static _Atomic int		fallback_in_use = 0;
#define KEEP_FALLBACK_TIME (300)

/* Variables and define for the API call throttling and retries.
 */
static _Atomic unsigned long n_API_calls = 0;
static _Atomic bool throttled = false;
static pthread_mutex_t	apimutex = PTHREAD_MUTEX_INITIALIZER;
		/* How many times the retry can happen is derived from
		 * the number of elements of retry_delay_ms (see above) */
#define MAX_RETRY (sizeof(retry_delay_ms)/sizeof(retry_delay_ms[0]))

/* Thread local node and readers.
 */
static __thread struct node work = {NULL,NULL};

struct reader readers[MAX_READERS];

/*
 * Specific rcu cleaner functions for dentries
 */
static void rcu_free_dent(struct dentries *dent)
{
	unsigned long i;

	for (i = 0; i < dent->n_subdirs; i++) {
		if (NULL != dent->subdirs[i].dent) {
			DEBUG(	"Freeing dentries for subdir: %s/%p\n",
				dent->subdirs[i].name,
				dent->subdirs[i].dent);
			rcu_free_dent(dent->subdirs[i].dent);
		}
	}

	DEBUG("Freeing dentries: %p\n", dent);
	fs_free(dent);
}

static void rcu_free_dentries(struct rcu_head *head)
{
	rcu_free_dent(rcu_get_p_struct_from_head(head));
}


bool is_fuse_hidden(const char *filename) {
	return (0 == strncmp(filename, fuse_hidden, sizeof(fuse_hidden) -1));
}

static size_t get_json_response( const char *ptr, const size_t size,
				 const size_t nmemb, void *userdata)
{
	size_t chunk = size * nmemb;
	struct json *j = (struct json *)userdata;

	if ( j->cb + chunk + 1 > DEF_JSON_BUF_SIZE ) {
		if (j->cb < DEF_JSON_BUF_SIZE) {
			j->pb = fs_alloc(j->cb + chunk + 1);
			memcpy(j->pb, j->buf,j->cb);
		}
		else {
			j->pb = fs_realloc(j->pb, j->cb + chunk + 1);
			if ( NULL == j->pb )
			lprintf(LOG_CRIT,
				"out of memory. Cannot allocate %lu bytes.\n",
				j->cb + chunk + 1);
		}
	}
	memcpy(j->pb + j->cb, ptr, chunk);
	j->cb += chunk;
	return chunk;
}

void json_init(struct json *j)
{
	j->cb = 0;
	j->pb = j->buf;
}

void json_free(struct json *j)
{
	if (j->pb != j->buf)
		fs_free(j->pb);
}

/* TODO use a real JSON parser instead of strstr
 *   Instead, to search the member "foo", we search directly "foo", since we
 *   know keys are NOT escaped by 1fichier. We also check that after "foo"
 *   we find some whitespace then a ':'. Otherwise we loop since "foo" could
 *   also have been a value.
 *   That works because :
 *	- keys are NOT escaped by 1fichier
 *	- valid json cannot contain "foo" inside a string (it would have to
 *	  be escaped like "bar\"foo\"bar", hence strstr won't find "foo")
 *
 */

#define JSON_STR_ERR ((size_t)-1)
       const char json_item_start[]= JSON_ITEM_START;
static const char json_escapes[] = "\"\\/bfnrt";
static const char json_sub_folders[]= "\"sub_folders\"";
       const char json_items[]= "\"items\"";
static const char json_shares[]= "\"shares\"";
       const char json_filename[]= "\"filename\"";
       const char json_url[]= "\"url\"";
static const char json_email[]= "\"email\"";
static const char json_hide_links[]= "\"hide_links\"";
static const char json_rw[]= "\"rw\"";
static const char json_size[]= "\"size\"";
static const char json_date[]= "\"date\"";
static const char json_name[]= "\"name\"";
static const char json_id[]= "\"id\"";
static const char json_cdate[]= "\"create_date\"";
static const char json_cold_storage[]= "\"cold_storage\"";
static const char json_hot_storage[]= "\"hot_storage\"";
static const char json_allowed_cold_storage[]= "\"allowed_cold_storage\"";
       const char json_folder_id[]= "\"folder_id\"";
static const char json_status[]= "\"status\"";
static const char json_OK[]= "\"OK\"";
static const char json_message[]= "\"message\"";

/* Translations to avoid forbidden characters.
 */
static struct {
	char from;
	char to[3];
	int  files;
} translation[] = {
	{ '"'   , { '\xef', '\xbc', '\x82' }, 0 },
	{ '$'   , { '\xef', '\xbc', '\x84' }, 1 },
	{ '\x27', { '\xef', '\xbc', '\x87' }, 0 },
	{ '<'   , { '\xef', '\xbc', '\x9c' }, 1 },
	{ '>'   , { '\xef', '\xbc', '\x9e' }, 1 },
	{ '\x5c', { '\xef', '\xbc', '\xbc' }, 1 },
	{ '`'   , { '\xef', '\xbd', '\x80' }, 1 },
};

/* Head and tail spaces protection
 */
static char head_repl[3] = { '\xe2', '\x81', '\xa0' };


char *json_find_key(const char *json, const char *str)
{
	char *p;
	do
	{
		p = strstr(json, str);
		if (NULL == p)
			return NULL;
		p += strlen(str);
		while ( *p == '\x09' || *p == '\x0A' ||
			*p == '\x0D' || *p == ' ')
			p++;
	} while (':' != *p);
	p++;
	while ( *p == '\x09' || *p == '\x0A' ||
		*p == '\x0D' || *p == ' ')
		p++;
	return p;
}

static int json_u_decode(const char *esp, char *dest)
{
	unsigned i;
	char cpc[5];
	uint32_t cp;

	for (i = 0; i < 4; i++) {
		cpc[i] = esp[i];
		if (!isxdigit(esp[i]))
			return 0;
	}
	cpc[i] = '\0';
	cp = strtol(cpc, NULL, 16);
	/* \u0000 in the json input for dir and file names is forbidden. */
	if (0 == cp)
		return 0;
	if (cp < 0x80) {
		*dest = (char)cp;
		return 1;
	}
	if (cp < 0x800) {
		*dest++ = (cp >> 6)   | 0xC0;
		*dest   = (cp & 0x3F) | 0x80;
		return 2;
        }
	if (cp < 0x10000) {
		*dest++ = (cp >> 12)	     | 0xE0;
		*dest++ = ((cp >> 6) & 0x3F) | 0x80;
		*dest++ = (cp	     & 0x3F) | 0x80;
		return 3;
	}
	if (cp < 0x110000) {
		*dest++ = (cp >> 18)	     | 0xF0;
		*dest++ = ((cp >>12) & 0x3F) | 0x80;
		*dest++ = ((cp >> 6) & 0x3F) | 0x80;
		*dest++ = (cp        & 0x3F) | 0x80;
		return 4;
        }
	return 0;
}

/* When translating, skip a lead or trail mark */
#define trans_skip_lead(p, tr) 			\
		((TR_NONE == tr || params1f.raw_names) ? p : 	\
				((*(p)     == head_repl[0] &&	\
				  *(p + 1) == head_repl[1] &&	\
				  *(p + 2) == head_repl[2])   ? p + 3 : p))

#define trans_skip_trail(p, tr) 		\
		((TR_NONE == tr || params1f.raw_names) ? false :\
				((*(p)     == head_repl[0] &&	\
				  *(p + 1) == head_repl[1] &&	\
				  *(p + 2) == head_repl[2] &&	\
				  *(p + 3) == '"' )   ? true : false))

/*
 * @brief performs a reverse translation from the server's name
 *
 * If there is a match in the translation table (to) returns the reverse
 * translation character (from) and skips 2 characters.
 * Otherwise, or in raw_names mode, returns the character at the position
 * of the pointer and skip is zero.
 *
 * @param start point of the translation
 * @param (output) number of chars to skip
 * @param type of translation to perform
 * @return translation (a single character)
 */

static char rev_trans(const char *p, size_t *skip, enum translation_type tr)
{
	unsigned int i;

	*skip = 0;
	if (TR_NONE == tr || params1f.raw_names)
		return *p;
	for (i = 0; i < sizeof(translation) / sizeof(translation[0]); i++) {
		if (*p < translation[i].to[0])
			break;
		if (*p == translation[i].to[0]) {
			if (*(p + 1)  < translation[i].to[1])
				break;
			if (*(p + 1) == translation[i].to[1]) {
				if (*(p + 2)  < translation[i].to[2])
					break;
				if (*(p + 2) == translation[i].to[2]) {
					if (TR_DIR  == tr ||
					    0 != translation[i].files) {
						*skip = 2;
						return translation[i].from;
					}
				}
			}
		}
	}
	return *p;
}


/*
 * @brief performs a translation to a compatible name for the server
 *
 * If there is a match in the translation table (to):
 * - in raw mode, this is an error (impossible character) then 0 is returned
 * - otherwise stores the translation and returns the translation size (3)
 *
 * If no match, stores the current character and returns 1 (size of char!)
 *
 * @param character to translate
 * @param (output) where to store the translation
 * @param type of translation to perform (TR_FILE or TR_DIR)
 * @return number of characters outputed
 */
size_t translate(char c, char *trans, enum translation_type tr)
{
	unsigned int i;

	for (i = 0; i < sizeof(translation) / sizeof(translation[0]) - 1; i++)
		if (c <= translation[i].from)
			break;
	if (c != translation[i].from ||
	    (TR_FILE  == tr && 0 == translation[i].files)) {
		*trans = c;
		return sizeof(char);
	}
	if (params1f.raw_names)
		return 0;
	*(trans    ) = translation[i].to[0];
	*(trans + 1) = translation[i].to[1];
	*(trans + 2) = translation[i].to[2];
	return sizeof(translation[0].to);
}

/*
 * @brief length of the json string once translated as specified
 *
 * The length takes into account escaped json characters that will be
 * 'unescaped' and special translations needed for files and directories
 * as specified for that call.
 *
 * @param json string (points on the leading ")
 * @param type of translation needed
 * @return length of the translated string
 */

size_t json_strlen(const char *json, enum translation_type tr)
{
	const char *p = json;
	size_t	s = 0, sz;

	if (NULL == p || '"' != *p)
		return JSON_STR_ERR;
	for (p = trans_skip_lead(p + 1, tr); '\0' != *p && '"' != *p; p++, s++)
	{
		if ('\\' == *p) {
			p++;
			if ('u' == *p) {
				char decode[4];
				sz = json_u_decode(p + 1, decode);
				if (0 == sz)
					return JSON_STR_ERR;
				p += 4;
				s += sz - 1;
				// TODO surrogate pairs
			}
			else
				if (NULL == strchr(json_escapes, *p))
					return JSON_STR_ERR;

		}
		else {
			if (trans_skip_trail(p, tr))
				break;
			rev_trans(p, &sz, tr);
			p += sz;
		}
	}
	return s;
}

/*
 * @brief copies the json string to destination with translation as specified
 *
 * Destination must be big enough for the copy. json_strlen with the same
 * translation will ensure that.
 *
 * @param json string (points on the leading ")
 * @param destination of the translation
 * @param type of translation needed
 * @return length of the translated string
 */

char *json_strcpy(const char *json, char *dest, enum translation_type tr)
{
	size_t sz;
	const char *p = json;

	if (NULL == p || '"' != *p)
		return NULL;
	p++;
	for (p = trans_skip_lead(p, tr); '\0' != *p && '"' != *p; p++, dest++)
	{
		if ('\\' == *p) {
			p++;
			switch (*p) {
				case '"' : *dest = '"';
					   break;
				case '\\': *dest = '\\';
					   break;
				case '/' : *dest = '/';
					   break;
				case 'b' : *dest = '\x08';
					   break;
				case 'f' : *dest = '\x0C';
					   break;
				case 'n' : *dest = '\x0A';
					   break;
				case 'r' : *dest = '\x0D';
					   break;
				case 't' : *dest = '\x09';
					   break;
				case 'u' : sz = json_u_decode(p + 1, dest);
					   if (0 == sz)
						return NULL;
					   p    += 4;
					   dest += sz - 1;
					   break;
				default	 : return NULL;
			}
		}
		else {
			if (trans_skip_trail(p, tr))
				break;
			*dest = rev_trans(p, &sz, tr);
			p += sz;
		}
	}
	*dest = '\0';
	return dest + 1;
}


/*
 * @brief check filename illegal spaces, chars, and length
 *
 * Both the API and the Web interface SILENTLY remove some whitespace (table
 * below) before/after the name. This behaviour being unsuitable for
 * a filesystem, when using a filename for creation, this function must be
 * called to check for leading/trailing "spaces".
 *
 * At the same time, it also checks the filename length and other forbidden
 * characters to avoid having to call the API if it is know it will fail.
 *
 * @param string to check
 * @param type of translation needed (TR_FILE or TR_DIR)
 * @return O or code ENAMETOOLONG (speaks by itself) or illegal spaces: EILSEQ
 */

/* Trimmed are characters from 0x09 (TAB) to 0x0D (LF) and 0x20 (Space: ' ') */

#define isforbidden(c) ((0x09 <= c && 0x0D >= c) || 0x20 == c)


static int forbidden_space(const char *str) {
	DEBUG(	"forbidden leading or trailing spaces in `%s`\n", str);
	return -EILSEQ;
}

int forbidden_chars(const char *str, enum translation_type tr)
{
	const char *p;
	size_t len, skip, sz;
	char buf[3];

	/* Check only the last part of the path, the other parts are tested
	 * by fuse before the current call, checking each part of path */
	p = strrchr(str, '/');
	if (NULL == p)
		p = str;
	else
		p++;

// TODO in NOT raw-names, should not start with head_repl

	if (isforbidden(*(p))) {
		if (params1f.raw_names)
			return forbidden_space(str);
		else
			len = 3;
	}
	else {
		len = 0;
	}

	for (; '\0' != *p; p++) {
		sz = translate(*p, buf, tr);
		if (0 == sz) {
			DEBUG(	"forbidden character in `%s` near %c (0x%02X)\n",
				str, *p, (int)*p);
			return -EILSEQ;
		}
		len += sz;
		if (!params1f.raw_names) {
			rev_trans(p, &skip, tr);
			if (0 != skip) {
				DEBUG(	"forbidden sequence in `%s`: %c%c%c\n",
					str, *p, *(p+1), *(p+2));
				return -EILSEQ;
			}
		}
	}

	/* Then check 'trailing space' */
	if (isforbidden(*(p - 1))) {
		if (params1f.raw_names)
			return forbidden_space(str);
		else
			len += 3;
	}
	if (MAX_FILENAME_LENGTH >= len)
		return 0;
	else
		return -ENAMETOOLONG;
}


/*
 * @brief translates and JSON-escapes a filename or directory name
 *
 * Copy the string into the buffer, translating and JSON escaping when
 * necessary, according to the translation pattern required.
 * The trailer string is then appended at the end.
 *
 * The buffer length must be MAX_ESCAPED_LENGTH + sizeof(trailer) to fit
 * the worst case scenario (all characters escaped to \unnnn).
 *
 * @param string to be tranlasted and escaped
 * @param buffer where the translation is copied
 * @param type of translation needed (TR_FILE or TR_DIR)
 * @param trailer to append.
 * @return none
 */

static void name_translate(const char *str, char *buf,
			   enum translation_type tr, const char *trailer)
{
	const char *p = str;
	/* Leading space, have been filtered out for raw name as EILSEQ */
	if (!params1f.raw_names)
		if (isforbidden(*p) ||
		    (p[0] == head_repl[0] &&
		     p[1] == head_repl[1] &&
		     p[2] == head_repl[2])) {
			buf[0] = head_repl[0];
			buf[1] = head_repl[1];
			buf[2] = head_repl[2];
			buf += 3;
		}


	/* Copy with escape and translation if needed */
	for (; '\0' != *p; p++)
		if (0 == (*p & 0x80) && 0x20 > *p) {
			sprintf(buf, "\\u%04x", (int)*p);
			buf += 6;
		}
		else if ( *p == '"' ) { /* \ is translated */
			buf[0] = '\\';
			buf[1] = '"';
			buf += 2;
		}
		else {
			if (params1f.raw_names)
				*buf++ = *p;
			else
				buf += translate(*p, buf, tr);
		}

	/* Trailing space */
	if (!params1f.raw_names)
		if (isforbidden(*(p - 1)) ||
		    (p - str >= 3 &&
		     p[-3] == head_repl[0] &&
		     p[-2] == head_repl[1] &&
		     p[-1] == head_repl[2])) {
			buf[0] = head_repl[0];
			buf[1] = head_repl[1];
			buf[2] = head_repl[2];
			buf += 3;
		}

	strcpy(buf, trailer);
}

static int cmp_files(const void *f1,const void *f2)
{
	return strcmp(((const struct file *)f1)->filename,
		      ((const struct file *)f2)->filename);
}

static void init_fs(struct file *fs)
{
	STORE(fs->flags, 0);
}

static int cmp_dirs(const void *d1,const void *d2)
{
	return strcmp(((const struct dir *)d1)->name,
		      ((const struct dir *)d2)->name);
}

static char *parse_check(char  *ptr, struct dir *d, const char * msg) {
	if (NULL == ptr)
		return NULL;
	if ( '[' == *ptr ) {
		*ptr = '\0';
		return ptr + 1;
	}
	else {
		lprintf(LOG_ERR,
			"Ignoring: %s of `%s` id=%"PRId64", is not an array!\n",
			msg, d->name, d->id);
		return NULL;
	}
}

static void parse_err(const char * pattern, struct dir *d)
{
	lprintf(LOG_ERR,
		"Ignoring: error parsing %s of `%s` id=%"PRId64"\n",
		pattern, d->name, d->id);
}


static size_t json_value_length(const char *json, const char *key, char **val,
				enum translation_type tr)
{
	char *p;
	size_t sz;

	if (NULL != val)
		*val = NULL;
	p = json_find_key(json, key);
	if (NULL == p)
		return 0;
	sz = json_strlen(p, tr);
	if (NULL != val)
		*val = p + 1;
	return (JSON_STR_ERR == sz) ? 0 : sz;
}


static char name_email_link[]= " on ";

static unsigned long parse_count(const char *s, const char *pattern1,
				 const char *pattern2, size_t add,
				 size_t *s_str, bool *f_refresh, bool *f_stat,
				 struct dir *d)
{
	unsigned long n;
	char *p, *q, *val;
	size_t t, sz = 0, r1 = 0, r2 = 0;
	bool f_r = *f_refresh, f_s = *f_stat;

	if (NULL == s)
		return 0;
	p = strstr(s, json_item_start);
	if (NULL == p)
		return 0;
	if (f_r)
		r1 = strlen(params1f.refresh_file);
	if (f_s)
		r2 = strlen(params1f.stat_file);
	n = 0;
	while(true) {
		q = strstr(p + sizeof(json_item_start), json_item_start);
		if (NULL != q)
			*q = '\0';
		t = json_value_length(p, pattern1, &val,
				      (0 == add) ? TR_FILE : TR_DIR);
		if (0 == t) {
			parse_err(pattern1, d);
			return 0;
		}
		sz += t + 1;
		if (f_r && r1 == t
			&& 0 == strncmp(val, params1f.refresh_file, r1)) {
			lprintf(LOG_WARNING,
				"refresh triggger off: a file or directory `/%s` already exists in `%s`.\n",
				params1f.refresh_file, d->name);
			*f_refresh = f_r = false;
		}
		if (f_s && r2 == t
			&& 0 == strncmp(val, params1f.stat_file, r2)) {
			lprintf(LOG_WARNING,
				"statistics off: a file or directory `/%s` already exists in `%s`.\n",
				params1f.stat_file, d->name);
			*f_stat = f_s = false;
		}

		if (NULL != pattern2) {
			t = json_value_length(p, pattern2, NULL, TR_NONE);
			if (0 == add) {
				if (0 == t) {
					parse_err(pattern2, d);
					return 0;
				}
				sz += t + 1;
			}
			else {
				if (0 != t)
					sz += t + add;
			}
		}
		n++;
		if (NULL == q)
			break;
		*q = json_item_start[0];
		p = q;
	}
	*s_str = sz;
	return n;
}

static bool fill_folders(char *start, struct dentries *dent, char *s,
			 unsigned int access, bool is_root)
{
	char *p, *q;
	struct dir *x;
	struct tm tm;

	x = dent->subdirs;
	p = strstr(start, json_item_start);
	if (NULL == p) {
		lprintf(LOG_ERR, "unexpected condition in fill_folders.\n");
		return false;
	}
	while(true) {
		start = strstr(p + sizeof(json_item_start), json_item_start);
		if (NULL != start)
			*start = '\0';
		x->dent = NULL;
		pthread_mutex_init(&x->lookupmutex, NULL);
		x->id = 0;
		q = json_find_key(p, json_id);
		if (NULL == q) {
			lprintf(LOG_ERR, "could not find id for dir: %s\n",
				x->name);
			return false;
		}
		else {
			errno = 0;
			x->id = strtoll(q, NULL, 10);
			if (0 != errno) {
				lprintf(LOG_ERR,
					"error reading id for dir: %s\n",
					x->name);
				return false;
			}
		}
		x->name = s;
		s = json_strcpy(json_find_key(p, json_name), s, TR_DIR);
		q = json_find_key(p, json_email);
		if (NULL == q) {
			if (is_root && 0 == strcmp(x->name, upload_d.name)) {
				x->access = ACCESS_RO;
				x->name = upload_d.name;
			}
			else {
				x->access = access;
			}
		}
		else {
			memcpy(s - 1,
			       name_email_link,
			       sizeof(name_email_link) - 1);
			x->access = s - x->name + sizeof(name_email_link) - 2;
			s = json_strcpy(q, x->name + x->access, TR_NONE);
			x->access <<= ACCESS_SHIFT;
			q = json_find_key(p, json_hide_links);
			if (NULL != q && '1' == *q) {
				x->access += ACCESS_HIDDEN;
			}
			else {
				q = json_find_key(p, json_rw);
				if (NULL != q && '1' == *q)
					x->access += ACCESS_RW;
				else
					x->access += ACCESS_RO;
			}
		}
		x->cdate = params.mount_st.st_ctim.tv_sec;

		/* Don't read create_date on root shares: they don't have one!*/
		if (0 != x->id || ACCESS_RW == x->access) {
			q = json_find_key(p, json_cdate);
			if (NULL == q || '"' != *q) {
				lprintf(LOG_WARNING,
					"could not find create_date for dir: %s\n",
					x->name);
			}
			else {
				q= strptime(q + 1, "%Y-%m-%d %H:%M:%S", &tm);
				if ( NULL == q || '"' != *q) {
					lprintf(LOG_WARNING,
						"error reading create_date for dir: %s\n",
						x->name);
				}
				else {
					tm.tm_isdst = -1; /* mktime guess TZ */
					x->cdate = mktime(&tm);
				}
			}
		}

		x++;
		if (NULL == start)
			break;
		*start = json_item_start[0];
		p = start;
	}

	qsort(dent->subdirs, dent->n_subdirs, sizeof(struct dir), cmp_dirs);

	return true;
}

static bool fill_files(char *start, struct dentries *dent, char *s, bool hidden)
{
	char *p, *q;
	struct file *x;
	struct tm tm;

	if (NULL == start)
		return true;

	for (x = dent->files;
	     NULL != (p = strstr(start, json_item_start));
	     x++, start = p + sizeof(json_item_start)) {
		init_fs(x);
		if (hidden) {
			x->URL = hidden_url;
		}
		else {
			x->URL = s;
			s = json_strcpy(json_find_key(p, json_url), s, TR_NONE);
		}
		x->filename = s;
		s = json_strcpy(json_find_key(p, json_filename), s, TR_FILE);
		q = json_find_key(p, json_size);
		if (NULL == q) {
			lprintf(LOG_ERR,
				"could not find size for file: %s\n",
				x->filename);
			return false;
		}
		else {
			errno = 0;
			x->size = strtoll(q, NULL, 10);
			if (0 != errno) {
				lprintf(LOG_ERR,
					"error reading size for file: %s\n",
					x->filename);
				return false;
			}
		}
		q = json_find_key(p, json_date);
		if (NULL == q || '"' != *q) {
			lprintf(LOG_ERR,
				"could not find date for file: %s\n",
				x->filename);
			return false;
		}
		else {
			q= strptime(q + 1, "%Y-%m-%d %H:%M:%S", &tm);
			if ( NULL == q || '"' != *q) {
				lprintf(LOG_ERR,
					"error reading date for file: %s\n",
					x->filename);
				return false;
			}
			else {
				tm.tm_isdst = -1; /* let mktime guess TmZone */
				x->cdate = mktime(&tm);

			}
		}
	}

	return true;
}

static void parse_all(struct dir *d, struct json *j, struct dentries **dent)
{
	char    *subdirs = NULL,  *files = NULL;
	size_t s_subdirs = 0   , s_files = 0;
	char *s;
	const char *url_pattern;
	unsigned long n_subdirs, n_files, i, k;
	bool f_refresh, f_stat, f_hidden;

	f_refresh = (&root == d	 && NULL != params1f.refresh_file);
	f_stat    = (&root == d	 && NULL != params1f.stat_file);

	if (NULL != j) {
	/* Removing text inside "shares" to avoid fooling the algorithm.
	 * Assuming there is no ']' in shares. The only place it could
	 * be is in the e-mail string. ']' is allowed in an e-mail with
	 * restrictions so at the moment this will fail and is a //TODO
	 * to be merged with "use a real JSON parser!". */
		s = j->pb;
		while (NULL != (s = json_find_key(s, json_shares))) {
			if ( '[' != *s++ )
				continue;
			while (']' != *s && '\0' != *s)
				*s++ = ' ';
		}

		subdirs = json_find_key(j->pb, json_sub_folders);
		files	= json_find_key(j->pb, json_items);
		subdirs = parse_check(subdirs, d, json_sub_folders);
		files	= parse_check(files  , d, json_items);
	}

	n_subdirs = parse_count(subdirs, json_name, json_email,
				sizeof(name_email_link) - 1,
				&s_subdirs, &f_refresh, &f_stat, d);
	f_hidden = IS_HIDDEN(d->access);
	if (f_hidden)
		url_pattern = NULL;
	else
		url_pattern = json_url;
	n_files	  = parse_count(files  , json_filename, url_pattern, 0,
				&s_files  , &f_refresh, &f_stat, d);

	if (f_refresh) {
		if (params1f.refresh_hidden)
			f_refresh = false;
		else
			n_files++;
	}
	if (f_stat)
		n_files++;

	*dent = fs_alloc(sizeof(struct dentries) + s_subdirs + s_files +
			 sizeof(struct dir) * n_subdirs +
			 sizeof(struct file) * n_files);

	(*dent)->n_subdirs = n_subdirs;
	(*dent)->n_files   = n_files;
	(*dent)->subdirs   = (struct dir *)&(*dent)->files[n_files];

	s = (char *)(&(*dent)->subdirs[n_subdirs]);
	if (0 != (*dent)->n_subdirs &&
	    ! fill_folders(subdirs,
			   *dent,
			   s,
			   (d->access & ACCESS_MASK),
			   (&root == d)) )
		(*dent)->n_subdirs = 0;

	if (0 != (*dent)->n_files) {
		if ( fill_files(files, *dent, s + s_subdirs, f_hidden) ) {
			i = n_files - ((f_refresh) ? 1:0) - ((f_stat) ? 1:0);
			if (f_refresh) {
				init_fs(&(*dent)->files[i]);
				(*dent)->files[i].size = 0;
				(*dent)->files[i].cdate =
						params.mount_st.st_ctim.tv_sec;
				(*dent)->files[i].URL =
				(*dent)->files[i].filename =
						(char *)params1f.refresh_file;
				i++;
			}
			if (f_stat) {
				init_fs(&(*dent)->files[i]);
				(*dent)->files[i].size = STAT_MAX_SIZE;
				(*dent)->files[i].cdate =
						params.mount_st.st_ctim.tv_sec;
				(*dent)->files[i].URL = NULL;
				(*dent)->files[i].filename =
						(char *)params1f.stat_file;
			}
			qsort((*dent)->files, (*dent)->n_files,
			      sizeof(struct file), cmp_files);

		}
		else {
			(*dent)->n_files = 0;
		}
	}

	/* Deduplication of entries 
	 * flags is used non-atomically because 'flagged' elements will be
	 * removed in the same function since find_path does not expect 
	 * duplicated names */
	f_hidden = false; /* now indicates whether some files are "hidden" */

	/* Mark as DEL files having the name of a directory */
	i = k = 0;
	while (i < (*dent)->n_subdirs && k < (*dent)->n_files) {
		int cmp;
		cmp =  strcmp((*dent)->subdirs[i].name,
			      (*dent)->files[k].filename );
		if (0 == cmp) {
			f_hidden = true;
			(*dent)->files[k].flags = FL_DEL;
			lprintf(LOG_NOTICE,
				"in parent directory %s (%llu), file %s shadowed by a directory.\n",
				d->name, d->id, (*dent)->files[k].filename);
			k++;
		}
		else {
			if (cmp > 0)
				k++;
			else
				i++;
		}
	}

	/* Mark as DEL duplicated files in the directory */
	i = 0;
	k = 1;
	while (k < (*dent)->n_files) {
		if (0 != LOAD((*dent)->files[k].flags, relaxed)) {
			k++;
			continue;
		}
		if ((*dent)->files[i].flags != FL_DEL &&
		    0 ==  strcmp((*dent)->files[i].filename,
				 (*dent)->files[k].filename )) {
			f_hidden = true;
			lprintf(LOG_NOTICE,
				"in parent directory %s (%llu), duplicated filename %s, keeping most recent.\n",
				d->name, d->id, (*dent)->files[i].filename);
			/* Identical names, keep more recent */
			if ((*dent)->files[i].cdate > (*dent)->files[k].cdate) {
				(*dent)->files[k].flags = FL_DEL;
				k++;
				continue;
			}
			(*dent)->files[i].flags = FL_DEL;
		}
		i = k;
		k = i + 1;
	}

	/* Removing duplicates from the file list */
	if (f_hidden) {
		i = 0;
		k = 0;
		while (i < (*dent)->n_files) {
			if ((*dent)->files[i].flags == FL_DEL) {
				k++;
			}
			else {
				if (0 != k)
					memmove(&(*dent)->files[i - k],
						&(*dent)->files[i]    ,
						sizeof(struct file));
			}
			i++;
		}
		(*dent)->n_files -= k;
	}

	STORE(d->dent, *dent);
}

/*
 * @brief utility to initialise a curl handle
 *
 * The handle is initialised and populated according to the global parameters
 *
 * //TODO better signal handler? see: https://curl.se/mail/lib-2018-12/0076.html
 *
 * @param none
 * @return the initialised curl handle
 */
CURL *curl_init(bool is_http)
{
	CURL *curl;

	curl = curl_easy_init();
	if (NULL == curl)
		lprintf( LOG_CRIT, "initializing curl easy handle.\n" );
	CURL_EASY_SETOPT(curl, CURLOPT_IPRESOLVE, params1f.curl_IP_resolve
			     , "%ld");
	if (is_http && NULL != params.user_agent)
		CURL_EASY_SETOPT(curl, CURLOPT_USERAGENT, params.user_agent,
				 "%s");
	if (params1f.insecure)
		CURL_EASY_SETOPT(curl, CURLOPT_SSL_VERIFYPEER, 0, "%ld");
	if (NULL != params.ca_file)
		CURL_EASY_SETOPT(curl, CURLOPT_CAINFO, params.ca_file, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPALIVE, 1L,"%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPIDLE, CURL_KEEPALIVE_TIME,"%ld");
	CURL_EASY_SETOPT(curl, CURLOPT_TCP_KEEPINTVL,CURL_KEEPALIVE_TIME,"%ld");
//	CURL_EASY_SETOPT(curl, CURLOPT_NOSIGNAL, 1L,"%ld"); /* SIGPIPE ignored*/
	CURL_EASY_SETOPT(curl, CURLOPT_CONNECTTIMEOUT, CURL_CNX_TIMEOUT,"%ld");

	return curl;
}

static long curl_json_perform_wrapped(	CURL *curl, struct json *j,
					enum api_routes rt,
					const char *name_msg)
{
	static char try_again_message[] = "File unavailable";
	CURLcode res;
	char *p;
	long http_code;

	res = curl_easy_perform(curl);

	j->pb[j->cb]='\0';
	if (CURLE_OK != res) {
		if (!init_done &&
		    ( CURLE_COULDNT_RESOLVE_HOST  == res ||
		      CURLE_COULDNT_RESOLVE_PROXY == res ||
		      CURLE_COULDNT_CONNECT       == res)
		    )
			return CURLE_COULDNT_CONNECT;
		lprintf(LOG_ERR,
			"Ignoring: error %ld on curl_easy_perform url=`%s` name=`%s`.\n",
			res, api_urls[rt], name_msg);
		return 0;
	}

	CURL_EASY_GETINFO(curl, CURLINFO_RESPONSE_CODE, &http_code);
	if (HTTP_OK != http_code) {
		lprintf(LOG_ERR,
			"Ignoring: (http_code: %ld) url=`%s` name=`%s`.\n",
			http_code, api_urls[rt], name_msg);
		return http_code;
	}

	p = json_find_key(j->pb, json_status);
	if (NULL == p ||
	    0 != strncmp(p, json_OK, sizeof(json_OK) - 1)) {
		lprintf(LOG_ERR,
			"Ignoring: status is NOT OK, url=`%s` name=`%s` response=%s.\n",
			api_urls[rt], name_msg, j->pb);
		p = json_find_key(j->pb, json_message);
		if (NULL == p  ||
		    0 != strncmp(p + 1,
				 try_again_message,
				 sizeof(try_again_message) - 1))
			return 0;
		else
			return -EBUSY;
	}

	return http_code;
}

long curl_json_perform(char *postdata, struct json *j,
			enum api_routes rt, const char *name_msg)
{
	unsigned long i_API_call;
	long http_code;
	unsigned int retry, network_wait;
	struct timespec start;
	bool too_many_rq = false;
	bool locked = false;
	CURL *curl;
	struct curl_slist *list;

	curl = curl_init(true);
	list = curl_slist_append(NULL, auth_header);
	if (NULL != list)
		list = curl_slist_append(list,
					  "Content-Type: application/json");
	if (NULL == list)
		lprintf(LOG_CRIT, "building headers (curl_slist_append).\n");

	CURL_EASY_SETOPT(curl, CURLOPT_HTTPHEADER, list, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_WRITEFUNCTION, get_json_response, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_WRITEDATA, (void *)j, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_POSTFIELDS, postdata, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_URL, api_urls[rt], "%s");


	if (NULL != params1f.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);

	i_API_call = ADD(n_API_calls, 1L, relaxed) + 1L;
	lprintf(LOG_INFO, "<<< API(in) (iReq:%lu) %s POST=%s name=%s\n"
			  , i_API_call
			  , api_urls[rt] + sizeof(url_api_ep) - 1
			  , postdata
			  , name_msg);

	too_many_rq = LOAD(throttled);
	if (too_many_rq)
		lock(&apimutex, &locked);

	retry = network_wait = 0;
	while(1) {
		http_code = curl_json_perform_wrapped(curl, j, rt, name_msg);
		if (HTTP_TOO_MANY_REQUESTS == http_code) {
			if (!too_many_rq) {
				too_many_rq = true;
				STORE(throttled, true);
				lock(&apimutex, &locked);
			}
			json_free(j);
			json_init(j);
			retry++;
			if (retry >= MAX_RETRY)
				break;
			usleep(retry_delay_ms[retry]);
		}
		else {
			if (CURLE_COULDNT_CONNECT == http_code) {
				if (network_wait < params.network_wait) {
					usleep(NETWORK_WAIT_INTVL * 1000000);
					network_wait += NETWORK_WAIT_INTVL;
					continue;
				}
				else {
					lprintf(LOG_ERR,
						"Could not connect to network at startup.\n");
					break;
				}
			}
			if (too_many_rq && 0 == retry)
				STORE(throttled, false);
			break;
		}
	}
	unlock(&apimutex, &locked);
	if (!init_done && 0 != network_wait)
		lprintf(LOG_NOTICE,
			"Waited %u seconds for network at startup.\n",
			network_wait);
	if (NULL != params1f.stat_file)
		update_api_stats(rt, &start, http_code, retry);
	lprintf(LOG_INFO, ">>> API(out) (iReq:%lu) retry=%u json size=%lu http_code=%ld\n"
			  , i_API_call
			  , retry
			  , j->cb
			  , http_code);

	curl_slist_free_all(list);
	curl_easy_cleanup(curl);
	return http_code;
}

static bool init_dir_done(struct dir *d, struct dentries **dent,
			  memory_order order)
{
	*dent = atomic_load_explicit(&d->dent, order);
	if (NULL == *dent)
		return false;
	return true;
}


static void init_dir(struct dir *d, struct dentries **dent)
{
	static const char ls_post[] = "{\"folder_id\":%lld,\"files\":1}";
	static const char ls_shared[] =
		"{\"folder_id\":0,\"sharing_user\":\"%s\",\"files\":1}";
	long http_code;
	struct json j;

	if (init_dir_done(d, dent, memory_order_acquire))
		return;

	DEBUG("init_dir: `%s` (id=%"PRId64")\n", d->name, d->id);

	lock(&d->lookupmutex, NULL);
	/* In case another process simultaneously initialises the same dir
	 * we must now look again at pointers. Since we are just behind
	 * a lock relaxed is enough here.
	 */
	if (init_dir_done(d, dent, memory_order_relaxed)) {
		DEBUG("init_dir already done: `%s`\n", d->name);
		unlock(&d->lookupmutex, NULL);
		return;
	}
	json_init(&j);
	if (IS_SHARED_ROOT(d)) {
		char buf[sizeof(ls_shared) + strlen(d->name) ];
		snprintf(buf, sizeof(buf), ls_shared, SHARER_EMAIL(d));

		http_code = curl_json_perform(buf, &j, FOLDER_LS, d->name);
	}
	else {
		char buf[sizeof(ls_post) + STRLEN_MAX_INT64];
		snprintf(buf, sizeof(buf), ls_post, d->id);

		http_code = curl_json_perform(buf, &j, FOLDER_LS, d->name);
	}
	parse_all(d,
		  (HTTP_OK == http_code) ? &j : NULL, dent);
	json_free(&j);
	if (0 != params1f.refresh_time && &root == d) {
		struct timespec	  now;
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		STORE(refresh_start, now.tv_sec, relaxed);
	}
	unlock(&d->lookupmutex, NULL);

#ifdef _DEBUG
	if (LOG_DEBUG <= params.log_level) {
		char buf[32];
		char shared[] = { 'W', 'R', 'H', 'E' };
		char hint;
		int i;
		DEBUG(	"`%s` has %ld subdirs and %ld files\n",
			d->name,
			(*dent)->n_subdirs,
			(*dent)->n_files );
		for (i = 0; i< (*dent)->n_subdirs; i++) {
			ctime_r(&(*dent)->subdirs[i].cdate, buf);
			buf[24]='\0';
			if (ACCESS_RW == (*dent)->subdirs[i].access)
				hint = ' ';
			else
				hint = shared[(*dent)->subdirs[i].access &
					      ACCESS_MASK];
			DEBUG( "%03d: [%c% 11lld|%s] %s\n",
				i + 1,
				hint,
				(*dent)->subdirs[i].id,
				buf,
				(*dent)->subdirs[i].name);
		}
		for (i = 0; i< (*dent)->n_files; i++) {
			ctime_r(&(*dent)->files[i].cdate, buf);
			buf[24]='\0';
			DEBUG(	"%03d: [% 12"PRId64"|%s] %s => %s\n",
				i + 1,
				(*dent)->files[i].size,
				buf,
				(*dent)->files[i].filename,
				(*dent)->files[i].URL);
		}
	}
#endif
}

static void free_cur_live_dump_new(struct streams *old, struct streams *new,
				   const char *where)
{

	if (NULL != old) {
		DEBUG(">> %s: rcu file array pointer %p\n", where, old);
		rcu_free_ptr(old);
	}
	if (NULL == new) {
		DEBUG(">> %s: new stream list is now empty\n", where);
		return;
	}
#ifdef _DEBUG
	unsigned long i;
	for (i=0; i < new->n_streams; i++) {
		unsigned long cnt;
		struct strm_loc *loc;
		cnt = LOAD(new->a_streams[i]->counter);
		loc = LOAD(new->a_streams[i]->loc    );
		DEBUG(	">> %s: new stream list[%lu] %s (%lX) >%lu\n",
			where,
			i,
			(IS_HIDDEN(new->a_streams[i]->access) ?
				new->a_streams[i]->path		 :
				new->a_streams[i]->URL
			),
			cnt,
			(NULL == loc) ? 0 : loc->until);
	}
#endif
}

static void free_stream(struct stream *s, const unsigned long counter)
{
	struct strm_loc *loc;

	loc = LOAD(s->loc);
	DEBUG(	">> RCU and free stream %p(->%p): %s (%lX).\n",
		s, loc, s->URL, counter);

	fs_free(loc);
	rcu_free_ptr(s);
}

/* Available storage need not be refreshed for operations that
 * do not change it. According to 1fichier's answer, these are:
 * rename/move and create/delete directory. Hence in these operation
 * this function is called with the refresh_storage flag to false.
 *
 * Refresh also cleans potential streams with the ERROR flag on.
 */

static void refresh(struct dir *d, enum refresh_cause cause, bool ref_stor)
{
	unsigned long	spin = 0;
	struct dentries *dent;
	struct streams *cur, *new;
	unsigned long	i, j, counter;

	update_refresh_stats(cause);

	dent = AND(d->dent, 0);
	if (NULL != dent) {
		lprintf(LOG_INFO, ">> Refreshing on %s %s=%p.\n"
				  , refresh_msgs[cause], d->name, dent);
		rcu_free_struct(dent, rcu_free_dentries);
	}
	if (ref_stor) {
		lock(&storage_mutex, NULL);
		storage_flags |= FL_STOR_REFRESH;
		unlock(&storage_mutex, NULL);
	}

	cur = LOAD(live);
	do {
		if (0 != spin)
			fs_free(new);
		spin++;
		if (NULL == cur)
			return;
		for (i = j = 0; i < cur->n_streams; i++) {
			counter = LOAD(cur->a_streams[i]->counter);
			if (0 == (counter & OPEN_MASK) &&
			    0 != (counter & FL_ERROR))
				j++;
		}
		if (0 == j)
			return;
		new = fs_alloc(sizeof(struct streams) +
			       sizeof(void *) * (cur->n_streams - j));
		for (i = j = 0; i < cur->n_streams; i++) {
			counter = LOAD(cur->a_streams[i]->counter);
			if (0 == (counter & OPEN_MASK) &&
			    0 != (counter & FL_ERROR))	   {
				/* When a stream is marked as error, the first
				 * refresh will delete it. Unlikely we have
				 * several refreshes in parallel in which case
				 * if FL_DEL is already marked, we just continue
				 * because it means another parallel refresh
				 * just did DEL/call_rcu
				 */
				if (unlikely(0 != (counter & FL_DEL)))
					continue;
				counter = OR(cur->a_streams[i]->counter,FL_DEL);
				if (unlikely(0 != (counter & FL_DEL)))
					continue;
				free_stream(cur->a_streams[i], counter);
			}
			else {
				if (0 == (counter & FL_DEL))
					new->a_streams[j++] = cur->a_streams[i];
			}

		}
		/* Note that there could be more nodes in error that what we
		 * counting initially if error happen in parallel of refresh
		 * Hence we always alloc, and free if necessary.
		 */
		if (0 == j) {
			fs_free(new);
			new = NULL;
		}
		else {
			new->n_streams = j;
		}
	} while(!XCHG(live, cur, new));
	spin_add(spin -1, "refresh");
	free_cur_live_dump_new(cur, new, "Refresh");
}

static int find_dir(const void *key,const void *dir)
{
	int res;
	const struct pbcb *k = (const struct pbcb *)key;
	const struct dir  *d = (const struct dir *)dir;

	res =  strncmp( k->pb, d->name, k->cb );
	if (0 != res)
		return res;
	if ('\0' == d->name[k->cb])
		return 0;
	else
		return -1;
}

static int find_file(const void *key,const void *file)
{
	int res;
	const struct pbcb *k = (const struct pbcb *)key;
	const struct file *f = (const struct file *)file;

	res =  strncmp( k->pb, f->filename, k->cb );
	if (0 != res)
		return res;
	if ('\0' == f->filename[k->cb])
		return 0;
	else
		return -1;
}

static void timed_refresh()
{
	struct timespec	  now;
	unsigned long	start;

	start = LOAD(refresh_start);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	if (now.tv_sec - start >= params1f.refresh_time &&
	    XCHG(refresh_start, start, now.tv_sec))
	/* If atomic exchange fails, it means another thread saw the
	 * the timeout and already refreshed, so we don't do it twice!
	 */
		refresh(&root, REFRESH_TIMER, true);
}

static bool refresh_for_write(struct dir *d, long long id)
{
	unsigned long i;
	struct dentries *dent;

	if (NULL == d || (&upload_d == d && def_root == root.name))
		d = &root;
	if (id == d->id) {
		refresh(d, REFRESH_WRITE, true);
		return true;
	}
	dent =  LOAD(d->dent);
	if (NULL == dent)
		return false;
	for (i = 0; i < dent->n_subdirs; i++)
		if (refresh_for_write(&dent->subdirs[i], id))
			return true;
	return false;
}


void find_path(const char *path, struct walk *w)
{
	const char *p, *q;
	struct dir *d = &root;
	struct dentries *dent;
	struct pbcb key;

	DEBUG("find_path: `%s`\n", path);

	if ((NULL == params1f.refresh_file ||
	     0 != strcmp(path + 1, params1f.refresh_file)) &&
	     0 != params1f.refresh_time)
		timed_refresh();

	w->is_dir  = true;
	w->is_wf   = false;
	w->res.dir = NULL;
	w->parent  = NULL;
	if ('\0' == path[1]) { /* This is only true when path is '/' */
		w->res.dir = &root;
		w->parent  = &root;
		return;
	}
	for (p = path + 1; NULL != (q = strchr(p, '/')); p = q + 1) {
		key.pb = (char *)p;
		key.cb = q - p;
		init_dir(d, &dent);
		d = bsearch(&key, dent->subdirs, dent->n_subdirs
				, sizeof(struct dir), find_dir);
		if (NULL == d)
			return;
	}
	key.pb = (char *)p;
	key.cb = strlen(p);
	w->parent = d;
	init_dir(d, &dent);
	w->res.dir = bsearch(&key, dent->subdirs, dent->n_subdirs
				 , sizeof(struct dir), find_dir);
	if (NULL == w->res.dir) {
		w->is_dir = false;
		w->res.fs = bsearch(&key, dent->files, dent->n_files
					, sizeof(struct file), find_file);
		if (NULL != w->res.fs && F_DEL(w->res.fs))
			w->res.fs = NULL;
		/* Now search in the temporary files being written, not an
		 * 'else' from previous F_DEL case because a file might
		 * have been deleted and written as a new file. */
		if (NULL == w->res.fs)
			write_find(p, w);
	}
}

struct dir *refresh_upload(const char *path, struct dentries **dent)
{
	struct walk w;

	if (def_root == root.name) {
		find_path(path, &w);
		if (!w.is_dir || NULL == w.res.dir)
			return NULL;
		refresh(w.res.dir, REFRESH_POLLING, false);
		find_path(path, &w);
		if (!w.is_dir || NULL == w.res.dir)
			return NULL;
		init_dir(w.res.dir, dent);
		return w.res.dir;
	}
	else {
		*dent = AND(upload_d.dent, 0);
		lprintf(LOG_INFO, ">> Refreshing on %s %s=%p.\n"
				, refresh_msgs[REFRESH_POLLING]
				, upload_d.name
				, *dent);
		rcu_free_struct(*dent, rcu_free_dentries);
		update_refresh_stats(REFRESH_POLLING);
		init_dir(&upload_d, dent);
		return &upload_d;
	}
}

static int find_filename(const void *filename,const void *file)
{
	const struct file *f = (const struct file *)file;

	return strcmp(filename, f->filename);
}

const char *find_file_upload(struct dir *d, const char *filename)
{
	struct file *f;
	struct dentries *dent;

	init_dir(d, &dent);

	f = bsearch(filename, dent->files, dent->n_files
			    , sizeof(struct file), find_filename);
	if (NULL == f)
		return NULL;
	return f->URL;
}

const char * list_subdir_upload(struct dentries *dent, unsigned long *i,
				long long *subdir_id, time_t *cdate)
{
	unsigned long idx = *i;
	if (idx >=  dent->n_subdirs)
		return NULL;
	*i += 1;
	*cdate	   = dent->subdirs[idx].cdate;
	*subdir_id = dent->subdirs[idx].id;
	return dent->subdirs[idx].name;
}

const char * list_file_upload(struct dentries *dent, unsigned long *i,
			      const char **URL, time_t *cdate)
{
	unsigned long idx = *i;
	if (idx >=  dent->n_files)
		return NULL;
	*i += 1;
	*cdate = dent->files[idx].cdate;
	*URL   = dent->files[idx].URL;
	return dent->files[idx].filename;
}

/*
 * @brief check if write is forbidden in the directory.
 *
 * @param dir struct pointer of the directory to check
 * @param whether to log a message
 * @param file found to check age and parent.
 * @return true is write is forbidden (false otherwise)
 */

bool forbidden_write(struct dir * d, bool msg, struct walk *w)
{
	if (ACCESS_RW != (d->access & ACCESS_MASK)) {
		if (NULL != w &&
		    get_upload_id() == w->parent->id &&
		    !w->is_dir && !w->is_wf) {
			struct timespec now;

			clock_gettime(CLOCK_REALTIME_COARSE, &now);
			if (now.tv_sec >= w->res.fs->cdate +
					  params1f.resume_delay)
				return false;
		}
		if (msg)
			lprintf(LOG_WARNING,
				"no write access on protected directory: `%s`.\n",
				d->name);
		return true;
	}
	return false;
}



static int unfichier_getattr_wrapped(const char *path, struct stat *stbuf)
{
	struct walk w;

	find_path(path, &w);

	if (NULL == w.res.fs) {
		if (params1f.refresh_hidden &&
		    NULL != params1f.refresh_file && &root == w.parent &&
		    0 == strcmp(path + 1, params1f.refresh_file))
			refresh(&root, REFRESH_HIDDEN, true);
		memset(stbuf, 0, sizeof(struct stat));
		return -ENOENT;
	}

	memcpy(stbuf, &params.mount_st, sizeof(struct stat));
	if (w.is_dir) {
		stbuf->st_atim.tv_sec =
		stbuf->st_ctim.tv_sec =
		stbuf->st_mtim.tv_sec = w.res.dir->cdate;
		if (forbidden_write(w.res.dir, false, NULL)) {
			stbuf->st_mode &= ~(S_IWUSR | S_IWGRP | S_IWOTH);
		}
	}
	else {
		stbuf->st_mode		= st_mode &
					  ~(S_IWUSR | S_IWGRP | S_IWOTH);
		stbuf->st_nlink		= 1;
		if (w.is_wf) {
			write_getattr(stbuf, (struct wfile *)w.res.fs);
		}
		else {
			stbuf->st_size	 = w.res.fs->size;
			stbuf->st_atim.tv_sec =
			stbuf->st_ctim.tv_sec =
			stbuf->st_mtim.tv_sec = w.res.fs->cdate;
		}
		stbuf->st_blocks = stbuf->st_size / ST_BLK_SZ;
	}
	return 0;
}

static int unfichier_getattr(const char *path, struct stat *stbuf)
{
	int res;

	DEBUG("getattr: `%s`\n", path);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_getattr_wrapped(path, stbuf);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}


static int unfichier_readdir_wrapped(const char *path, void *buf,
				     fuse_fill_dir_t filler, off_t offset,
				     struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;
	struct walk w;
	unsigned long i;
	struct dentries *dent;

	find_path(path, &w);
	if (NULL == w.res.dir || !w.is_dir)
		return -ENOENT;

	init_dir(w.res.dir, &dent);

	if (0 != filler(buf, ".", NULL, 0))
		return -EBADF;
	if (&root == w.res.dir) {
		if (0 != filler(buf, "..", NULL, 0))
			return -EBADF;
	}

	for (i = 0; i < dent->n_subdirs; i++)
		if ( 0 != filler(buf, dent->subdirs[i].name, NULL, 0))
			return -EBADF;
	for (i = 0; i < dent->n_files; i++)
		if (!F_DEL(&dent->files[i]) &&
		    !is_fuse_hidden(dent->files[i].filename))
			if (0 != filler(buf, dent->files[i].filename, NULL, 0))
				return -EBADF;

	/* Now add the temporary files being written */
	return write_readdir(w.res.dir->id, path, buf, filler);
}

static int unfichier_readdir(const char *path, void *buf,
			     fuse_fill_dir_t filler, off_t offset,
			     struct fuse_file_info *fi)
{
	int res;

	DEBUG("readdir: `%s`\n", path);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_readdir_wrapped(path, buf, filler, offset, fi);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}


static int cmp_live(const void *URL,const void *p2)
{
	const struct stream *s = *(const struct stream **)p2;

	if (IS_HIDDEN(s->access))
		return 1;
	else
		return strcmp(URL, s->URL);
}
static int cmp_hidden(const void *path,const void *p2)
{
	const struct stream *s = *(const struct stream **)p2;

	if (IS_HIDDEN(s->access))
		return strcmp(path, s->path);
	else
		return -1;
}

/*
 * @brief manage list of opened files
 *
 * When a file not present in the list is opened, these functions create the new
 * array list, sorting out potential no longer needed files.
 *
 * Principle:
 *   The counter has 3 parts,
 *   - number of times the file is opened
 *   - a DEL flag
 *   - an ERROR flag
 * When the file is not needed anymore: not opened, not error, and not yet DEL,
 * (it means the whole counter is zero), then the algorithm tries to set the DEL
 * flag. If it succeeds (atomic_compare_exchange), then the memory is RCUed and
 * the now unneeded file is removed from the array.
 * Doing so ensures that only one thread will succeed in setting the DEL flag,
 * avoiding a double RCU that will trigger a double-free.
 * Also, if a thread wants to reuse a file while another one wants to delete it
 * the first doing the exchange will win, and the other thread will loop.
 *
 * The ERROR flag is not managed here, but during refresh. Files having had an
 * error are kept until refresh has been triggered, to avoid uselessly trying
 * to open them.
 */

static bool keep_live_stream(struct stream *s, struct timespec *t)
{
	unsigned long counter, spin = 0;
	struct strm_loc *loc;

	do {
		if (0 == spin)
			counter = LOAD(s->counter);
		spin++;

		if (0 != (counter & FL_ERROR) || 0 != (counter & ~3))
			return true;
		if (0 != (counter & FL_DEL))
			return false;
		/* From here: no error, not marked as del, and file not opened
		 *	      thus the only possible value of counter is: 0 */
		loc = LOAD(s->loc);
		if (0 == t->tv_nsec)
			clock_gettime(CLOCK_MONOTONIC_COARSE, t);
		if (NULL != loc && loc->until > t->tv_sec)
			return true;
	 } while(!XCHG(s->counter, counter, counter | FL_DEL));
	spin_add(spin - 1, "keep_live_stream");
	free_stream(s, counter);
	return false;
}

static int cmp_streams(const struct stream *s1, const struct stream *s2)
{
	if (IS_HIDDEN(s1->access)) {
		if (IS_HIDDEN(s2->access))
			return strcmp(s1->path, s2->path);
		else
			return -1;
	}
	else {
		if (IS_HIDDEN(s2->access))
			return 1;
		else
			return strcmp(s1->URL, s2->URL);
	}
}

static struct streams *new_streams_array(struct streams *cur,
					 struct stream	*added_stream,
					 struct timespec *t)
{
	struct streams *new;
	unsigned int i;
	bool ins = false;

	if (NULL == cur) {
		new = fs_alloc(sizeof(struct streams) + sizeof(void *));
		new->n_streams	  = 1;
		new->a_streams[0] = added_stream;
		return new;
	}

	/* We might allocated more memory than needed, but it is
	 * only pointers here, so no big deal
	 */
	new = fs_alloc(sizeof(struct streams) +
		       sizeof(void *) * (cur->n_streams + 1));
	new->n_streams = 0;
	for (i = 0; i < cur->n_streams; i++) {
		if (!keep_live_stream(cur->a_streams[i], t))
			continue;
		if (!ins && cmp_streams(added_stream, cur->a_streams[i]) <= 0) {
			ins = true;
			new->a_streams[new->n_streams++] = added_stream;
		}
		new->a_streams[new->n_streams++] = cur->a_streams[i];
	}

	if (!ins)
		new->a_streams[new->n_streams++] = added_stream;

	return new;
}

struct stream *find_live_stream(const char *path,
				struct walk *w,
				struct streams *live,
				unsigned long *counter)
{
	struct stream **found;

	if (NULL == live) {
		/* This situation happens only the first time a file is
		 * opened. Due to parallelislm, the message can possibly
		 * be displayed several times. */
		DEBUG("Open: first opened file(s).\n");
		return NULL;
	}

	if (IS_HIDDEN(w->parent->access))
		found = bsearch(path, live->a_streams, live->n_streams,
				sizeof(void *), cmp_hidden);
	else
		found = bsearch(w->res.fs->URL, live->a_streams,
				live->n_streams, sizeof(void *), cmp_live);
	if (NULL == found)
		return NULL;

	*counter = LOAD((*found)->counter);
	if (0 != ((*counter) & FL_DEL))
		return NULL;
	return *found;
}

static struct stream *new_stream(struct file *f, struct timespec *t,
				 const char * path, struct dir *parent)
{
	struct stream *new_stream = NULL;

	if (0 == t->tv_nsec)
		clock_gettime(CLOCK_MONOTONIC_COARSE, t);
	if (params1f.refresh_file == f->URL) {
		new_stream = fs_alloc(sizeof(struct stream));
		new_stream->URL = params1f.refresh_file;
	}
	else {
		size_t sz1;
		if (IS_HIDDEN(parent->access))
			sz1 = strlen(f->filename)  +
			      strlen(parent->name) - (parent->access >> 2) + 2;
		else
			sz1 = strlen(f->URL) + 1;
		new_stream = fs_alloc(sizeof(struct stream) + sz1
							    + strlen(path) + 1);
		strcpy(new_stream->path, path);
		new_stream->URL = (char *)(new_stream->path + strlen(path) + 1);

		if (IS_HIDDEN(parent->access)) {
			strcpy(new_stream->URL, f->filename);
			new_stream->email = new_stream->URL +
					    strlen(f->filename) + 1;
			strcpy(new_stream->email,
			       parent->name + (parent->access >> 2));
			new_stream->id = parent->id;
		}
		else {
			strcpy(new_stream->URL, f->URL);
		}
	}
	STORE(new_stream->loc, NULL);
	STORE(new_stream->counter, OPEN_INC);
	new_stream->access 	= parent->access;
	new_stream->size	= f->size;
	new_stream->last_rq_id	= 0;
	new_stream->nb_loc      = 0;
	STORE(new_stream->nb_streams, 0);
	pthread_mutex_init(&new_stream->fastmutex, NULL);
	pthread_mutex_init(&new_stream->curlmutex, NULL);

	return new_stream;
}

/*
 * @brief utility to know if a given path must use TLS/SSL or not
 *
 * @param path to be tested
 * @return true if ssl, false if on the no_ssl list
 */

bool is_ssl(const char *path)
{
	unsigned int i;

	for (i = 0; i < params1f.n_no_ssl; i++)
		if (0 == strncmp(path, params1f.a_no_ssl[i].pb
				     , params1f.a_no_ssl[i].cb)) {
			return false;
		}
	return true;
}

/*
 * @brief does the actual work for opening files
 *
 * Opened files and files that have a still valid location are accessible
 *   through the global atomic pointer 'opened'.
 * This points to a structure that has an array of pointers to those opened
 *   files. This is desirable, because even when a file have been closed, the
 *   'download ticket' (location) can still be valid. Thus it saves the time
 *   to get a new download ticket if the file is opened again in the validity
 *   period. This can't be handled like astreamfs because the directory tree
 *   can be flushed, and storing the location in the tree means it can be
 *   lost when refreshed, resulting in sub-optimal behavior.
 *   This also saves memory when we have a lot of files and few at a time are
 *   opened.
 *
 * This global list is managed without mutexes but with an atomic exchanges.
 * It could suffer from live lock in situation where the driver would be bombed
 * with parallel opens, but is a conscious choice because such a situation
 * is probably not a standard use!
 * See discussions about pro and cons of live-locks/spin-locks vs mutexes.
 *
 * All the management of the list is done at open. Release (close) only
 * decrements the counter (number of times the files is opened).
 * Refresh takes care of removing files with errors.
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_open_wrapped(const char *path, struct fuse_file_info *fi)
{
	struct walk w;
	unsigned long spin1 = 0, spin2;
	unsigned long counter;
	struct timespec t;
	struct streams *live_cur, *live_new = NULL;
	struct stream  *stream_found, *stream_new = NULL;

	find_path(path, &w);
	DEBUG(	"found: is_dir=%s is_wf=%s %p\n",
		((w.is_dir) ? "true" : "false"),
		((w.is_wf) ? "true" : "false"),
		w.res.fs);
	if (NULL == w.res.fs || w.is_dir)
		return -ENOENT;

	if (w.is_wf ||
	    ((fi->flags & O_ACCMODE) != O_RDONLY))
		return write_open(path, &w, fi);

	if (params1f.refresh_file == w.res.fs->filename) {
		refresh(&root, REFRESH_TRIGGER, true);
		fi->fh = (uintptr_t)params1f.refresh_file;
		return 0;
	}
	if (params1f.stat_file == w.res.fs->filename) {
		fi->fh = (uintptr_t)out_stats(NULL);
		return 0;
	}

	live_cur = LOAD(live);
	do {
		spin1++;
		t.tv_nsec = 0;
		stream_found = NULL;
		if (NULL != live_new) {
			fs_free(live_new);
			live_new = NULL;
		}
		spin2 = 0;
		stream_found = find_live_stream(path, &w, live_cur, &counter);
		do {
			spin2++;
			if (NULL != stream_found) {
				fs_free(stream_new);
				if (0 != (counter & FL_ERROR)) {
					fi->fh = (uintptr_t)stream_found;
					return -EACCES;
				}
				DEBUG(">> Open: file found: %s.\n", path);

			}
		} while(NULL != stream_found &&
			!XCHG(stream_found->counter, counter
						   , counter + OPEN_INC));
		spin_add(spin2 - 1, "unfichier_open_wrapped(1)");
		if (NULL != stream_found) {
			stream_new = stream_found;
			break;
		}

		if (NULL ==  stream_new)
			stream_new = new_stream(w.res.fs, &t, path, w.parent);

		live_new = new_streams_array(live_cur, stream_new, &t);
	} while(!XCHG(live, live_cur, live_new));
	spin_add(spin1 - 1, "unfichier_open_wrapped(2)");
	if (NULL != live_new)
		free_cur_live_dump_new(live_cur, live_new, "Open");

	stream_new->ssl = is_ssl(path);

	DEBUG(	">> Open: stream handle: %p (0x%lX).\n",
		stream_new,
		LOAD(stream_new->counter));

	fi->fh = (uintptr_t)stream_new;
	fi->keep_cache = 1;
	return 0;
}

/*
 * @brief fuse callback for open
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_open(const char *path, struct fuse_file_info *fi)
{
	int   res;

	DEBUG(">> open(%s,\"%s\"/0x%X)\n", path,
		(  ((fi->flags & O_ACCMODE) == O_RDONLY) ? "R" :
		  (((fi->flags & O_ACCMODE) == O_WRONLY) ? "W" : "RW")
		),
		(int)fi->flags
	     );

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_open_wrapped(path, fi);

	rcu_read_unlock();
	rcu_unregister_thread();
	return res;
}


/*
 * @brief fuse callback for release
 *
 * Close instruction to async reader are only sent if the file is not opened
 * anymore and there was an error. Otherwise the stream is left open in case
 * it is shortly needed again. This also avoid having to lock here and in open
 * because a file marked as error cannot be re-opened with the same handle.
 * Note that streams can't be freed until the counter is not zero (still opened)
 * hence the rcu_lock not needed here.
 *
 * @param path of the file to be opened (in)
 * @param address of the file information to return if succesful (out)
 * @return 0 if Ok or error code (negative)
 */

static int unfichier_release(const char *path, struct fuse_file_info *fi)
{
	struct stream *s;
	unsigned long counter;

	s = (struct stream *)((uintptr_t)(fi->fh));

	/* This is the constant address for the refresh file */
	if (s == (struct stream *)params1f.refresh_file) {
		DEBUG(">> Releasing trigger file.\n");
		return 0;
	}
	/* Stats: 1 open/1 buffer, so here just to free the buffer */
	counter = LOAD(s->counter);
	if (0 != (counter & FL_SPECIAL)) {
		DEBUG(">> Releasing stat file.\n");
		fs_free((void *)(uintptr_t)(fi->fh));
		return 0;
	}

	if (0 != (counter & FL_WRITE))
		return write_release((struct wfile *)((uintptr_t)(fi->fh)));


	counter = SUB(s->counter, OPEN_INC);
	work.size = 0;

	DEBUG(">> Releasing %s (0x%lX).\n", path, counter);
	return 0;
}

bool ok_json_get(const char *json_str, const char *pattern, off_t *value)
{
	char *p;
	p = json_find_key(json_str, pattern);
	if (NULL == p) {
		lprintf(LOG_ERR,
			"Unexpected/Ignoring: did not find %s key in json response (URL=%s)\n",
			pattern, api_urls[USER_INFO]);
		return false;
	}
	if ( 1 != sscanf(p, "%"SCNd64, value) ) {
		lprintf(LOG_ERR,
			"Unexpected/Ignoring: could not read the value of %s (URL=%s)\n",
			pattern, api_urls[USER_INFO]);
		return false;
	}
	return true;
}

#define TERA_BYTE (1024ULL * 1024 * 1024 * 1024)

static int extract_avail(long http_code, const char *json_str)
{
	off_t hot, cold, allowed, x;
	if (HTTP_OK != http_code)
		return -EIO;
	if (!ok_json_get(json_str, json_cold_storage, &cold))
		return -EIO;
	if (!ok_json_get(json_str, json_hot_storage, &hot))
		return -EIO;
	if (!ok_json_get(json_str, json_allowed_cold_storage, &allowed))
		return -EIO;
	DEBUG(	"<<< unfichier_statfs cold=%llu, hot=%llu, allowed=%llu\n",
		cold, hot, allowed);
	allowed *= TERA_BYTE;
	x = (allowed + hot + DEFAULT_BLK_SIZE - 1) / DEFAULT_BLK_SIZE;
	max_storage = x;
	if (cold >= allowed)
		x = 0;
	else
		x = (allowed - cold) / DEFAULT_BLK_SIZE;
	avail_storage = x;
	return 0;
}

static int unfichier_statfs(const char *path, struct statvfs *buf)
{
	struct timespec	now;
	int res;
	bool refresh = false;

	/* Note: this function does NOT use possibly dangling pointer.
	 *	 It only uses static root (if refresh is needed) and
	 *	 static available_storage and the like.
	 *	 Hence it does not need rcu_lock, etc...
	 */

	lock(&storage_mutex, NULL);

	if (0 != storage_flags) {
		clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
		if (0 == storage_next_update ||
		    now.tv_sec > storage_next_update) {
			storage_next_update = now.tv_sec + USER_INFO_DELAY;
			refresh = true;
		}
	}
	if (refresh) {
		static const char empty_json_object[]="{}";
		long http_code;
		struct json j;

		DEBUG("<<<(in) unfichier_statfs\n");

		json_init(&j);
		http_code = curl_json_perform((char *)empty_json_object,
					       &j,
					       USER_INFO,
					       (char *)path);
		res = extract_avail(http_code, j.pb);
		storage_flags = (0 == res) ? 0 : FL_STOR_ERROR;
		json_free(&j);
	}
	else {
		DEBUG("<<<(in/out)>>> unfichier_statfs\n");
		res = (0 == (FL_STOR_ERROR & storage_flags)) ? 0 : -EIO;
	}
	buf->f_blocks = max_storage;
	buf->f_bfree  =
	buf->f_bavail = avail_storage;

	unlock(&storage_mutex, NULL);

	buf->f_bsize  = DEFAULT_BLK_SIZE;
	buf->f_frsize = DEFAULT_BLK_SIZE;

	buf-> f_namemax = MAX_FILENAME_LENGTH;

	return res;
}

static bool forbidden_change(struct walk *w)
{
	if (w->is_dir && 0 != (w->res.dir->access & ~ACCESS_MASK)) {
		lprintf(LOG_WARNING,
			"cannot rename or remove the protected directory: `%s`.\n",
			w->res.dir->name);
		return true;
	}
	return false;
}

static bool forbidden_operation(struct dir * d)
{
	if (ACCESS_HIDDEN == (d->access & ACCESS_MASK)) {
		lprintf(LOG_WARNING,
			"impossible operation with 'hidden links' shared directory: `%s`.\n",
			d->name);
		return true;
	}
	return false;
}

/*
 * @brief check if the path is locked
 *
 * Concerns:
 * - the stat file
 * - the refresh file
 *
 * The check is always done towards existing files
 *
 * @param path to be checked
 * @return true if this is a reserved name.
 */
bool locked_files(struct walk *w, const char *msg)
{
	if (w->res.fs->filename == params1f.refresh_file) {
		lprintf(LOG_WARNING,
			"impossible to %s the refresh_file.\n",
			msg);
	    return true;
	}
	if (w->res.fs->filename == params1f.stat_file) {
		lprintf(LOG_WARNING,
			"impossible to %s the stat_file.\n",
			msg);
	    return true;
	}
	return false;
}

/*
 * @brief check if the name (file or dir) is reserved
 *
 * Concerns:
 * - the upload directory: when the directory is not already present
 * - the refresh file: with the refresh-hidden option when the file is not shown
 *
 * Since the check is always done towards NON-existing file/dir, the only
 * way is to do a costly strcmp.
 *
 * @param path to be checked
 * @param id of parent directory
 * @return true if this is a reserved name.
 */

bool reserved_names(const char* path, long long parent_id)
{
	return (root.id == parent_id &&
		(  0 == strcmp(path + 1, upload_d.name) ||
		  (params1f.refresh_hidden &&
		   0 == strcmp(path + 1, params1f.refresh_file)) ));
}

/* Note: see man 2 rename
 *  rename is always called with either 2 files (since we don't have
 *  symlinks in the mount), or with a dir as oldpath (from), in which case
 *  newpath (to) is either non existing or an empty dir.
 *
 *  In this version of fuse (26) we do not have the 'flags' of renameat2
 *  Since anyway it is impossible to do "atomic exchange" talking to a server
 *  that does not, the code has the behaviour of RENAME_NOREPLACE flag.
 *  This means that when newpath (to) exists, the return code is EEXIST.
 *  Should the user want to rename replacing an existing file, he shall
 *  first delete the target, then do the rename.
 *  rename also documents that in case of 2 hardlinks referring to the same
 *  content, it does nothing. To avoid having to call the server to check that,
 *  the code does not do that, instead it will also return EEXIST in this case.
 */

static const char json_trailer[] = "\"}";

static long rename_file(const char * url, long long dest_id,
			const char *rename_to,
			struct json *j,	const char *from)
{
	static const char mv_file_post[] = "{\"urls\":[\"%s\"],\"destination_folder_id\":%lld,\"rename\":\"";
	int sz;

	char post[sizeof(mv_file_post)	+ strlen(url)
					+ STRLEN_MAX_INT64
					+ MAX_ESCAPED_LENGTH
					+ sizeof(json_trailer) ];
	sz = sprintf(post, mv_file_post, url, dest_id);
	name_translate(rename_to, post + sz, TR_FILE, json_trailer);
	return curl_json_perform(post, j, FILE_MV, from);
}

static long chattr_file(const char *url,
			const char *rename_to,
			struct json *j,	const char *from)
{
	static const char chattr_post[]	 = "{\"urls\":[\"%s\"],\"filename\":\"";
	int sz;

	char post[sizeof(chattr_post) + strlen(url)
				      + MAX_ESCAPED_LENGTH
				      + sizeof(json_trailer)];

	sz = sprintf(post, chattr_post, url);
	name_translate(rename_to, post + sz, TR_FILE, json_trailer);
	return curl_json_perform(post, j, FILE_CHATTR, from);
}

static long rename_dir(long long dir_id, long long dest_id,
		       const char *rename_to,
		       struct json *j, const char *from)
{
	static const char mv_dir_post[]	 = "{\"folder_id\":%lld,\"destination_folder_id\":%lld,\"rename\":\"";
	int sz;

	char post[sizeof(mv_dir_post) + 2 * STRLEN_MAX_INT64
				      + MAX_ESCAPED_LENGTH
				      + sizeof(json_trailer) ];
	sz = sprintf(post, mv_dir_post, dir_id, dest_id);
	name_translate(rename_to, post + sz, TR_DIR, json_trailer);
	return curl_json_perform(post, j, FOLDER_MV, from);
}

static int unfichier_rename_wrapped(const char *from, const char *to,
				    enum refresh_cause ref_cause,
				    struct walk *wt, bool *del)
{
	static const char mv_fusr_post[] = "{\"urls\":[\"%s\"],\"destination_folder_id\":0,\"destination_user\":\"%s\",\"rename\":\"";
	static const char mv_dusr_post[] = "{\"folder_id\":%lld,\"destination_folder_id\":0,\"destination_user\":\"%s\",\"rename\":\"";
	static const char msg_rename[]   = "rename";
	long http_code = HTTP_OK;
	struct walk wf;
	char *q, *r = NULL;
	int res;
	struct json j;

	/* Note that Linux kernel (normally!) does NOT call the fuse driver in
	 * error situations like renaming of different types: file to dir, dir
	 * to file or dir to subdir, component of paths does not exist.
	 * The test is done anyway... better safe than sorry... but this error
	 * test code should never be reached.
	 * So, on Linux: EINVAL, EISDIR, ENOTDIR, should never be returned.
	 * Since this code mimics in fact renameat2(RENAME_NOREPLACE), kernel
	 * DOES call this function when target exists. Thus the only test that
	 * is hit before calling the move APIs is EEXIST
	 * When calling the move APIs, ENOENT can be returned (file/dir removed
	 * from server) or EREMOTEIO (other error on server).
	 * EACCESS is also returned when trying to play with the refresh_file,
	 * or when doing an impossible operation with shares or upload dir.
	 */

	find_path(from, &wf);
	if (NULL == wf.res.fs)
		return -ENOENT;

	/* Avoid useless tests when renaming 'resume directories' */
	if (REFRESH_WRITE != ref_cause) {
		if (!wf.is_dir && root.id == wf.parent->id
			       && locked_files(&wf, msg_rename))
			return -EACCES;

		if (forbidden_change(&wf) ||
		    forbidden_write(wf.parent, true, &wf))
			return -EACCES;

		res = forbidden_chars(to, (wf.is_dir) ? TR_DIR : TR_FILE);
		if (0 != res)
			return res;
	}

	find_path(to, wt);
	/* Note: the test to check whether a directory is renamed to its
	 *       descendant is complex and is left to the API that returns
	 *       an error code when such situation happens. */
	if (NULL == wt->parent)
		return -ENOENT;		/* Dir component of path not found */
	if (REFRESH_WRITE != ref_cause) {
		if (NULL != wt->res.fs) {
			if (wt->is_dir && !wf.is_dir)
				return -EISDIR;	/* Ren a file to existing dir */
			if (!wt->is_dir && wf.is_dir)
				return -ENOTDIR;/* Ren a dir to existing file */
			if (wt->is_dir && wf.is_dir)
				return -EEXIST;	/* Ren exist dir unsupported  */
			if (!wt->is_dir && !wf.is_dir)
				*del = true; 	/* Ren existing file: 2 steps */
		}
		if (forbidden_write(wt->parent, true, NULL))
			return -EACCES;
		if (reserved_names( to, wt->parent->id))
			return -EACCES;
		/* Temp file being written to (or waiting)		      */
		/* //TODO: but this is not atomic until file lists are merged!*/
		if (wf.is_wf) {
			res = write_rename(&wf, wt, from, to);
			if (-EAGAIN == res)
				res = unfichier_rename_wrapped(from,
							       to,
							       ref_cause,
							       wt,
							       del);
			return res;
		}
	}


	q = strrchr(to, '/') + 1;

	/* Directories */
	json_init(&j);
	if (wf.is_dir) {
		if (IS_SHARED_ROOT(wt->parent)) {
			char post[sizeof(mv_dusr_post)+ STRLEN_MAX_INT64
						      + strlen(wt->parent->name)
						      + MAX_ESCAPED_LENGTH
						      + sizeof(json_trailer) ];
			res = sprintf(post, mv_dusr_post
					  , wf.res.dir->id
					  , SHARER_EMAIL(wt->parent));
			name_translate(q, post + res, TR_DIR, json_trailer);
			http_code = curl_json_perform(post, &j,
						       FOLDER_MV, from);
		}
		else {
			http_code = rename_dir( wf.res.dir->id,
						wt->parent->id,
						q,
						&j, from);
		}
		json_free(&j);
		refresh(wf.parent, REFRESH_MV, false);
		refresh(wt->parent, REFRESH_MV, false);

		switch (http_code) {
			case HTTP_OK	    : return 0;
					/* Probably delete in parallel? */
			case HTTP_NOT_FOUND : return -ENOENT;
			default		    : return -EREMOTEIO;
		}
	}

	/* Files */
	if (wf.parent == wt->parent)
	{	/* Same directory, using chattr */
		http_code = chattr_file(wf.res.fs->URL,	q, &j, from);
	}
	else {
		if (IS_SHARED_ROOT(wt->parent)) {
			char post[sizeof(mv_fusr_post) + strlen(wf.res.fs->URL)
						       + strlen(wt->parent->name)
						       + MAX_ESCAPED_LENGTH
						       + sizeof(json_trailer)];
			res = sprintf(post, mv_fusr_post
					  , wf.res.fs->URL
					  , SHARER_EMAIL(wt->parent));
			name_translate(q,
				       post + res,
				       TR_FILE,
				       json_trailer);
			http_code = curl_json_perform(post, &j,
							FILE_MV, from);
		}
		else {
			http_code = rename_file(wf.res.fs->URL,
						wt->parent->id,
						q,
						&j, from);
		}
	}
	if (HTTP_OK == http_code)
		r = strstr(j.pb, wf.res.fs->URL);
	json_free(&j);

	/* Don"t refresh target if no error and must delete "to" */
	if (NULL == r || !*del)
		refresh(wt->parent, ref_cause, false);

	if (NULL == r) {
		refresh(wf.parent, ref_cause, false);
		if (HTTP_NOT_FOUND == http_code)
			return -ENOENT;	 /* Probably delete in parallel? */
		else
			return -EREMOTEIO;
	}
	else {
		if (wf.parent != wt->parent)
			OR(wf.res.fs->flags, FL_DEL);
	}
	return 0;
}

/*
 * @brief rename resume directories.
 *
 * @param current name of the directory
 * @param target name to be renamed to
 * @param id of the directory to be renamed
 * @param id of the parent to be renamed to (/.upload.1fichierfs)
 * @param dirname part of the target name.
 * @return O if success, othewise negative error code.
 */

int rename_dir_upload(const char *from,  const char *to, long long dir_id,
		      long long dest_id, const char *dirname)
{
	long http_code = 0;
	struct json j;
	int res;
	struct walk wt;
	bool del;

	if (0 == strcmp(from, to))
		return 0;

	json_init(&j);

	if (def_root == root.name) {
		res = unfichier_rename_wrapped(from, to, REFRESH_WRITE,
					       &wt, &del);
	}
	else {
		http_code = rename_dir(dir_id, dest_id, dirname, &j, from);

		switch (http_code) {
			case HTTP_OK	    : res = 0;
					      break;
					/* Probably delete in parallel? */
			case HTTP_NOT_FOUND : res = -ENOENT;
					      break;
			default		    : res = -EREMOTEIO;
		}
	}

	json_free(&j);
	return res;
}

/*
 * @brief rename file from upload directory to the target.
 *
 * Renaming with the dir ID is more reliable because it is resistant to
 * a rename or move of any directory inside the target path.
 *
 * However, if the target directory is deleted (which cannot happen in the
 * same 1fichierfs that created the file since this directory is non-empty, but
 * can happen from another instance of 1fichierfs or the Web page) then
 * created anew, and assuming the rest of the path was untouched, the file
 * can be renamed using the full path (not during resume since the full path
 * is not kept).
 *
 * @param full path of target (if not NULL, used if rename with ID/file failed)
 * @param directory ID of the target where to rename
 * @param URL of the file in the upload directory
 * @param filename part of the full path to rename to.
 * @return O if success, othewise negative error code.
 */
int rename_upload(const char *to, const long long dest_id,
	          const char *URL , const char *filename)
{
	long http_code = 0;
	struct json j;
	struct walk wt;
	char *p;
	int res;

	DEBUG("rename_upload(%s,%lld,%s,%s)\n", to, dest_id, URL, filename);

	json_init(&j);

	http_code = (get_upload_id() == dest_id) ?
			chattr_file(URL, filename, &j, NULL) :
			rename_file(URL, dest_id, filename, &j, NULL);

	if (HTTP_OK == http_code)
		p = strstr(j.pb, URL);
	else
		p = NULL;

	if (NULL == p) {
		switch (http_code) {
			case HTTP_NOT_FOUND:
				res = -ENOENT;	/* Parallel delete via web? */
				break;
			case HTTP_FORBIDDEN:
				res = -EACCES;	/* Parent dir removed? */
				break;
			default:
				res = -EREMOTEIO; /* Misc error */
		}
	}
	else {
		res = 0;
	}
	refresh_for_write(NULL, dest_id);
	refresh_for_write(&upload_d, get_upload_id());

	if (0 != res && NULL != to) {
		lprintf(LOG_WARNING,
			"failed to rename from upload dir using target dir ID %lld, trying full path: %s\n",
			dest_id, to);
		/* Refresh all since a dir was deleted out of this instance */
		refresh_for_write(NULL, 0);
		find_path(to, &wt);

		res = 0;
		if (NULL != wt.res.fs)
			res = -EEXIST;
		else if (NULL == wt.parent)
			res = -ENOENT;
		else if (forbidden_write(wt.parent, true, NULL))
			res = -EACCES;
		if (0 == res)
			res = rename_upload(NULL, wt.parent->id, URL, filename);
		else
			lprintf(LOG_ERR,
				"%d, failed to rename with full path: %s\n",
				res, to);
	}

	json_free(&j);
	return res;
}

static char *unlink_file(const char *URL, long *http_code, const char *filenam);

static int unfichier_rename(const char *from, const char *to)
{
	int res;
	struct walk wt;
	bool del = false;

	DEBUG(	"<<<(in) unfichier_rename `%s` to `%s`\n",
		from, to);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_rename_wrapped(from, to, REFRESH_MV, &wt, &del);

	/* Step 2 is to remove after rename if necessary */
	if (0 == res && del) {
		if (wt.is_wf) {
			res = write_unlink(&wt);
		}
		else {
			long http_cd;
			if (NULL == unlink_file(wt.res.fs->URL, &http_cd, to)) {
				if (HTTP_NOT_FOUND == http_cd)
					res = -ENOENT;	 /* Parallel delete */
				else
					res = -EREMOTEIO;
			}
		}
		refresh(wt.parent, REFRESH_MV, false);
	}

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_rename `%s` to `%s` res=%d\n"
			  , from, to, res);
  
	return res;
}

static int unfichier_link_wrapped(const char *source, const char *link)
{
	static const char cp_post[] = "{\"urls\":[\"%s\"],\"folder_id\":%lld,\"rename\":\"";
	static const char cu_post[] = "{\"urls\":[\"%s\"],\"folder_id\":0,\"sharing_user\":\"%s\",\"rename\":\"";
	static const char msg_link_from[] = "link from";
	long http_code = HTTP_OK;
	struct walk ws, wl;
	char *p = NULL, *lk;
	int sz, res;

	find_path(source, &ws);
	if (NULL == ws.res.fs)
		return -ENOENT;
	/* The API works only on files: -ENOSYS on directories
	 */
	if (ws.is_dir)
		return -ENOSYS;
	if (ws.is_wf)
		return -EBUSY;
	if (forbidden_operation(ws.parent))
		return -EACCES;
	if (get_upload_id() == ws.parent->id)
		return -EACCES;
	if (!ws.is_dir && root.id == ws.parent->id
		       && locked_files(&ws, msg_link_from))
		return -EACCES;

	res = forbidden_chars(link, TR_FILE);
	if (0 != res)
		return res;
	find_path(link, &wl);
	if (NULL == wl.parent)
		return -ENOENT;
	if (reserved_names(link, wl.parent->id))
		return -EACCES;
	if (NULL != wl.res.fs)
		return -EEXIST;
	if (forbidden_write(wl.parent, true, NULL))
		return -EACCES;

	lk = strrchr(link  , '/') + 1;

	{
		struct json j;
		json_init(&j);
		if (IS_SHARED_ROOT(wl.parent)){
			char post[sizeof(cu_post) + strlen(ws.res.fs->URL)
						  + strlen(wl.parent->name)
						  + MAX_ESCAPED_LENGTH
						  + sizeof(json_trailer) ];
			sz = sprintf(post, cu_post
					 , ws.res.fs->URL
					 , SHARER_EMAIL(wl.parent));
			name_translate(lk, post + sz, TR_FILE, json_trailer);
			http_code = curl_json_perform(post, &j
							   , FILE_CP, source);
		}
		else {
			char post[sizeof(cp_post) + strlen(ws.res.fs->URL)
						  + STRLEN_MAX_INT64
						  + MAX_ESCAPED_LENGTH
						  + sizeof(json_trailer) ];
			sz = sprintf(post, cp_post
					 , ws.res.fs->URL
					 , wl.parent->id);
			name_translate(lk, post + sz, TR_FILE, json_trailer);
			http_code = curl_json_perform(post, &j
							   , FILE_CP, source);
		}
		if (HTTP_OK == http_code)
			p = strstr(j.pb, ws.res.fs->URL);
		json_free(&j);
	}


	refresh(wl.parent, REFRESH_LINK, true);
	if (HTTP_OK != http_code || NULL == p) {
		/* Linking does not change source dir: refresh it on error only.
		 * If source = link's target, this refresh does nothing since
		 * we just refreshed the link's target directory above.
		 */
		refresh(ws.parent, REFRESH_LINK, false);
		if (HTTP_NOT_FOUND == http_code)
			return -ENOENT;	 /* Probably parallel delete? */
		else
			return -EREMOTEIO;
	}
	return 0;
}

static int unfichier_link(const char *source, const char *link)
{
	int res;

	DEBUG(	"<<<(in) unfichier_link `%s` -> `%s`\n", source, link);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_link_wrapped(source, link);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_link `%s` -> `%s` res=%d\n"
			  , source, link, res);
	return res;
}

/*
 * @brief fuse callback for unlink
 *
 * ... and related functions.
 * As usual the callback is the wrapper for RCU and debug messages.
 *
 * For deleting a file in the upload directory, unlink_upload is called and
 * if root is / (default) will use the wrapped function, so that the user
 * will have the same view of the directory. If root is not '/', the upload
 * directory is not accessible within the mount, hence unlink_file is called
 * directly.
 *
 * @param path file to unlink
 * @return 0 if Ok or error code (negative)
 */


static char *unlink_file(const char *URL, long *http_code, const char *filename)
{
	static const char rm_post[] = "{\"files\":[{\"url\":\"%s\"}]}";
	char post[sizeof(rm_post) + strlen(URL)];
	char *p = NULL;
	struct json j;

	json_init(&j);

	sprintf(post, rm_post, URL);
	*http_code = curl_json_perform(post, &j, FILE_RM, filename);
	if (HTTP_OK == *http_code)
		p = strstr(j.pb, URL);
	json_free(&j);

	return p;
}

static int unfichier_unlink_wrapped(const char *filename)
{
	static const char msg_unlink[] = "unlink";
	long http_code = 0;
	char *p = NULL;
	struct walk w;


	find_path(filename, &w);
	if (NULL == w.res.fs || w.is_dir)
		return -ENOENT;
	if (forbidden_write(w.parent, true, &w) || locked_files(&w, msg_unlink))
		return -EACCES;

	if (w.is_wf)
		return write_unlink(&w);

	p = unlink_file(w.res.fs->URL, &http_code, filename);

	if (NULL == p) {
		refresh(w.parent, REFRESH_UNLINK, true);
		if (HTTP_NOT_FOUND == http_code)
			return -ENOENT;	 /* Probably deleted in parallel! */
		else
			return -EREMOTEIO;
	}
	else {
		OR(w.res.fs->flags, FL_DEL);
	}

	return 0;
}

/*
 * @brief delete file from upload directory to the target.
 *
 * @param URL of the file to delete
 * @param name of the file to be deleted in the upload directory
 * @return O if success, othewise negative error code.
 */
int unlink_upload(const char * URL, const char *ftpname)
{
	long http_code = 0;

	if (NULL == unlink_file(URL, &http_code, ftpname)) {
		if (HTTP_NOT_FOUND == http_code)
			return -ENOENT;	 /* Parallel delete via web? */
		else
			return -EREMOTEIO;
	}
	refresh_for_write(&upload_d, get_upload_id());
	return 0;
}

static int unfichier_unlink(const char *path)
{
	int res;

	DEBUG("<<<(in) unfichier_unlink `%s`\n", path);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_unlink_wrapped (path);
	if (-EAGAIN == res)
		res = unfichier_unlink_wrapped (path);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_unlink `%s` res=%d\n"
			  , path, res);

	return res;
}


/*
 * @brief fuse callback for rmdir
 *
 * ... and related functions.
 * As usual the callback is the wrapper for RCU and debug messages.
 *
 * @param path directory to remove
 * @return 0 if Ok or error code (negative)
 */

static int do_rmdir(const char *dirname, long long id)
{
	static const char rmdir_post[] = "{\"folder_id\":%lld}";
	long http_code = 0;
	char post[sizeof(rmdir_post) + STRLEN_MAX_INT64];
	int res;
	struct json j;
	json_init(&j);

	sprintf(post, rmdir_post, id);
	http_code = curl_json_perform(post, &j, FOLDER_RM, dirname);
	json_free(&j);

	if (HTTP_NOT_FOUND == http_code) {
		res = -ENOENT;	/* Parallel delete? */
	}
	else {
		if (HTTP_OK == http_code)
			res = 0;
		else
			res = -EREMOTEIO;
	}
	return res;
}

static int unfichier_rmdir_wrapped(const char *dirname, bool protect_upload)
{
	struct walk w;
	struct dentries *dent;
	unsigned long i;
	bool undo;
	int res;

	find_path(dirname, &w);
	if (NULL == w.res.dir || !w.is_dir)
		return -ENOENT;

	if (protect_upload && forbidden_write(w.parent, true, NULL))
			return -EACCES;

	init_dir(w.res.dir, &dent);
	if (0 != dent->n_subdirs)
		return -ENOTEMPTY;
	for (i = 0; i < dent->n_files; i++)
		if (!F_DEL(&dent->files[i]))
			return -ENOTEMPTY;

	/* rm upload directory only when not already used for write */
	if (get_upload_id() == w.res.dir->id) {
		if (!write_init_undo_1())
			return -EACCES;
		undo = true;
	}
	else {
		if (forbidden_change(&w))
			return -EACCES;
		undo = false;
	}

	res = do_rmdir(dirname, w.res.dir->id);
	refresh(w.parent, REFRESH_RMDIR, false);

	if (undo)
		write_init_undo_2(res);

	return res;
}

static int unfichier_rmdir(const char *dirname)
{
	int res;

	DEBUG("<<<(in) unfichier_rmdir `%s`\n", dirname);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_rmdir_wrapped(dirname, true);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_rmdir `%s` res=%d\n"
			  , dirname, res);
	return res;
}

int rmdir_upload(const char *dirname, long long id)
{
	if (def_root == root.name)
		return unfichier_rmdir_wrapped(dirname, false);
	else
		return do_rmdir(dirname, id);
}


/*
 * @brief fuse callback for mkdir
 *
 * ... and related functions.
 * As usual the callback is the wrapper for RCU and debug messages.
 *
 * @param path directory to remove
 * @return 0 if Ok or error code (negative)
 */


static int do_mkdir(const char *dirname, long long parent_id,
		    long long *created_id)
{
	int	    res;
	long	    http_code;
	struct json j;
	static const char mkdir_post[]= "{\"folder_id\":%lld,\"name\":\"";
	char post[sizeof(mkdir_post) + STRLEN_MAX_INT64
				     + MAX_ESCAPED_LENGTH
				     + sizeof(json_trailer) ];

	json_init(&j);

	res = sprintf(post, mkdir_post, parent_id);
	name_translate(dirname, post + res, TR_DIR, json_trailer);

	http_code = curl_json_perform(post, &j, FOLDER_MKDIR, dirname);


	if (HTTP_OK == http_code) {
		if (ok_json_get(j.pb, json_folder_id
				    , (off_t *)created_id)) {
			res = 0;
		}
		else {
			lprintf(LOG_WARNING,
				"could not get created directory id in: %s\n",
				j.pb);
			res = -EREMOTEIO;
		}
	}
	else {
		res = -EREMOTEIO;
	}
	json_free(&j);

	return res;
}

static int unfichier_mkdir_wrapped(const char *path, long long *created_id,
				   bool protect_upload)
{
	int	    res;
	struct walk w;

	find_path(path, &w);
		/* These two tests should not happen. Fuse checks before call */
	if (NULL == w.parent)
		return -ENOENT;		/* Dir component of path not found */
	if (NULL != w.res.dir)
		return -EEXIST;

	if (protect_upload) {
		if (forbidden_write(w.parent, true, NULL))
			return -EACCES;

		if (reserved_names(path, w.parent->id))
			return -EACCES;

		res = forbidden_chars(path, TR_DIR);
		if (0 != res)
			return res;
	}

	res = do_mkdir(strrchr(path  , '/') + 1, w.parent->id, created_id);
	refresh(w.parent, REFRESH_MKDIR, false);

	return res;
}

int mkdir_upload(const char *path, const char *dirname, long long parent_id,
		 long long *created_id)
{
	if (def_root == root.name)
		return unfichier_mkdir_wrapped(path, created_id, false);
	else
		return do_mkdir(dirname, parent_id, created_id);
}

static int unfichier_mkdir(const char *path, mode_t unused)
{
	(void)unused;
	long long   created_id;
	int res;

	DEBUG("<<<(in) unfichier_mkdir `%s`\n", path);

	rcu_register_thread();
	rcu_read_lock();

	res = unfichier_mkdir_wrapped(path, &created_id, true);

	rcu_read_unlock();
	rcu_unregister_thread();

	lprintf(LOG_NOTICE, "(out)>>> unfichier_mkdir `%s` res=%d\n"
			  , path, res);
	return res;
}

/*
 * ***************************************************************************
 * From there starts the "stream engine" code
 */

static void get_work(struct reader *r)
{
	int err;
	struct timespec until;
	struct node *tmp, *rcv;


	if (r->idle || NULL != r->rcv) {
		DEBUG("get_work[%d] sem_wait\n", r - readers);
		err= sem_wait(&r->sem_go);
	}
	else {
		clock_gettime(CLOCK_REALTIME, &until);
		until.tv_sec += CURL_IDLE_TIME;
		DEBUG("get_work[%d] timedsem\n", r - readers);
		err= sem_timedwait(&r->sem_go, &until);
		if ( ETIMEDOUT == errno ) {
			DEBUG("get_work[%d]  timedout\n", r - readers);
			return;
		}
	}
	if ( 0 != err ) {
		lprintf(LOG_CRIT,
			"get_work[%d] sem_wait error: %d.\n",
			r - readers, err);
	}

	msg_pop( &r->rcv, &rcv );
	DEBUG("get_work[%d] got: %p\n",	r - readers, rcv);
	while(1) {
		DEBUG(	"get_work[%d] poped: %p->%p %"PRIu64":%lu\n",
			r - readers, rcv, rcv->next, rcv->offset, rcv->size);
		tmp = rcv->next;
		if (0 == rcv->size) {
			rcv->next = r->prv;
			r->prv = rcv;
			if ( NULL != tmp ) {
				lprintf(LOG_CRIT,
					"get_work[%d] there should NOT be another request after close: %p %"PRIu64":%lu-%lu\n",
					r - readers, tmp, rcv->offset,
					rcv->written, rcv->size);

			}
			break;
		}
		else {
			prv_insert(&r->prv, rcv, &work);
			if ( NULL == tmp )
				break;
		}
		DEBUG(	"get_work[%d] additional sem_wait, rcv=%p\n",
			r - readers, tmp);
		sem_wait(&r->sem_go);
		rcv = tmp;
	}
}

#include "astream_engine.c"

struct s_loc {
	struct stream *s;
	CURL *curl;
	long  http_code;
};


static char content_range_header[]="Content-Range:";
#define S_CONTENT_RANGE_HEADER sizeof(content_range_header) -1

static size_t header_check(char *buffer, size_t size,
			   size_t nitems, struct s_loc *sl)
{
	struct stream *s = sl->s;
	char *p;
	size_t sz = nitems * size;
	unsigned long long file_size;

	/*
	 * Status code must be 206. For other codes we stop.
	 * Since first line of response is always the response code we can
	 * use curl_easy_getinfo directly: it is more portable (http/2, etc...)
	 */
	if ( 0 == sl->http_code ) {
		CURL_EASY_GETINFO(sl->curl, CURLINFO_RESPONSE_CODE,
				  &sl->http_code);
		switch (sl->http_code)
		{
			case HTTP_PARTIAL_CONTENT : 	return sz;
			case HTTP_NOT_FOUND :
			case HTTP_GONE	    :		return 0;
			default : lprintf(LOG_ERR,
					  "unexpected range request status code: %ld\n",
					   sl->http_code);
		}
	}


	/*
	 * Check the file size from the Content-Range header.
	 */
	else {
		if (sz <= S_CONTENT_RANGE_HEADER ||
		    0 != strncasecmp(buffer, content_range_header
					   , S_CONTENT_RANGE_HEADER))
			return sz;

		p= memchr(buffer + S_CONTENT_RANGE_HEADER, '/', sz);
		if (NULL != p) {
			file_size= 0;
			for (p++; p < buffer + sz && isdigit(*p) ; p++)
				file_size= 10 * file_size + *p - '0';
			if (s->size == file_size)
				return sz;
			lprintf(LOG_ERR,
				"wrong file size:%"PRIu64"/%"PRIu64"\n",
				file_size, s->size);
		}
		else {
			lprintf(LOG_ERR,
				"malformed Content-Range header: %.*s.\n",
				buffer, s);
		}
	}
	OR(s->counter, FL_ERROR);
	return 0;
}




/*
 * @brief stores location-related informations.
 *
 * @param pointer on the current stream
 * @param location string
 * @param size of the location string (strlen, ie not counting '\0')
 * @param time of storage
 * @return none
 */
static const char *store_loc(struct stream *s,
			     struct strm_loc **loc,
			     const char	   *location ,
			     const size_t   sz_location,
			     const time_t   now_sec)
{
	(*loc) = fs_alloc(sizeof(struct strm_loc) + sz_location + 1);
	s->nb_loc ++;
	(*loc)->i_loc = s->nb_loc;
	(*loc)->until = now_sec + CURL_KEEP_LOCATION_TIME;
	memcpy((*loc)->location, location, sz_location);
	(*loc)->location[sz_location]= '\0';
	DEBUG(	"store_loc: (%p)->%s for URL=%s until=%ld\n",
		(*loc), (*loc)->location, s->URL,
		(*loc)->until - main_start.tv_sec);
	return (*loc)->location;
}


/*
 * @brief does the actual request for the donwload/get_token fallback
 *
 * It is expected to get a 302 (redirect) = HTTP_FOUND. If so the location
 * is returned, otherwise it means the 'redirect' is not supported and
 * the function returns NULL. We use a HEAD req since we want only headers.
 *
 * @param pointer on the current stream
 * @param pointer on a curl handle
 * @return pointer on the location (or NULL if errors)
 */
static const char *get_loc_fallback_wrapped(struct stream *s, CURL *curl,
					    long *http_code)
{
	CURLcode    res;
	const char *location;

	CURL_EASY_SETOPT(curl, CURLOPT_HTTPHEADER, NULL, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_WRITEFUNCTION, NULL, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_NOBODY, 1L, "%p");
	CURL_EASY_SETOPT(curl, CURLOPT_POSTFIELDS, NULL, "%s");
	CURL_EASY_SETOPT(curl, CURLOPT_URL, s->URL, "%s");

	res = curl_easy_perform(curl);

	if (CURLE_OK != res) {
		lprintf(LOG_ERR,
			"%ld on fallback get_token curl_easy_perform url=`%s`.\n",
			res, s->URL);
		return NULL;
	}
	CURL_EASY_GETINFO(curl, CURLINFO_RESPONSE_CODE, http_code);
	if (HTTP_FOUND != *http_code) {
		lprintf(LOG_ERR,
			"expecting http_code 302, got %ld on fallback for url=`%s`.\n",
			*http_code, s->URL);
		return NULL;
	}
	CURL_EASY_GETINFO(curl, CURLINFO_REDIRECT_URL, &location);
	return location;
}

static const char *get_loc_fallback_wrap_stats(struct stream *s,
					       long *http_code, bool *single)
{
	const char *location, *p;
	CURL *curl;

	curl = curl_init(true);
	location = get_loc_fallback_wrapped(s, curl, http_code);
	if (NULL != location)			/* Dup string before cleaning */
		location = fs_strdup(location);	/* curl: returned ptr is freed*/
	curl_easy_cleanup(curl);

	if (HTTP_NOT_FOUND == *http_code) {        /* Here it is unknown      */
		refresh(&root, REFRESH_404, true); /* whether fallback works  */
		return NULL;		   	   /* or not.                 */
	}

	if (NULL == location) {
		if (fallback_tested) {
			if (0 != AND(fallback_until, 0))
				lprintf(LOG_ERR,
					"fallback to get download tokens stopped working or network identification changed.\n");
		}
		else {
			lprintf(LOG_WARNING,
				"fallback to get download tokens does not work or network identification not used.\n");
		}
		return NULL;
	}
	if (!fallback_tested)
		lprintf(LOG_NOTICE, "fallback to get download tokens works!\n");
	p = strrchr(location, '/');
	if (NULL == p || p[1] != 'p')
		*single = true;
	return location;
}

/*
 * @brief fallback method to get a download token
 *
 *  If, for whatever reason (except 404 meaning the file is not there), the
 *  get_token API does not work, we try the "good old redirect method".
 *  Since it is automatic and we do not ask for any credentials beside the
 *  API key, this will only work if the "network identification" has been set
 *  and the program is used at the corresponding IP.
 *
 *  The function will test if the "fallback" works, if it does not work (or
 *  at some point stops working), it will never try to use it again, and
 *  the program needs to be restarted if the situation was fixed.
 *
 *  NOTE: when a location is returned, if the path starts with 'p' it is a
 *        'standard' location same as what the API returns with "single":0
 *        or without "single", because 0 is the default.
 *        If the path starts with 's' it is a "single" location, same as
 *        calling the API with "single":1
 *        For a "single", s->loc stays NULL, and the function only duplicates
 *        the string because curl_easy_cleanup will free it. The duplicated
 *        string is used only within stream_fs and will be freed there.
 *
 *  When testing for the first time, the function is locked on a global lock
 *  by the caller to avoid parallel tests. The caller then sets fallback_tested.
 *  fallback_tested changes only once under lock, so it does not need atomics.
 *  The other global fallback variables do.
 *
 * @param pointer on the current stream
 * @param address of a pointer on the strm_loc structure
 * @param time (seconds only) when the request started
 * @param pointer on a curl handle
 * @return pointer on the location (or NULL if errors)
 */

static const char *get_loc_fallback(struct stream *s,
				    struct strm_loc **loc,
				    const time_t now_sec,
				    long *http_code)
{
	const char *location, *p;
	struct timespec	   start;
	bool single = false;

	if (fallback_tested) {
		if (0 == LOAD(fallback_until))
			return NULL;
	}
	else {
		lprintf(LOG_NOTICE,
			"testing fallback to get download tokens.\n");
	}

	/* needed for stats (see comment below) */
	if (NULL != params1f.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);

	location = get_loc_fallback_wrap_stats(s, http_code, &single);

	/* The common function curl_json_perform also updates the stats, but it
	 * is not used for fallback, so stats must be updated here instead.   */
	if (NULL != params1f.stat_file)
		update_api_stats(FALLBACK_GET_TOKEN,
				 &start,
				 (NULL == location) ? 0 : HTTP_OK,
				 single ? 1 : 0);
	if (NULL == location)
		return NULL;
	if (single) {
		s->nb_loc ++;
		return location;
	}
	else {
		p = store_loc(s, loc, location, strlen(location), now_sec);
		fs_free((char *)location);
		return p;
	}
}

/*
 * @brief gets a new location for the URL (or file name if hidden)
 *
 *  This function will use the API donwload/get_token first.
 *  If it does not work, it will revert to the fallback above.
 *  When that happens and fallback is working, to avoid spamming the API
 *  that returned an error, the fallback method is used for KEEP_FALLBACK_TIME
 *  seconds. When that is elapsed, the function tries the API again.
 *
 *  For "hidden" situation, the fallback cannot be used, because there is no
 *  URL to redirect to since precisely the URL are hidden!
 *
 * @param pointer on the current stream
 * @param address of a pointer on the strm_loc structure
 * @param time (seconds only) when the request started
 * @param pointer on a curl handle
 * @param initialised json structure pointer
 * @param address of a pointer on a curl_slist
 * @return pointer on the location (or NULL if errors)
 */
static const char *get_new_location(struct stream *s,
				    struct strm_loc **loc,
				    time_t now_sec,
				    struct json *j,
				    long *http_code
				   )
{
	bool fallback_used = true;
	const char *location;
	char *p, *q = NULL;
	static const char down_post[]	= "{\"url\":\"%s\",\"no_ssl\":%d}";
	static const char down_hidden[]	= "{\"filename\":\"%s\",\"sharing_user\":\"%s\",\"folder_id\":%lld,\"no_ssl\":%d}";
	static const char url_pattern[]	= "\"url\":\"";

	*http_code = 0;
	if (IS_HIDDEN(s->access)) {
		char buf[sizeof(down_hidden) + strlen(s->URL)
					     + strlen(s->email)
					     + STRLEN_MAX_INT64];
		sprintf(buf, down_hidden, s->URL
					, s->email
					, s->id
					, (s->ssl) ? 0 : 1);
		*http_code = curl_json_perform(buf, j,
					        DOWNLOAD_GET_TOKEN, s->URL);
		fallback_used = false;
	}
	else
	{
		/* Using directly fallback avoids spamming a not working API. */
		if (fallback_tested && now_sec < LOAD(fallback_until))
			return get_loc_fallback(s, loc, now_sec, http_code);

		char buf[sizeof(down_post) + strlen(s->URL)];

		sprintf(buf, down_post, s->URL, (s->ssl) ? 0 : 1);
		*http_code = curl_json_perform(buf, j,
					        DOWNLOAD_GET_TOKEN, s->URL);
		fallback_used = false;
	}
	if (HTTP_OK != *http_code) {
		lprintf(LOG_ERR,
			"http_code %ld on get_token json resp.:`%s` for %s\n",
			*http_code, j->pb, s->URL);
		if (HTTP_NOT_FOUND == *http_code) {
			/* Cache tree out of sync, resync all */
			refresh(&root, REFRESH_404, true);
			return NULL;
		}
		if (-EBUSY == *http_code)  /* Don't try to use fallback when  */
			return NULL;	   /* file is unavailable	      */
		if (IS_HIDDEN(s->access))  /* Can't use fallback on "hidden": */
			return NULL;	   /*     no URL to redirect to!      */

		/* Trying fallback here. If it works keep it for some time.   */
		lock(&fallback_mutex, NULL);
		location = get_loc_fallback(s, loc, now_sec, http_code);
		if (HTTP_NOT_FOUND != *http_code) {
			if (NULL != location) {
				STORE(fallback_until,
				      now_sec + KEEP_FALLBACK_TIME);
				if (fallback_tested) {
					if (0 == OR(fallback_in_use, 1))
						lprintf(LOG_NOTICE,
							"Using fallback to get download tokens again!\n");
				}
				else {
					STORE(fallback_in_use, 1);
				}
			}
			fallback_tested = true;
		}
		unlock(&fallback_mutex, NULL);
		return location;
	}
	if (fallback_tested && !fallback_used && 0 != AND(fallback_in_use, 0))
		lprintf(LOG_NOTICE, "API to get download tokens works again!\n");
	p = strstr(j->pb, url_pattern);
	if (NULL != p) {
		p += sizeof(url_pattern) - 1;
		q = strchr(p, '"');
	}
	if (NULL == q)
		lprintf(LOG_CRIT,
			"Unexpected get_token json response:`%s` for %s\n",
			j->pb, s->URL);

	return store_loc(s, loc, p, q - p, now_sec);
}

/*
 * @brief returns a valid location to stream (or NULL if impossible)
 *
 *  This function will first return a valid location for the stream without
 *  locking, if it is the first time it is called from stream_fs (NULL = *loc).
 *  If there is no such valid location, it will lock, and call for a
 *  new location (function above).
 *
 *  Immediately after the lock, it repeats the test because it could
 *  (very unlikely) happen that another thread got the new valid location
 *  while this thread was locked.
 *  This second test is more complex: when the function is called a second
 *  time (see stream_fs) after a location that was supposed valid returned 410
 *  GONE, and the location is valid after the lock, it checks with the i_loc
 *  unique index if it is the same location or a new one.
 *
 * @param pointer on the current stream
 * @param address of a pointer on the strm_loc structure
 * @return pointer on the location (or NULL if errors)
 */

static const char *get_location(struct stream *s, struct strm_loc **loc,
				long *http_code)
{
	const char	  *location;
	struct timespec	   now;
	unsigned long	   old_i_loc;
	struct json	   j;

	if (NULL == *loc) {
		*loc = LOAD(s->loc);
		if (NULL != *loc) {
			clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
			if (now.tv_sec < (*loc)->until)
				return (*loc)->location;
		}
		old_i_loc = 0;
	}
	else {
		old_i_loc = (*loc)->i_loc;
	}

	lock(&s->curlmutex, NULL);
	/* Check again in case another thread got the location
	 * in parallel. 'relaxed' is enough after a lock.
	 */
	*loc = atomic_load_explicit(&s->loc, memory_order_relaxed);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &now);
	if (	(NULL != *loc &&
		 now.tv_sec < (*loc)->until &&
		 old_i_loc != (*loc)->i_loc
		) ||
		 S_ERROR(s)
	   ) {
		location = (*loc)->location;
		DEBUG(	"Location for %s from another thread: `%s`.\n",
			s->URL, (*loc)->location);
	}
	else {
		/* RCU-free the s->loc because a new one is requested, also
		 * needed when a 'single' is returned. */
		if (NULL != *loc) {
			rcu_free_ptr(*loc);
			/* Although this section is under lock, we use
			 * release so that other threads get the new value
			 * as soon as possible: that is because we now do
			 * a network operation that can take time.
			 */
			STORE(s->loc, NULL);
			*loc = NULL;
		}
		DEBUG("Getting new location for %s %ld\n", s->URL);
		json_init(&j);
		location = get_new_location(s,
					    loc,
					    now.tv_sec,
					    &j,
					    http_code
					   );
		json_free(&j);
		/* Atomics here are just before an unlock: relaxed is enough */
		if (NULL == location) {
			if (-EBUSY != *http_code)
				atomic_fetch_or_explicit(&s->counter,
							 FL_ERROR,
							 memory_order_relaxed);
		}
		else {
			if (NULL == *loc)
				DEBUG(	"Single location for %s is=`%s`\n",
					s->URL,
					location);
			else
				atomic_store_explicit(&s->loc,
						      *loc,
						      memory_order_relaxed);

		}
	}
	unlock(&s->curlmutex, NULL);
	return location;
}

/*
 * @brief stream (and single range) start
 *
 * Unlike astreamfs, since url details are initialiased with the API.
 * Also the location is taken from the API, which removes the locking/unlocking
 * complexity with the callbacks.
 *
 * It is the only place where curl_easy_perform is used to read files.
 *
 * If the curl on the location returned by get_location fails with 410 Gone (or
 * more infrequently 404), a retry is done because it means the guess on how
 * long to keep the location was wrong.
 *
 * @param pointer on a curl handle
 * @param pointer on the current stream
 * @param curl write callback function to be used
 * @param userdata for the write callback function
 * @return curl_easy_perform return code or STREAM_ERROR
 */

static long stream_fs(CURL *curl,
		      struct stream *s,
		      curl_write_callback callback,
		      void *userdata)
{
	CURLcode res;
	const char *location;
	struct s_loc sl;
	struct strm_loc *loc = NULL;
	unsigned int iter = 0;

	if (S_ERROR(s))
		return STREAM_ERROR;

	sl.s	= s;
	sl.curl	= curl;
	ADD(s->nb_streams, 1L);
	do {
		if (1 == iter)
			lprintf(LOG_WARNING,
				"retrying curl_easy_perform (URL=%s) on http_code: %ld\n",
				s->URL, sl.http_code) ;
		/* This is needed to protect s->loc in other threads */
		rcu_register_thread();
		rcu_read_lock();

		location = get_location(s, &loc, &sl.http_code);

		rcu_read_unlock();
		rcu_unregister_thread();

		if (NULL == location)
			return (-EBUSY == sl.http_code) ? -EBUSY :
							   STREAM_ERROR;


		sl.http_code = 0;
		DEBUG("curling %s (URL=%s)\n", location, s->URL);
		CURL_EASY_SETOPT(curl, CURLOPT_URL, location, "%s");
		CURL_EASY_SETOPT(curl, CURLOPT_HEADERDATA, &sl, "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_HEADERFUNCTION, header_check, "%p");
		CURL_EASY_SETOPT(curl, CURLOPT_WRITEFUNCTION, callback,"%p");
		CURL_EASY_SETOPT(curl, CURLOPT_WRITEDATA, userdata, "%p");
		res= curl_easy_perform(curl);
		if (NULL == loc)	   /* freeing 'single' location, used */
			fs_free((void *)location); /* only here: no need RCU  */
		iter++;
	} while (iter < 2 && (HTTP_GONE      == sl.http_code ||
			      HTTP_NOT_FOUND == sl.http_code    ));

	if (S_ERROR(s))
		return STREAM_ERROR;

	return (0 == res) ? HTTP_OK :  STREAM_ERROR;
}


/*
 * @brief asynchronous reader
 *
 * This is the actual asynchronous reader.
 * The very first part initializes the thread local 'work' buffer
 * The main loop waits for some work to arrive from unfichier_read.
 * - if it is a "close" (size is zero) we do nothing and loop back.
 * - otherwise we start reading at the offset requested by unfichier_read, after
 *   having inserted our local cache buffer.
 * We return from reading the stream on 3 conditions:
 * - stream end
 * - close
 * - unfichier_read selected this stream because there were no pending requests
 * In all those cases, we have nothing more to do than loop to the start, the
 * third case will automatically give a new work item and start a stream at
 * the new offset specified.
 *
 * @param opaque pointer
 * @return return value when the thread terminates (never reached for now)
 */

void *
async_reader(void *arg) {
	struct reader *r= arg;
	char range[40];
	struct node *n;
	struct stream *last_s = NULL;
	char buf[KBUF];
	long http_code;

	work.buf= buf;
	work.size = 0;

	do {
		if (NULL == r->prv) {
			DEBUG("async_reader[%d] loop: waiting.\n", r - readers);

			get_work(r);
			DEBUG(	"async_reader[%d] received read (%"PRIu64", %lu)\n",
				r - readers, r->prv->offset, r->prv->size );
		}
		if (0 == r->prv->size) {
			n = r->prv;
			r->prv = n->next;
			sem_post(&n->sem_done);
			continue;
		}
		if (S_ERROR(r->s)) {
			struct node *tmp;
			tmp = r->prv->next;
			r->prv->written = -EIO;
			if (&work != r->prv)
				sem_post(&r->prv->sem_done);
			r->prv = tmp;
			r->idle = true;
			continue;
		}
		if (last_s != r->s) {
			if (NULL != r->curl)
				curl_easy_cleanup(r->curl);
			r->curl = curl_init(r->s);
			last_s = r->s;
		}
		if (!r->streaming) {
			/* Add private buffer on top if necessary */
			if (r->prv->offset > r->start) {
				work.offset = r->start;
				work.written = 0;
				work.size = r->prv->offset - r->start;
				work.next = r->prv;
				r->prv = &work;
			}
			r->pos = r->start;
			r->streaming = true;
			sprintf(range,"%"PRIu64"-", r->start);
		}
		else {
			sprintf(range,"%"PRIu64"-", r->pos);
		}
		CURL_EASY_SETOPT(r->curl, CURLOPT_RANGE, range, "%s");
		DEBUG("async_reader[%d] range is %s\n", r - readers, range);

		/* Because streams are not closed on release, this is to make
		 * sure the stream won't be freed as long as it is live
		 */
		ADD(r->s->counter, OPEN_INC);
		http_code = stream_fs(r->curl,
				      r->s,
				      (curl_read_callback)write_data,
				      r);
		if (HTTP_OK != http_code && r->streaming) {
			int ret = (-EBUSY == http_code) ? -EBUSY : -EIO;
			DEBUG("stream_fs exception[%d].\n", r - readers);
			lock(&r->s->fastmutex, NULL);
			for (n = r->prv; n != NULL; n = n->next) {
				if (&work == n) {
					n->written = 0;
				}
				else {
					n->written = ret;
					sem_post(&n->sem_done);
				}
			}
			msg_pop(&r->rcv, &n);
			while (NULL != n) {
				int err;
				err= sem_wait(&r->sem_go);
				if ( 0 != err )
					lprintf(LOG_CRIT, "async_reader[%d] sem_wait error: %d.\n",
						r - readers, err);
				n->written = ret;
				sem_post(&n->sem_done);
				n = n->next;
			}
			r->idle = true;
			r->prv = NULL;
			unlock(&r->s->fastmutex, NULL);
		}
		SUB(last_s->counter, OPEN_INC);
	} while (!LOAD(exiting));

	if (NULL != r->curl)
		curl_easy_cleanup(r->curl);

	return NULL;
}

#ifdef _DEBUG
static void readers_dump(struct reader *r_dup, unsigned long long this_rq_id,
			 int i)
{
	int j;
	for (j=0; j< MAX_READERS; j++)
		DEBUG(	"dump: readers[%"PRIu64"%c%d] fs=%p %"PRIu64":%"PRIu64" (idle=%d) (n_rq=%u)\n",
			this_rq_id, (i==j)?'*':'|', j, r_dup[j].s,
			r_dup[j].start,	r_dup[j].end, r_dup[j].idle,
			r_dup[j].n_rq);

}
#endif

/* Global read counter, especially useful for debug */
_Atomic unsigned long long read_rq_id = 0;

/*
 * @brief fuse callback for read
 *
 * This function is called each time there is a block of data to read.
 * Steps:
 * - encapsulates the read request in our thread local 'work' buffer, including
 *   an arrival timestamp
 * - under lock, selects the relevant async reader for that read block.
 *   Each reader exposes a "read slice": start-end, a pending number of requests
 *   and a flag for idle state.
 *   -1) Best match is a block inside our "read slice" or at most 128k beyond
 *	 the end (128k being the kernel's readahead cache buffer size)
 *   -2) Second the "oldest" idle reader
 *   -3) Last the "oldest" reader with no pending requests
 * - Match 1) uses an existing stream reader.
 * - Match 2 and 3) will start a new stream, either on an already idle reader
 *	 or terminating the waiting stream plus start a new one.
 *	 We always add a 128K buffer ahead of the offset we received to better
 *	 serve out of order requests.
 *  - Match 4) is when no match is found: we do a range request with the exact
 *	 size instead of giving the request to the async reader.
 * - Once that is done, we put the request on the selected reader's receive
 *   queue and signal it with a semaphore (unless case 4).
 * - Now we only need to wait on the "done" semaphore for the reader to fill
 *   our buffer.
 * - We can then display the time it took and return to fuse.
 *
 * @param path to the file being read
 * @param buffer where we store read data
 * @param size of the buffer
 * @param offset from where we read
 * @param fuse internal pointer with possible private data
 * @return number of bytes read (should be size unless error or end of file)
 */
static int unfichier_read(const char *path, char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi)
{
	struct timespec start;
	int i, i_idle, i_wait;
	unsigned long last_rq_idle, last_rq_wait, this_rq_id;
	struct stream *s;
	struct reader r_dup[MAX_READERS];
	unsigned long counter;
	long http_code;

	if (unlikely(NULL == work.buf))
		sem_init(&work.sem_done, 0, 0);

	s = (struct stream *)((uintptr_t)(fi->fh));

	/* This is the constant address we have for refresh */
	if (s == (struct stream *)params1f.refresh_file)
		return 0;

	counter = LOAD(s->counter);
	/* This is the stat file */
	if (0 != (counter & FL_SPECIAL)) {
		if (offset + size >= ((struct stats *)s)->size) {
			if (offset >= ((struct stats *)s)->size)
				return 0;
			else
				size = ((struct stats *)s)->size - offset;
		}
		memcpy(buf, ((struct stats *)s)->buf + offset, size);
		return size;
	}

	if (0 != (counter & FL_WRITE))
		return write_read(buf, size, offset, fi);

	/* Shortcut in case of impossible read: beyond file end! */
	if (offset >= s->size) {
		DEBUG(	">>*<< impossible read beyond EOF (fast return 0): (%"PRIu64",%lu).\n",
			offset,	size);
		return 0;
	}

	/* Note: _COARSE clock does not have enough resolution here */
	if (NULL != params1f.stat_file)
		clock_gettime(CLOCK_MONOTONIC, &start);

	this_rq_id = ADD(read_rq_id, 1ULL, relaxed);
	DEBUG(	">> unfichier_read: %p (%"PRIu64",%lu) rq[%d]--------\n",
		s, offset, size, this_rq_id);

	work.buf     = buf;
	work.offset  = offset;
	work.written = 0;
	if (offset + size > s->size)
		work.size = s->size - offset;
	else
		work.size = size;

	i_idle = -1;
	i_wait = -1;
	last_rq_idle = ULONG_MAX;
	last_rq_wait = ULONG_MAX;

	/*
	 * Start critical section
	 */
	lock(&s->fastmutex, NULL);
	if (LOG_DEBUG == params.log_level)
		memcpy(r_dup, readers, sizeof(readers));
	for (i=0; i < MAX_READERS; i++) {
		if (readers[i].idle) {
			if (readers[i].last_rq_id < last_rq_idle) {
				i_idle = i;
				last_rq_idle = readers[i].last_rq_id;
			}
		}
		else {
			if (readers[i].s == s &&
			    work.offset >= readers[i].start &&
			    work.offset <= readers[i].end + KBUF)
			    break;
			if (0 == readers[i].n_rq &&
			    readers[i].last_rq_id < last_rq_wait) {
				i_wait = i;
				last_rq_wait = readers[i].last_rq_id;
			}
		}
	}
	if (i == MAX_READERS) {
		i = (-1 == i_idle) ? i_wait : i_idle;
		if (i == -1) {
			char range[40];
			CURL *curl;

			s->last_rq_id = this_rq_id;
			unlock(&s->fastmutex, NULL);

#ifdef _DEBUG
			DEBUG(	"no reader available rq[%"PRIu64"], doing single range: %"PRIu64"-%"PRIu64"\n",
				this_rq_id, offset, offset + size - 1);
			readers_dump(r_dup, this_rq_id, i);
#endif
			curl = curl_init(s);
			sprintf(range,"%"PRIu64"-%"PRIu64, offset
							 , offset + size - 1);
			CURL_EASY_SETOPT(curl, CURLOPT_RANGE, range, "%s");
			http_code = stream_fs(curl, s,
					      (curl_read_callback)single_range,
					      &work);
			if (HTTP_OK != http_code)
				work.written = (-EBUSY == http_code) ?  -EBUSY :
									-EIO;
			curl_easy_cleanup(curl);
			DEBUG(	">> read-response[single]: (%"PRIu64",%lu|%d)\n",
				this_rq_id, offset, size, work.written);

			if (NULL != params1f.stat_file)
				update_single_stats(work.written, &start);

			return (int)work.written;
		}
		readers[i].start = (work.offset > KBUF) ?
				    work.offset - KBUF : 0;
		readers[i].end = work.offset + work.size;
/* Infinite loop bug if reader's end goes beyond the received buffer
		if (readers[i].end < KBUF)
			readers[i].end = KBUF;
 * This test is useless already done above
		if (readers[i].end > s->size)
			readers[i].end = s->size;
*/
		readers[i].idle = false;
	}
	else {
		if (offset + size > readers[i].end)
			readers[i].end = offset + size;
	}
	ADD(readers[i].n_rq, 1, relaxed);
	s->last_rq_id 	      =
	readers[i].last_rq_id = this_rq_id;
	readers[i].s = s;
	msg_push(&readers[i].rcv, &work);
	sem_post(&readers[i].sem_go);
	unlock(&s->fastmutex, NULL);
	/*
	 * End critical section
	 */
#ifdef _DEBUG
	readers_dump(r_dup, this_rq_id, i);
#endif
	sem_wait(&work.sem_done);
	SUB(readers[i].n_rq, 1);

	DEBUG(	">> read-response[%u]: (%"PRIu64",%lu|%d)\n",
		this_rq_id, offset, size, work.written);

	if (NULL != params1f.stat_file)
		update_read_stats(work.written, i, &start);

	return work.written;
}




void unfichier_destroy(void * priv_data)
{
	unsigned int i;
	void * ret;
	lprintf(LOG_NOTICE, "Entering destroy.\n");

/* TODO: Known bug, if there is reader still busy (socket not closed) doing
 *       a fusermount -u will trigger SIGPIPE and the program will exit without
 *	 doing the full cleanup. (minor bug) */

	STORE(exiting, true);
	work.size = 0;
	if (NULL == work.buf)
		sem_init(&work.sem_done, 0, 0);
	for (i=0; i< MAX_READERS; i++) {
		msg_push(&readers[i].rcv, &work);
		sem_post(&readers[i].sem_go);
		sem_wait(&work.sem_done);
		pthread_join(readers[i].thread, &ret);
		sem_destroy(&readers[i].sem_go);
		lprintf(LOG_NOTICE, "<< thread reader[%d] ended.\n", i);
	}
	sem_destroy(&work.sem_done);

	write_destroy();
}

struct fuse_operations unfichier_oper = {
	.init		= unfichier_init,
	.destroy	= unfichier_destroy,
	.create		= unfichier_create,
	.write		= unfichier_write,
	.getattr	= unfichier_getattr,
	.readdir	= unfichier_readdir,
	.open		= unfichier_open,
	.read		= unfichier_read,
	.release	= unfichier_release,
	.statfs		= unfichier_statfs,
	.rename		= unfichier_rename,
	.unlink		= unfichier_unlink,
	.rmdir		= unfichier_rmdir,
	.link		= unfichier_link,
	.mkdir		= unfichier_mkdir,
	.utimens	= unfichier_utimens,
	.chmod		= unfichier_chmod,
	.chown		= unfichier_chown,
	.flush		= unfichier_flush,
	.fsync		= unfichier_fsync,
};

/* Note: this long_names list must be alphabetically sorted */
extern const char *long_names[];	/* Defined in     1fichierfs_option.c */
extern const unsigned int s_long_names;	/* Issue #3 - see 1fichierfs_option.c */
static const char end_expand[] = "Alo";


void cleanup(struct fuse_args *args)
{
	unsigned int i;
	struct streams *cur;

	lprintf(LOG_INFO,"Cleaning upon fuse unmount.\n");

	curl_global_cleanup();
	fuse_opt_free_args(args);

	if (def_user_agent != params.user_agent)
		fs_free(params.user_agent);
	if (def_type != params.filesystem_subtype)
		fs_free(params.filesystem_subtype);
	if (def_fsname != params.filesystem_name)
		fs_free(params.filesystem_name);
	fs_free(params1f.a_no_ssl);
	if (NULL != root.dent)
		refresh(&root, REFRESH_EXIT, false);
	if (def_root != root.name)
		fs_free(root.name);
	cur = LOAD(live);
	if (NULL != cur) {
		for (i = 0; i < cur->n_streams; i++)
			free_stream(cur->a_streams[i],
				    LOAD(cur->a_streams[i]->counter));
		fs_free(cur);
	}

	rcu_free_struct(upload_d.dent, rcu_free_dentries);

	STORE(live, NULL);  /* For final stats not to crash, since it's freed!*/

	rcu_unregister_thread();
	rcu_exit();

	fs_free(params.ca_file);
	fs_free(params1f.refresh_file);
	fs_free(params1f.api_key);
	fs_free(params1f.ftp_user);
	fs_free(auth_header);
	fs_free(params.log_file);
	if (NULL != params1f.stat_file) {
		struct stats end_stats;

		fs_free(params1f.stat_file);

		out_stats(&end_stats);
		if (NULL != log_fh)
			fputs(end_stats.buf, log_fh);
		else if (params1f.dry_run || params.foreground)
			fprintf(stderr, "%s", end_stats.buf);
	}
	if (NULL != log_fh)
		fclose(log_fh);
}


/*
 * @brief reads the root of the account
 *
 * Initialize root now to check the API Key, ans that the server responds,
 * so that if it fails the program exits before starting the fuse daemon.
 *
 * Then look if the upload directory can be found. It is limited to be
 * directly under root (for now) and if different mount path was specified
 * the real root of the account will be refreshed and freed.
 *
 * If a not NULL remote path is passed, checks it exists and is a directory,
 * then refreshes the tree whole tree and set this remote path as root.
 * The global 'root' will get the remote name: needed for e-mail on shares
 *
 * @param the remote path to mount
 * @return none
 */

static void init_root(char *remote_root)
{
	struct dentries *dent;
	struct walk w;
	long long root_id;
	unsigned long root_access;
	char *root_name;

	upload_d.name= (char *)upload_dir + 1;

	init_dir(&root, &dent);
	root.cdate = params.mount_st.st_mtim.tv_sec;

	write_init_foreground(remote_root);

	if (NULL == remote_root)
		return;

	if (!params1f.dry_run) {
		find_path(remote_root, &w);
		if (NULL == w.res.dir)
			lprintf( LOG_ERR, "root path: `%s` does not exist.\n"
					, remote_root);
		if (!w.is_dir)
			lprintf( LOG_ERR, "root path: `%s` is not a directory.\n"
					, remote_root);
		lprintf(LOG_INFO,"Mounting only the remote path: %s\n"
				, remote_root);
		root_id     = w.res.dir->id;
		root_access = w.res.dir->access;
		root_name   = fs_strdup(w.res.dir->name);
		refresh(&root, REFRESH_INIT_ROOT, false);
		root.id     = root_id;
		root.access = root_access;
		root.name   = root_name;
		if (ACCESS_RW != (root_access & ACCESS_MASK))
			params1f.readonly = true;
	}
	fs_free(remote_root);

}


/******************************
 */
int main(int argc, char *argv[])
{
	unsigned int ret = 0;
	struct parse_data data = { false, NULL, NULL, 0UL };
	struct fuse_args exp_args = {argc, argv, false};
	struct fuse_args args;
	struct timespec t;

	clock_gettime(CLOCK_REALTIME, &t);
	clock_gettime(CLOCK_MONOTONIC_COARSE, &main_start);

	expand_args(&exp_args, long_names, s_long_names, end_expand);

	/* First pass = counting (for memory allocation) + help, version, etc */
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	args.allocated = false;
	if (0 != fuse_opt_parse(&args,
				&data,
				unfichier_opts,
				unfichier_opt_count))
		lprintf(LOG_ERR, "parsing arguments.\n");
	fuse_opt_free_args(&args);

	check_args_first_pass(&data);


	/* Second pass = parse all arguments + populate allocated memory      */
	args.argc = exp_args.argc;
	args.argv = exp_args.argv;
	if (0 != fuse_opt_parse(&args,
				&data,
				unfichier_opts,
				unfichier_opt_proc))
		lprintf(LOG_ERR, "parsing arguments.\n");


	if (exp_args.allocated)
		fs_free(exp_args.argv);

	/* Check arguments, and output debug information */
	check_args_second_pass(&t);
	auth_header = fs_alloc(sizeof(auth_bearer) + strlen(params1f.api_key));
	memcpy(auth_header, auth_bearer, sizeof(auth_bearer) - 1);
	strcpy(auth_header + sizeof(auth_bearer) - 1, params1f.api_key);

	if (CURLE_OK != curl_global_init_mem(CURL_GLOBAL_ALL,
					     fs_alloc,
					     fs_free,
					     fs_realloc,
					     fs_strdup,
					     fs_calloc))
		lprintf( LOG_ERR, "initializing curl: curl_global_init().\n" );

	rcu_register_thread();

	init_root(data.root);

	if (!params1f.dry_run) {
		sigset_t set;

		check_args_third_pass(&args);

		/* Starting the fuse mount now!
		*/
		lprintf(LOG_NOTICE,"Init done, starting fuse mount.\n");

		init_done  = true;

		/* see: https://curl.se/mail/lib-2018-12/0076.html
		 * CURLOPT_NOSIGNAL is not enough with multi-threading, better
		 * to completely ignore SIGPIPE on all threads, although it
		 * seems to matter only when exiting, which is minor. */
		sigemptyset(&set);
		sigaddset(&set, SIGPIPE);
		pthread_sigmask(SIG_BLOCK, &set, NULL);

		ret = fuse_main(args.argc,
				args.argv,
				&unfichier_oper,
				NULL);
	}

	/* Cleaning */
	cleanup(&args);

	return ret;
}
