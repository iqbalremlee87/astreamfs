/*
 * 1fichierfs: common header for 1fichierfs modules.
 *
 *
 * Copyright (C) 2018-2021  Alain BENEDETTI <alainb06@free.fr>
 *
 * License:
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#define STREAM_STRUCT struct stream
#include "astreamfs_util.h"

#define PROG_NAME	"1fichierfs"
#define PROG_VERSION	"1.8.4"


#ifdef __GNUC__
#define likely(x)	__builtin_expect(!!(x), 1)
#define unlikely(x)	__builtin_expect(!!(x), 0)
#else
#define likely(x)	(x)
#define unlikely(x)	(x)
#endif

struct pbcb {
	char  *pb;
	size_t cb;
};

struct parse_data {
	bool got_API_key;
	const char *mount_path;
	char *root;
	unsigned long sz_no_ssl;
};

struct params1f {
	long  curl_IP_resolve;
	char *api_key;
	unsigned long n_no_ssl;
	struct pbcb  *a_no_ssl;
	char *stat_file;
	char *refresh_file;
	char *ftp_user;
	unsigned long refresh_time;
	unsigned long resume_delay;
	bool refresh_hidden;
	bool readonly;
	bool insecure;
	bool no_upload;
	bool dry_run;
	bool raw_names;
	uid_t	uid;
	gid_t	gid;
};

enum api_routes {
	FOLDER_LS		= 0,
	DOWNLOAD_GET_TOKEN	= 1,
	USER_INFO		= 2,
	FILE_MV			= 3,
	FILE_CHATTR		= 4,
	FILE_RM			= 5,
	FILE_CP			= 6,
	FOLDER_MV		= 7,
	FOLDER_RM		= 8,
	FOLDER_MKDIR		= 9,
	FTP_USER_LS		=10,
	FTP_USER_RM		=11,
	FTP_USER_ADD		=12,
	FTP_PROCESS		=13,
	FALLBACK_GET_TOKEN	=14,
};

enum refresh_cause {
	REFRESH_TRIGGER		= 0,
	REFRESH_HIDDEN		= 1,
	REFRESH_TIMER		= 2,
	REFRESH_POLLING		= 3,
	REFRESH_WRITE		= 4,
	REFRESH_MV		= 5,
	REFRESH_LINK		= 6,
	REFRESH_UNLINK		= 7,
	REFRESH_MKDIR		= 8,
	REFRESH_RMDIR		= 9,
	REFRESH_404		=10,
	REFRESH_EXIT		=11,
	REFRESH_INIT_ROOT	=12,
};

enum translation_type {
	TR_NONE,
	TR_FILE,
	TR_DIR,
};


struct strm_loc {
	unsigned long		i_loc;
	time_t			until;
	char			location[];
};

struct stream {
	_Atomic unsigned long	counter; /* nb_open +(1b:SP)+(1b:DEL)+(1b:ERR)*/
	unsigned long	 	access;  /* For shares when not ACCESS_RW     */
	long long	 	id;      /* in this case URL is the name      */
	char 			*email;
	unsigned long long	size;
	char			*URL;
	_Atomic (struct strm_loc *) loc;
	unsigned long		last_rq_id;
	bool			ssl;
	pthread_mutex_t		fastmutex;
	pthread_mutex_t		curlmutex;

	/* From there this is for statistics */
	unsigned long		nb_loc;
	_Atomic unsigned long	nb_streams;
	char			path[];
};


struct streams {
	unsigned long	n_streams;
	struct stream	*a_streams[];
};


#define STRLEN_MAX_INT64 20 /* Max length of int64 as string:
				  length=12345678901234567890
			       unsigned: 18446744073709551616
			       signed  : -9223372036854775807 */
#define KBUF (131072)		/* Readahead kernel buffer max size (128KB) */

	/* Live stream flags */
#define FL_ERROR	(1)
/* 1.0.9/1.2.0 This is to mark files as deleted on the tree and as deleted on
 *       the live list. It is thread safe (thanks to RCU) and does not require
 *       locks. It saves reading again the sub_dir for the tree management.   */
#define FL_DEL		(2)
#define FL_SPECIAL	(4)	     /* "Special" file, for the moment: stats */
#define FL_WRITE	(8)	     /* File being written */
#define OPEN_INC	(16)
#define OPEN_MASK	(~(FL_WRITE | FL_SPECIAL | FL_ERROR | FL_DEL))


/* Variable and functions defined in 1fichierfs.c */
/*******************************************************
 *  These are the things you can change at compile time.
 */
						/* Default initial size (on
						 * stack) of the JSON buffer  */
#define DEF_JSON_BUF_SIZE (16 * 1024 + 1)       /* This is not a random number!
						 * By default, curl callback
			bool			 * read buffers are 16k max.
						 * So when a json response fits
						 * in a single curl callback
						 * no malloc is needed. +1 for
						 * the '\0' end string.
						 * That should be the case of
						 * almost all requests except
						 * long directory listings (50+
						 * files + sub-directories).  */

/*******************************************************/

#define MAX_FILENAME_LENGTH (250)   /* Max lenght supported by 1fichier's API
				      for filenames                           */

#define MAX_ESCAPED_LENGTH (6 * MAX_FILENAME_LENGTH)  /* Worst = all \unnnn */


struct json {
	char  *pb;
	size_t cb;
	char   buf[DEF_JSON_BUF_SIZE];
};
struct file;
struct wfile;		/* in 1fichier_write.c */
struct wfile_spec;	/* in 1fichier_write.c */
struct dentries;
struct dir {
	_Atomic (struct dentries *) dent;
	pthread_mutex_t	lookupmutex;
	char		*name;
	long long	id;
	time_t		cdate;
	unsigned long	access; /* offset of email | last 2 bits access mode */
};

/* The access field is ACCESS_RW (0) when not on a shared directory. */
#define ACCESS_RW	(0)

struct walk {
	bool		is_dir;
	bool		is_wf;
	union {
		struct dir	*dir;
		struct file	*fs;
		struct wfile	*wf;
	} res;
	struct dir	*parent;
};

extern _Atomic bool exiting;
extern _Atomic (struct streams *)live;
extern struct params1f params1f;
extern struct fuse_operations unfichier_oper;
extern mode_t st_mode;

#define JSON_ITEM_START "{\""
extern const char json_item_start[];
extern const char json_items[];
extern const char json_filename[];
extern const char json_url[];
extern const char json_folder_id[];

/* This is used to refresh upload directory when remote root is not / */
#define UPLOAD_DIR_ID_NOT_INITIALISED (-1LL)
extern long long  get_upload_id(void );
extern void	  set_upload_id(long long id);

extern bool is_fuse_hidden(const char *filename);
extern CURL *curl_init(bool is_http);
extern void json_init(struct json *j);
extern void json_free(struct json *j);
extern char *json_find_key(const char *json, const char *str);

/*
 * @brief length of the json string once translated as specified
 *
 * @param json string (points on the leading ")
 * @param type of translation needed
 * @return length of the translated string
 */
extern size_t json_strlen(const char *json, enum translation_type tr);

/*
 * @brief copies the json string to destination with translation as specified
 *
 * @param json string (points on the leading ")
 * @param destination of the translation
 * @param type of translation needed
 * @return length of the translated string
 */
extern char *json_strcpy(const char *json, char *dest,
			 enum translation_type tr);


/*
 * @brief performs a translation to a compatible name for the server
 *
 * @param character to translate
 * @param (output) where to store the translation
 * @param type of translation to perform (TR_FILE or TR_DIR)
 * @return number of characters outputed
 */
extern size_t translate(char c, char *trans, enum translation_type tr);

/*
 * @brief check if the name (file or dir) is reserved
 *
 * @param path to be checked
 * @param id of parent directory
 * @return true if this is a reserved name.
 */
extern bool reserved_names( const char* path, long long parent_id);

extern int rmdir_upload(const char *dirname, long long id);
extern int mkdir_upload(const char *path, const char *dirname,
			long long parent_id, long long *created_id);
extern int rename_dir_upload(const char *from,  const char *to,
			     long long dir_id, long long dest_id,
			     const char *dirname);
/*
 * @brief rename file from upload directory to the target.
 *
 * @param full path of target (if not NULL, used if rename with ID/file failed)
 * @param directory ID of the target where to rename
 * @param URL of the file in the upload directory
 * @param filename part of the full path to rename to.
 * @return O if success, othewise negative error code.
 */
extern int rename_upload(const char *to, const long long dest_id,
			 const char *URL , const char *filename);
/*
 * @brief delete file from upload directory to the target.
 *
 * @param URL of the file to delete
 * @param name of the file to be deleted in the upload directory
 * @return O if success, othewise negative error code.
 */
extern int unlink_upload(const char * URL, const char *ftpname);

extern bool ok_json_get(const char *json_str, const char *pattern, off_t *val);
extern long curl_json_perform(char *postdata, struct json *j,
			      enum api_routes rt, const char *name_msg);
extern bool is_ssl(const char *path);
/*
 * @brief check if write is forbidden in the directory.
 *
 * @param dir struct pointer of the directory to check
 * @param whether to log a message
 * @param file found to check age and parent.
 * @return true is write is forbidden (false otherwise)
 */
extern bool forbidden_write(struct dir * d, bool msg, struct walk *w);
extern bool forbidden_path(const char* path, const char *msg);
/*
 * @brief check filename illegal spaces, chars, and length
 *
 * @param string to check
 * @param type of translation needed (TR_FILE or TR_DIR)
 * @return O or code ENAMETOOLONG (speaks by itself) or illegal spaces: EILSEQ
 */
extern int forbidden_chars(const char *str, enum translation_type tr);
extern void find_path(const char *path, struct walk *w);
struct stream *find_live_stream(const char *path,
				struct walk *w,
				struct streams *live,
				unsigned long *counter);
struct dir *refresh_upload(const char *path, struct dentries **dent);
extern const char *find_file_upload(struct dir *d, const char *filename);
const char * list_subdir_upload(struct dentries *dent, unsigned long *i,
				long long *subdir_id, time_t *cdate);
const char * list_file_upload(struct dentries *dent, unsigned long *i,
			      const char **URL, time_t *cdate);


/*_______________________________________*/
/* Functions defined in 1fichierfs_rcu.c */
struct rcu_head;

#define rcu_register_thread()
#define rcu_unregister_thread()
extern void rcu_init(void);
extern void rcu_read_lock(void);
extern void rcu_read_unlock(void);
extern void rcu_exit(void);

	/*
	* Specific rcu cleaner functions for pointers, and structures
	*/
extern void rcu_free_ptr(void *p);
extern void rcu_free_struct(void *pstruct, void (*func)(struct rcu_head *head));
extern void *rcu_get_p_struct_from_head(struct rcu_head *head);

/*_________________________________________________________*/
/* Variables and Functions defined in 1fichierfs_options.c */
extern const struct fuse_opt  unfichier_opts[];

extern int  unfichier_opt_count(void *, const char *, int, struct fuse_args *);
extern int  unfichier_opt_proc(void *,	const char *, int, struct fuse_args *);
extern void check_args_first_pass(struct parse_data *);
extern void check_args_second_pass(struct timespec *);
extern void check_args_third_pass(struct fuse_args *);


/*_________________________________________*/
/* Elements declared in 1fichierfs_write.c */
/*******************************************************
 *  These are the things you can change at compile time.
 */
#define MAX_WRITERS 4

/**/
extern const char upload_dir[];
struct wfile_list;
extern _Atomic (struct wfile_list *) wflist;

extern bool is_write_possible(void);
extern unsigned long get_wfl_n_files(struct wfile_list *wfl, bool **keep);
extern void get_wf_info(struct wfile_list *wfl, bool *keep, unsigned long i,
			uint64_t *size, time_t *eta, unsigned long *counter,
			unsigned long *step, const char **path);
extern bool get_writer_ref(struct wfile_list *wfl, bool *keep,
			   unsigned int i, char *ref, size_t len);
extern void write_destroy();
extern void *unfichier_init(struct fuse_conn_info *conn);
extern void write_init_foreground(const char *remote_root);
extern bool write_init_undo_1();
extern void write_init_undo_2(int res);
extern int unfichier_write(const char *path, const char *buf, size_t size,
			   off_t offset, struct fuse_file_info *fi);
extern int unfichier_create(const char *path, mode_t unused,
			    struct fuse_file_info *fi);
extern int unfichier_utimens(const char *path, const struct timespec tv[2]);
extern int unfichier_chmod(const char *path, mode_t mode);
extern int unfichier_chown(const char *path, uid_t uid, gid_t gid);
extern int unfichier_flush(const char *path, struct fuse_file_info *fi);
extern int unfichier_fsync(const char *path, int datasync,
			   struct fuse_file_info *fi);

extern int write_open(const char *path, struct walk *w,
		      struct fuse_file_info *fi);
extern int write_release(struct wfile *wf);
extern int write_readdir(long long id, const char *path, void *buf,
			 fuse_fill_dir_t filler);
extern int write_unlink(struct walk *w);
extern void write_getattr(struct stat *stbuf, struct wfile *wf);
extern void write_find(const char *filename, struct walk *w);
extern int write_rename(struct walk *wf, struct walk *wt,
			const char *from, const char *to);
extern int write_read(char *buf, size_t size, off_t offset,
		      struct fuse_file_info *fi);

/* Functions defined in 1fichierfs_stats.c */
#define STAT_MAX_SIZE	KBUF

struct stats {
	unsigned long	counter; /* constant to FL_SPECIAL */
	size_t		size;
	char		buf[STAT_MAX_SIZE];
};

void init_stats(void);
struct stats *out_stats(struct stats *out);
void update_read_stats( int written, unsigned int i, struct timespec *start);
void update_write_stats(int written, unsigned int i, struct timespec *start);
void update_write_err(unsigned int i);
void update_single_stats(int, struct timespec *);
void update_api_stats(enum api_routes, struct timespec *, long, unsigned int);
void update_refresh_stats(enum refresh_cause);
void update_speed_stats(bool read, int written, unsigned int i);

